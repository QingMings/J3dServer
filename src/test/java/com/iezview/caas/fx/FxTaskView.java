package com.iezview.caas.fx;

import com.iezview.caas.dao.IBuildingDAO;
import com.iezview.caas.dao.IRemotesensemapDAO;
import com.iezview.caas.daoimpl.BuildingDAO;
import com.iezview.caas.daoimpl.RemotesensemapDAO;
import com.iezview.caas.entity.Building;
import com.iezview.caas.entity.Remotesensemap;
import com.iezview.caas.http.client.UploadProgressTask;
import com.iezview.caas.http.retrofit.LoginService;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.TaskProgressView;
import org.controlsfx.dialog.ProgressDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 *
 */
public class FxTaskView  extends Application {
    private Stage primaryStage;
    final FileChooser fileChooser = new FileChooser();
    private IBuildingDAO dao = new BuildingDAO();
    private IRemotesensemapDAO remotesensemapDAO = new RemotesensemapDAO();
    private Consumer<Building> successConsumer = building -> {

        System.out.println("成功之后回调啊");
        dao.update(building);
    };

    private Consumer<Remotesensemap> remotesensemapConsumer = remotesensemap -> {
        remotesensemapDAO.update(remotesensemap);
    };
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("HelloWorld");


        HBox hBox2 = new HBox();
        Button button1 = new Button("上传");
        button1.setOnAction(event -> {
//            File file = new File("/Users/shishifanbuxie/Ezview/caasServer/src/main/resources/data/整理.xlsx");
            singleTaskService("/Users/shishifanbuxie/Downloads/eclipse-SDK-4.7.1-macosx-cocoa-x86_64-distro-3.1.0-SNAPSHOT.tar.gz");
        });

        Button button2 = new Button("上传多个");
        button2.setOnAction(event -> {
            multipleTasksExecutorOnStage();
        });
        hBox2.getChildren().addAll(button1,button2);
        VBox root = new VBox();
        root.setSpacing(5.0);
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(15.0));
        root.getChildren().addAll( hBox2);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }

    public Task<LoginService> task(String fileUrl) {
        Building building = dao.findById(90);
        return new UploadProgressTask("http://127.0.0.1:8080", fileUrl,"上传任务1",building, successConsumer,"1","29","5");
    }
    public Task<LoginService> task2(String fileUrl) {
        Remotesensemap remotesensemap = remotesensemapDAO.findById(1);
        return new UploadProgressTask("http://127.0.0.1:8080", fileUrl,"上传任务1",remotesensemap, remotesensemapConsumer,"111","1");
    }
    public void singleTaskService(String fileUrl) {
        Service<LoginService> service = new Service<LoginService>() {
            @Override
            protected Task<LoginService> createTask() {
                return task(fileUrl);
            }
        };

        ProgressDialog progDiag = new ProgressDialog(service);
        progDiag.setTitle("Progress Dialog Title");
        progDiag.initOwner(primaryStage);
        progDiag.setHeaderText("Header Text");
        progDiag.initModality(Modality.WINDOW_MODAL);

        progDiag.setOnCloseRequest(event -> {
            System.out.println("aaa");
            Platform.runLater(() -> {
                Notifications.create()
                    .title("Information")
                    .text("Task done")
                    .showInformation();
            });
        });

        service.start();

    }

    public void multipleTasksExecutorOnStage() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        List<Task<Void>> tasks = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Task task = task("/Users/shishifanbuxie/Downloads/eclipse-SDK-4.7.1-macosx-cocoa-x86_64-distro-3.1.0-SNAPSHOT.tar.gz");
            executorService.submit(task);
            tasks.add(task);
        }
        TaskProgressView<Task<Void>> view = new TaskProgressView<>();
        //view.setGraphicFactory(t -> new ImageView(new Image(getClass().getResourceAsStream("/icon.png"))));
        view.getTasks().addAll(tasks);


        Stage dialogStage = new Stage();
        dialogStage.setTitle("Tasks");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(view);
        dialogStage.setScene(scene);
        dialogStage.setOnCloseRequest(event -> {
            executorService.shutdownNow();
            dialogStage.hide();
        });

        dialogStage.show();
        executorService.shutdown();
        new Thread(() -> {
            try {
                executorService.awaitTermination(1, TimeUnit.MINUTES);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                Platform.runLater(dialogStage::hide);
            }
        }).start();
    }
}
