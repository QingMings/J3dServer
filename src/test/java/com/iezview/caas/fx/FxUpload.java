package com.iezview.caas.fx;

import com.iezview.caas.http.retrofit.*;
import com.iezview.caas.http.retrofit.body.ProgressInfo;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import okhttp3.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FxUpload extends Application{

    private Stage  primaryStage;
    final FileChooser fileChooser = new FileChooser();
    private OkHttpClient mokHttpClient = ProgressManager.getInstance().with(new OkHttpClient.Builder()).build();

    @Override
    public void start(Stage primaryStage) throws Exception {
        initListener();
        this.primaryStage=primaryStage;
        primaryStage.setTitle("HelloWorld");
        HBox hbox =new HBox();
        TextField  textField = new TextField();
        Button button = new Button();
        button.setText("选择文件");
        button.setOnAction(event -> {
            File file = fileChooser.showOpenDialog(primaryStage);
            if (file != null) {
                textField.setText(file.getAbsolutePath());
            }
        });
        hbox.getChildren().addAll(textField,button);

        HBox hBox2 = new HBox();
        Button button1 =new Button("上传");
        button1.setOnAction(event -> {
            File file = new File("/Users/shishifanbuxie/Downloads/myeclipse-2017-ci-8-offline-installer-windows.exe");
            Request request = new Request.Builder()
                .url("http://localhost:8080/caasServer/upload/10/5")
                .post(RequestBody.create(MediaType.parse("multipart/form-data"), file))
                .build();
            try {
                Response response= mokHttpClient.newCall(request).execute();
                System.out.println(response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        hBox2.getChildren().add(button1);
        VBox root = new VBox();
        root.setSpacing(5.0);
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(15.0));
        root.getChildren().addAll(hbox,hBox2);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }

    private void initListener(){
        ProgressManager.getInstance().addRequestListener("http://localhost:8080/caasServer/uplaod/10/5",getUploadListener());
    }

    private ProgressListener  getUploadListener(){
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                System.out.println(progressInfo);
            }

            @Override
            public void onError(long id, Exception e) {

            }

            @Override
            public void onSuccess(String uuid) {

            }
        };
    }
    private   void TestProgress(){
//        ProgressTask<CaasService>  progressTask = new ProgressTask<CaasService>();


//        progressTask.run();
    }
    public static File writeToFile(InputStream in, File file) throws IOException {
        FileOutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[1024];
        int num = 0;
        while ((num = in.read(buf)) != -1) {
            out.write(buf, 0, buf.length);
        }
        out.close();
        return file;
    }



}
