package com.iezview.caas.fx;

import com.iezview.caas.dao.IUnitDAO;
import com.iezview.caas.daoimpl.UnitDAO;
import com.iezview.caas.entity.Unit;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.List;

public class fxTableViewColorPickerExample extends Application
{ TableView tableView;
    IUnitDAO unitDAO;

    @Override
    public void start(Stage stage) throws Exception {
        tableView = createTableView();
        unitDAO = new UnitDAO();
        StackPane pane = new StackPane();
        pane.getChildren().add(tableView);
        Scene scene = new Scene(pane, 400.0, 400.0);
        stage.setScene(scene);
        stage.setTitle("测试 table propertyValueFactory ");
        stage.show();
        tableView.getItems().addAll(mockPersonItems());
    }

    private TableView createTableView() {
        TableView tableView = new TableView();

        TableColumn idColumn = new TableColumn("Id");
        idColumn.setCellValueFactory(new PropertyValueFactory("id"));
        idColumn.setMinWidth(80);
        TableColumn nameColumn = new TableColumn("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory("uname"));
        nameColumn.setMinWidth(150);
        TableColumn cityColumn = new TableColumn("ShortName");
        cityColumn .setCellValueFactory(new PropertyValueFactory("shortname"));
        cityColumn.setMinWidth(150);
        TableColumn stateColumn = new TableColumn("Area");
        //显示Unit里面的Area的 ID
        stateColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Unit, String>, ObservableValue>) param -> new SimpleStringProperty(param.getValue().getArea().getAname()));
        tableView.getColumns().addAll(idColumn, nameColumn, cityColumn,stateColumn);
        return tableView;
    }

    private List<Unit> mockPersonItems() {
        return unitDAO.findAll();
    }
}
