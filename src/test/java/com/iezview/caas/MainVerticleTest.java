package com.iezview.caas;

import com.iezview.caas.http.client.CaasService;
import com.iezview.caas.http.retrofit.LoginService;
import com.iezview.caas.http.retrofit.ProgressListener;
import com.iezview.caas.http.retrofit.body.ProgressInfo;
import com.iezview.caas.http.retrofit.body.ProgressResponseBody;
import com.iezview.caas.utils.ResourcesUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.Timeout;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClient;
import okhttp3.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.util.EntityUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RunWith(VertxUnitRunner.class)
public class MainVerticleTest implements ProgressListener {

    @Rule
    public Timeout rule = Timeout.seconds(600);
    private Vertx vertx;
    private Retrofit retrofit;
    private LoginService loginService;

    @Before
    public void init(TestContext context) {
        vertx = Vertx.vertx();
//        retrofit =new Retrofit.Builder().baseUrl("http://localhost:8080").build();
//         loginService = retrofit.create(LoginService.class);
//        DeploymentOptions options = new DeploymentOptions();
//        options.setConfig(Buffer.buffer("{\"server\" : {\"address\" : \"localhost\", \"port\" : 8080, \"rootStoragePath\" : \"/Users/shishifanbuxie/Office/storage\"}, \"storageStructure\" : {\"unitKey\" : \"院所本部\", \"baseUnitKey\" : \"下属基地\", \"subStructure\" : {\"remoteSensingImage\" : \"遥感图\", \"planningImage\" : \"规划图\", \"liveModel\" : \"实景模型\", \"planningModel\" : \"规划模型\", \"buildingImage\" : \"建筑图片\", \"planningTable\" : \"规划汇总表\"} } }").toJsonObject());
//        vertx.deployVerticle(MainVerticle.class.getName(), options,context.asyncAssertSuccess());
    }

    @Test
    public void test_Application(TestContext context) {
        Async async = context.async();
        WebClient client = WebClient.create(vertx);
        client.get(8080, "localhost", "/caasServer").send(ar -> {
            if (ar.succeeded()) {
                Assert.assertTrue(ar.result().bodyAsString().contains("caasServer"));
                async.complete();
            } else {
                context.fail(ar.cause());
            }
        });

    }


    @Test
    public void test_httpBaidu(TestContext context) {
        Async async = context.async();
        WebClient client = WebClient.create(vertx);
        client.get("www.baidu.com", "/").send(ar -> {
            if (ar.succeeded()) {
                System.out.println(ar.result().statusCode());
                System.out.println(ar.result().bodyAsString());
                async.complete();
            } else {
                context.fail(ar.cause());
            }

        });
    }

    //    @Test
    public void test_ResourcesUtils(TestContext context) {
        Async async = context.async();
        ResourcesUtils.getInstance().generateSubForder(Paths.get("com/forder/"));
        async.complete();
    }

    /**
     * 登录接口测试
     *
     * @param ctx
     */
    @Test
    public void test_userLogin(TestContext ctx) {
        Async async = ctx.async();
        WebClient client = WebClient.create(vertx);
        MultiMap form = MultiMap.caseInsensitiveMultiMap();
        form.set("username", "qingmings");
        form.set("password", "123456");
        client.post(8080, "localhost", "/caasServer/user/login").sendForm(form, ar -> {
            if (ar.succeeded()) {
                JsonObject result = ar.result().bodyAsJsonObject();
                if (result.getString("result").equals("1")) {
                    System.out.println("登录成功");
                    System.out.println(result.encode());
                } else {
                    System.out.println("登录失败:" + result.getString("resultMessage"));
                }
                async.complete();
            } else {
                ctx.fail("请求失败");
            }
        });
    }

    /**
     * 测试 研究所 上传接口
     * * @param context
     *
     * @param context
     * @url IP:port/upload/:unitId/:typeId
     * @ip ip地址
     * @port 端口
     * @upload 上下文环境
     * @:unitId 研究所ID
     * @:typeId 上传类型
     */
    @Test
    public void test_uploadYanJiuSuo(TestContext context) {
        Async async = context.async();
        HttpClientContext httpClientContext = new HttpClientContext();
        String uploadfilePath = "/Users/shishifanbuxie/Ezview/caasServer/src/main/resources/data/整理.xlsx";
        CloseableHttpAsyncClient client = HttpAsyncClients.createDefault();
        client.start();
        HttpPost httpPost = new HttpPost("http://localhost:8080/caasServer/upload/8/5");
        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
            .setCharset(Charset.forName("UTF-8"));
        try {
            multipartEntityBuilder.addBinaryBody("file", new FileInputStream(new File(uploadfilePath)), ContentType.DEFAULT_BINARY, "整理.xlsx");
            HttpEntity httpEntity = multipartEntityBuilder.build();
            httpPost.setEntity(new BufferedHttpEntity(httpEntity));
            client.execute(httpPost, httpClientContext, new FutureCallback<HttpResponse>() {
                @Override
                public void completed(HttpResponse httpResponse) {
                    try {
                        String content = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                        System.out.println(content);
                        JsonObject result = Buffer.buffer(content).toJsonObject();
                        context.assertTrue(result.getString("result").equals("1"));
                        async.complete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failed(Exception e) {
                    context.fail(e);
                }

                @Override
                public void cancelled() {

                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 测试基地 上传接口
     *
     * @param context
     * @url IP:port/upload/:unitId/:unitId2/:typeId
     * @ip ip地址
     * @port 端口
     * @upload 上下文环境
     * @:unitId 研究所ID
     * @:unitId2 基地ID
     * @:typeId 上传类型
     */
    @Test
    public void test_uploadJiDi(TestContext context) {
        Async async = context.async();
        HttpClientContext httpClientContext = new HttpClientContext();
        String uploadfilePath = "/Users/shishifanbuxie/Ezview/caasServer/src/main/resources/data/整理.xlsx";
        CloseableHttpAsyncClient client = HttpAsyncClients.createDefault();
        client.start();
        HttpPost httpPost = new HttpPost("http://localhost:8080/caasServer/upload/7/71/5");
        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
            .setCharset(Charset.forName("UTF-8"));
        try {
            multipartEntityBuilder.addBinaryBody("file", new FileInputStream(new File(uploadfilePath)), ContentType.DEFAULT_BINARY, "整理.xlsx");
            HttpEntity httpEntity = multipartEntityBuilder.build();
            httpPost.setEntity(new BufferedHttpEntity(httpEntity));
            client.execute(httpPost, httpClientContext, new FutureCallback<HttpResponse>() {
                @Override
                public void completed(HttpResponse httpResponse) {
                    try {
                        String content = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                        System.out.println(content);
                        JsonObject result = Buffer.buffer(content).toJsonObject();
                        context.assertTrue(result.getString("result").equals("1"));
                        async.complete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failed(Exception e) {
                    context.fail(e);
                }

                @Override
                public void cancelled() {

                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 测试 研究所 上传接口 (大文件上传)
     * * @param context
     *
     * @param context
     * @url IP:port/upload/:unitId/:typeId
     * @ip ip地址
     * @port 端口
     * @upload 上下文环境
     * @:unitId 研究所ID
     * @:typeId 上传类型
     */
    @Test
    public void test_largeFileUpload(TestContext context) {
        Async async = context.async();
        HttpClientContext httpClientContext = new HttpClientContext();
        String uploadfilePath = "/Users/shishifanbuxie/Downloads/myeclipse-2017-ci-8-offline-installer-macosx.dmg";
//        String uploadfilePath = "/Users/shishifanbuxie/Ezview/caasServer/src/main/resources/data/整理.xlsx";
        CloseableHttpAsyncClient client = HttpAsyncClients.createDefault();
        client.start();
        HttpPost httpPost = new HttpPost("http://localhost:8080/caasServer/upload/8/5");
        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
            .setCharset(Charset.forName("UTF-8"));
        try {
            multipartEntityBuilder.addBinaryBody("file", new FileInputStream(new File(uploadfilePath)), ContentType.APPLICATION_OCTET_STREAM, "ubuntu-16.04-desktop-amd64.iso");
            HttpEntity httpEntity = multipartEntityBuilder.build();
//            HttpEntity httpEntity =new FileEntity(new File(uploadfilePath));
            httpPost.setEntity(new BufferedHttpEntity(httpEntity));
            client.execute(httpPost, httpClientContext, new FutureCallback<HttpResponse>() {
                @Override
                public void completed(HttpResponse httpResponse) {
                    try {
                        String content = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                        System.out.println(content);
                        JsonObject result = Buffer.buffer(content).toJsonObject();
                        context.assertTrue(result.getString("result").equals("1"));
                        async.complete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failed(Exception e) {
                    context.fail(e);
                }

                @Override
                public void cancelled() {

                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * retrofit 方式 get方法
     *
     * @param context
     */
    @Test
    public void test_getRoot(TestContext context) {
        Async async = context.async();


        Call<ResponseBody> call = loginService.hello();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    System.out.println(response.body().string());
                    async.complete();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                context.fail(throwable);

            }
        });

    }

    /**
     * retrofit 方式 post form   登录方法
     *
     * @param context
     */
    @Test
    public void test_retrofit_post_login(TestContext context) {
        Async async = context.async();
        Call<ResponseBody> call = loginService.login("qingmings", "123456");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    System.out.println(response.body().string());
                    async.complete();
                } catch (IOException e) {
                    context.fail(e.getCause());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                context.fail(throwable);
            }
        });

    }

    /**
     * retrofit 方式 post form   上传文件
     *
     * @param context
     */
    @Test
    public void test_retrofit_post_uploadYanJiuSuo(TestContext context) {

        Async async = context.async();
        MediaType textType = MediaType.parse("text/plain");
        RequestBody name = RequestBody.create(textType, "怪盗kidou");
        RequestBody age = RequestBody.create(textType, "24");
        File file1 = new File("/Users/shishifanbuxie/Ezview/caasServer/src/main/resources/data/整理.xlsx");
        RequestBody file = RequestBody.create(MediaType.parse("application/octet-stream"), file1);
//        ResponseBody.create(MediaType.parse("application/octet-stream"),new File(""));

        // 演示 @Multipart 和 @Part
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file1.getName(), file);
        Call<ResponseBody> call = loginService.uplaodYanJiuSuo("10", "5", filePart);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    System.out.println(response.body().string());
                    async.complete();
                } catch (IOException e) {
                    context.fail(e.getCause());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                context.fail(throwable);
            }
        });


    }

    @Test
    public void testDownloadFile(TestContext context) {
        List<ProgressListener> listeners = new ArrayList<>();
        listeners.add(this);
        Async async = context.async();
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(chain -> {
            okhttp3.Response response = chain.proceed(chain.request());
            return response.newBuilder().body(new ProgressResponseBody(response.body(), listeners, 150)).build();
        });
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://localhost:8080/caasServer/").client(client.build()).build();
        CaasService service = retrofit.create(CaasService.class);
        Call<ResponseBody> call = service.downloadYanJiuSuo("5", "5", "221");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        System.out.println(response.body().contentType());
                        System.out.println(response.body().contentLength());

                        writeResponseBodyToDisk(response.body(),"/Users/shishifanbuxie/Office/a.png");
                        async.complete();
                    } catch (Exception e) {
                        context.fail(e.getCause());
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("shibai");
                context.fail("失败");
            }
        });
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Override
    public void onSuccess(String uuid) {

    }

    @Override
    public void onProgress(ProgressInfo progressInfo) {
        System.out.println(progressInfo.toString());
    }

    @Override
    public void onError(long id, Exception e) {

    }

    /**
     * 将文件写入本地
     *
     * @param body http响应体
     * @param path 保存路径
     * @return 保存file
     */
    private File writeResponseBodyToDisk(ResponseBody body, String path) {

        File futureStudioIconFile = null;
        try {

            futureStudioIconFile = new File(path);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                }

                outputStream.flush();

                return futureStudioIconFile;
            } catch (IOException e) {
                return futureStudioIconFile;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return futureStudioIconFile;
        }
    }
}
