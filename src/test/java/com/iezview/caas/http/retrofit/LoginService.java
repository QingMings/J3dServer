package com.iezview.caas.http.retrofit;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface LoginService {
    @GET("/caasServer/")
    Call<ResponseBody>  hello();
    @POST("/caasServer/user/login")
    @FormUrlEncoded
    Call<ResponseBody>  login(@Field("username") String username,@Field("password") String password);

    @POST("/caasServer/upload/{unitId}/{typeId}")
    @Multipart
    Call<ResponseBody>  uplaodYanJiuSuo(@Path("unitId") String unitId, @Path("typeId") String typeId, @Part MultipartBody.Part file);
}
