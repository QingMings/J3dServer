package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.AreaDAO;
import com.iezview.caas.daoimpl.BuildingDAO;
import com.iezview.caas.daoimpl.SubjectsDAO;
import com.iezview.caas.daoimpl.UnitDAO;
import com.iezview.caas.entity.Area;
import com.iezview.caas.entity.Building;
import com.iezview.caas.entity.Subjects;
import com.iezview.caas.entity.Unit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * builddao  test
 *
 * @author qingmings
 */
public class BuildDaoTest {

    private IBuildingDAO buildingDAO;
    private IUnitDAO unitDAO;
    private IAreaDAO areaDAO;
    private  ISubjectsDAO subjectsDAO;

    @Before
    public void initDao() {
        buildingDAO = new BuildingDAO();
        unitDAO = new UnitDAO();
        areaDAO = new AreaDAO();
        subjectsDAO = new SubjectsDAO();
    }

    @Test
    public void insertData() {
        Building building = new Building();
        List<Unit> units = unitDAO.findUnitByNameKeyWord("垃圾", 0, 1);
        Unit u = units.get(0);
        if (u == null) {
            Assert.fail("Unit  查询结果为空");
            return;
        }
        building.setUnit(u);
        building.setNumber("1");
        building.setBname("垃圾学院2号办公楼");
        building.setResourcepath("/build/jikexueyuan");
        building.setBtype("建筑类型");
        building.setYears(2007);
        building.setHeight(55.45);
        building.setPurpose("办公楼");
        building.setTotalarea(10000.34);
        building.setAbovearea(8000.0);
        building.setUnderarea(2000.0);
        building.setConstructionunits("中铁二局第三项目部");
        building.setCentrallng(45.45);
        building.setCentrallat(45.45);
        building.setSynopsis("这是垃圾学院的一号办公楼,占地面积XXX,总面积XXXX,是一座办公楼");
        buildingDAO.save(building);
    }

    /**
     * 测试 findBuildingByNameKeyWord 模糊查询
     */
    @Test
    public void Test_findBuildingsByNameKeyWord() {

        List<Building> buildings = buildingDAO.findBuildingByNameKeyWord("试验");
        if (buildings == null) {
            Assert.fail("木有查到 \"一号办公楼\"");
            return;
        }

        Assert.assertTrue("木有查到 \"一号办公楼\"", buildings.size()>0);
    }

//    @Test
    public void Test_delete() {
        List<Building> buildings = buildingDAO.findBuildingByNameKeyWord("号办公");
        if (buildings == null) {
            Assert.fail("木有查到 \"一号办公楼\"");
            return;
        }
        buildingDAO.delete(buildings.get(0));
    }

    /**
     * 测试  findBuildingsCountByNameKeyWord  模糊查询行数
     */
    @Test
    public void Test_findBuildingsCountByNameKeyWord() {
        Long count = buildingDAO.findBuildingCountByNameKeyWord("1号办公");
        System.out.println(count);
    }

    /**
     * 测试 findall
     */
    @Test
    public void Test_findAll() {
        List<Building> buildings = buildingDAO.findAll();
        buildings.stream().forEach(building -> System.out.println(building));
    }

    /**
     * 测试  根据单位查询建筑
     */
    @Test
    public void Test_findByUnit() {
        List<Unit> units = unitDAO.findByUname("极客学院");
        List<Building> buildings = buildingDAO.findBuildingByUnit(units.get(0));
        System.out.println(buildings.size());
    }

    /**
     * 按院所名称关键字查询下属建筑
     */
    @Test
    public  void Test_findBuildingByUnitNamekeyWord(){
        List<Building>  buildings = buildingDAO.findBuildingByUnitNameKeyWord("垃圾");
        System.out.println(buildings.size());
    }

    /**
     * 测试 按院所名称关键字查询下属建筑总行数
     */
    @Test
    public  void Test_findBuildingCountByUnitNameKeyWork(){
        Long count  = buildingDAO.findBuildingCountByUnitNameKeyWord("垃圾");
        System.out.println(count);
    }


    @Test
    public  void test_saveAll(){
        List<Building> buildings = new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            Building building = new Building();
            List<Unit> units = unitDAO.findUnitByNameKeyWord("垃圾", 0, 1);
            Unit u = units.get(0);
            if (u == null) {
                Assert.fail("Unit  查询结果为空");
                return;
            }
//            if (i!=2){
            building.setUnit(u);
//            }
            building.setNumber("1");
            building.setBname("batch");
            building.setResourcepath("/build/jikexueyuan");
            building.setBtype("建筑类型");
            building.setYears(2007);
            building.setHeight(55.45);
            building.setPurpose("办公楼");
            building.setTotalarea(10000.34);
            building.setAbovearea(8000.0);
            building.setUnderarea(2000.0);
            building.setConstructionunits("中铁二局第三项目部");
            building.setCentrallng(45.45);
            building.setCentrallat(45.45);
            building.setSynopsis("这是垃圾学院的一号办公楼,占地面积XXX,总面积XXXX,是一座办公楼");
            buildings.add(building);
        }
        buildingDAO.saveAll(buildings);
    }


    @Test
    public void test_findBuildingByArea(){
        List<Area> area =  areaDAO.findAreaByNameKeyWord("北京");
        List<Building>  buildings = buildingDAO.findBuildingByArea(area.get(0));
         buildings.forEach(building -> {
             System.out.println(building.getBname()  +":" +building.getUnit().getArea().getAname());
         });
    }
    @Test
    public void test_findBuildingBySubject(){
        List<Subjects> subjects = subjectsDAO.findSubjectsByNameKeyWord("放射");
        List<Building> buildings =buildingDAO.findBuildingBySubject(subjects.get(0));
//            buildings.forEach(building -> {
//                System.out.println(building.getBname()+":"+building.getId());
////                building.getUnit().getSubjects().forEach(subjects1 -> System.out.println(subjects1.getSname()));
//            });
        System.out.println(buildings.size());
    }
    @Test
    public void test_findBuildingCountBySubject(){
        List<Subjects> subjects = subjectsDAO.findSubjectsByNameKeyWord("放射");
        Long count = buildingDAO.findBuildingCountBySubject(subjects.get(0));
        System.out.println(count);
    }
}
