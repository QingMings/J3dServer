package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.PlanmapDAO;
import com.iezview.caas.daoimpl.UnitDAO;
import com.iezview.caas.entity.Planmap;
import com.iezview.caas.entity.Unit;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * 规划图单元测试
 */
public class PlanmapDAOTest {


    private IPlanmapDAO planmapDAO;
    private IUnitDAO unitDAO;

    @Before
    public void init(){
        planmapDAO = new PlanmapDAO();
        unitDAO = new UnitDAO();
    }
    @Test
    public void test_insert(){
       List<Unit> units =unitDAO.findUnitByNameKeyWord("垃圾");
       if (units!=null){
       Unit unit = units.get(0);
           Planmap  planmap = new Planmap();
           planmap.setUnit(unit);
           planmap.setPname("规划图名称3");
           planmap.setResourcepath("/test/planmap/规划图测试/");
           planmapDAO.save(planmap);
       }
    }
    @Test
    public void test_findAll(){
        List<Planmap>  planmaps =  planmapDAO.findAll(0,20);
        planmaps.forEach(planmap -> System.out.println(planmap));
    }
    @Test
    public void test_findPlanmapByKeyWord(){
        List<Planmap> planmaps = planmapDAO.findPlanmapByNameKeyWord("规划");
         planmaps.forEach(planmap -> System.out.println(planmap));
    }
    @Test
    public void test_findPlanmapCountByKeyWord(){
        Long count = planmapDAO.findPlanmapCountByNameKeyWord("规划");
        System.out.println(count);
    }

    @Test
    public void test_count(){
        Long count =planmapDAO.count();
        System.out.println(count);
    }
}
