package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.SubjectsDAO;
import com.iezview.caas.daoimpl.UnitDAO;
import com.iezview.caas.entity.Subjects;
import com.iezview.caas.entity.Unit;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SubjectsDaoTest {

    private ISubjectsDAO subjectsDAO;
    private IUnitDAO unitDAO;
    @Before
    public void init(){
            subjectsDAO =new  SubjectsDAO();
            unitDAO = new UnitDAO();
    }

    /**
     * 测试插入
     */
    @Test
    public  void test_Insert(){
        Subjects subjects = new Subjects();
        subjects.setSname("放射科");
        subjectsDAO.save(subjects);


    }

    /**
     * 测试查询
     */
    @Test
    public void  test_findAll(){
       List<Subjects> subjectsList= subjectsDAO.findAll(0,20);
       subjectsList.forEach(subjects -> System.out.println(subjects.toString()));
    }

    /**
     * 测试名称关键字查询
     */
    @Test
    public void  test_findSubjectsByNameKeyWord(){
        List<Subjects> subjects =subjectsDAO.findSubjectsByNameKeyWord("血液");
        subjects.forEach(subjects1 -> System.out.println(subjects1.toString()));
    }

    /**
     * 测试获取总行数
     */
    @Test
    public void test_count(){
            Long count = subjectsDAO.count();
        System.out.println(count);
    }

    /**
     * 名称关键字总行数
     */
    @Test
    public void test_findSubjectsCountByNameKeyWord(){
        Long count = subjectsDAO.findSubjectsCountByNameKeyWord("血液");
        System.out.println(count);
    }

    @Test
    public void test_insertSubjectForUnit(){
        List<Subjects> subjects=   subjectsDAO.findAll();

         List<Unit> units =  unitDAO.findAll(20);
         subjects.forEach(subjects1 -> {
             subjects1.getUnits().addAll(units);
             subjectsDAO.save(subjects1);
         });


    }
}
