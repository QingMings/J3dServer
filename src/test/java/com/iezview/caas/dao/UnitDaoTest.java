package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.*;
import com.iezview.caas.entity.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class UnitDaoTest {

    private IUnitDAO unitdao;
    private IAreaDAO areaDAO;
    private ISubjectsDAO subjectsDAO;
    private IBuildingDAO buildingDAO;
    private IRemotesensemapDAO remotesensemapDAO;

    @Before
    public void initDao() {
        unitdao = new UnitDAO();
        areaDAO = new AreaDAO();
        subjectsDAO = new SubjectsDAO();
        buildingDAO = new BuildingDAO();
        remotesensemapDAO = new RemotesensemapDAO();
    }

    /**
     * 测试  findAll 方法
     */
    @Test
    public void queryData() {
        List<Unit> units = unitdao.findAll(0, 20);
        units.stream().forEach(unit -> System.out.println(unit));

    }

    @Test
    public void test_query_one_to_many_buildings() {
        Unit unit = unitdao.findById(6);
        Set<Building> buildings = unit.getBuildings();
        System.out.println(buildings.size());
        Building building = new Building();

        building.setUnit(unit);
        building.setNumber("1");
        building.setBname("垃圾学院2号办公楼");
        building.setResourcepath("/build/jikexueyuan");
        building.setBtype("建筑类型");
        building.setYears(2007);
        building.setHeight(55.45);
        building.setPurpose("办公楼");
        building.setTotalarea(10000.34);
        building.setAbovearea(8000.0);
        building.setUnderarea(2000.0);
        building.setConstructionunits("中铁二局第三项目部");
        building.setCentrallng(45.45);
        building.setCentrallat(45.45);
        building.setSynopsis("这是垃圾学院的一号办公楼,占地面积XXX,总面积XXXX,是一座办公楼");
        buildingDAO.save(building);
        unit.getBuildings().clear();
//        EntityManagerHelper.getEntityManager().refresh(unit);
        Set<Building> buildings1 = unit.getBuildings();
        System.out.println(buildings1.size());

    }

    @Test
    public void test_query_one_to_many_remotesensemaps() {
        Unit unit = unitdao.findById(6);
        Set<Remotesensemap> remotesensemaps = unit.getRemotesensemaps();
//        remotesensemaps.forEach(building -> System.out.println(remotesensemaps));
        System.out.println(remotesensemaps.size());
        Remotesensemap remotesensemap = new Remotesensemap();
        remotesensemap.setUnit(unit);
        remotesensemap.setSname("遥感图单位名称1");
        remotesensemap.setMaxlng(45.0);
        remotesensemap.setMaxlat(45.0);
        remotesensemap.setMinlng(20.0);
        remotesensemap.setMinlat(20.0);
        remotesensemap.setResourcepath("/test/remotesensemap/遥感图/");
        remotesensemapDAO.save(remotesensemap);
        EntityManagerHelper.getEntityManager().refresh(unit);
        Set<Remotesensemap> remotesensemaps1 = unit.getRemotesensemaps();
        System.out.println(remotesensemaps1.size());
    }

    @Test
    public void test_query_one_to_many_planmap() {
        Unit unit = unitdao.findById(6);
        Set<Planmap> planmaps = unit.getPlanmaps();
        planmaps.forEach(building -> System.out.println(planmaps));
    }

    @Test
    public void test_query_one_to_many_planmodels() {
        Unit unit = unitdao.findById(6);
        Set<Planmodel> planmodels = unit.getPlanmodels();
        planmodels.forEach(building -> System.out.println(planmodels));
    }

    @Test
    public void test_query_one_to_many_obliquemodels() {
        Unit unit = unitdao.findById(6);
        Set<Obliquemodel> obliquemodels = unit.getObliquemodels();
//        obliquemodels.forEach(building -> System.out.println(obliquemodels));
        System.out.println(obliquemodels.size());
        Obliquemodel  obliquemodel = new Obliquemodel();
        obliquemodel.setName("倾斜摄影模型2");
        obliquemodel.setCentrallat(45.0);
        obliquemodel.setCentrallng(45.0);
        obliquemodel.setCentralheight(45.0);
        obliquemodel.setDataformat("不知道");
        obliquemodel.setAcquisitiontime(LocalDateTime.now());
        obliquemodel.setAltitudemode(2.0);
        obliquemodel.setHeading(2.0);
        obliquemodel.setTilt(2.0);
        obliquemodel.setRoll(2.0);
        obliquemodel.setXscale((short)2.0);
        obliquemodel.setYscale((short)2.0);
        obliquemodel.setZscale((short)2.0);
        obliquemodel.setTiletype("不知道");
        obliquemodel.setTilemin((short) 2.0);
        obliquemodel.setTilemax((short) 4.0);
        obliquemodel.setTilequantity(2);
        obliquemodel.setResourcepath("/test/obliquemodel/01/");
        obliquemodel.setDescription("倾斜摄影模型描述");
        obliquemodel.setIsload(0);
        obliquemodel.getUnits().add(unit);
        EntityManagerHelper.getEntityManager().refresh(unit);

    }

    @Test
    public void test_query_one_to_many_projectsummaries() {
        Unit unit = unitdao.findById(6);
        Set<Projectsummary> projectsummaries = unit.getProjectsummaries();
        projectsummaries.forEach(building -> System.out.println(projectsummaries));
    }


    /**
     * 测试 findUnitByNameKeyWord
     */
    @Test
    public void Test_findUnitByNameKeyWord() {
        List<Unit> units = unitdao.findUnitByNameKeyWord("极客", 0, 20);

        Assert.assertTrue("没有查到 Uname =\"极客\" 的数据", units.size() > 0);
    }
    /**
     *  test   build  subjects strings
     */
//    @Test
//    public void   buildSubjectsString(){
//         List<String> subjectList=new ArrayList<>();
//        for (int i = 0; i <10 ; i++) {
//            subjectList.add("subject"+i);
//        }
//         String subjectsString = unitdao.setSubjectsString(subjectList);
//        System.out.println(subjectsString);
//
//    }

    /**
     * test insert unit to  Table@Unit
     */
    @Test
    public void insertUnit() {
        List<Subjects> subjects = subjectsDAO.findSubjectsByNameKeyWord("放射科");
        Unit unit = new Unit();
        unit.setParent(0);
        unit.setUname("医3学院");
        unit.setShortname("医3学院");
        unit.setKeyname("医3学院");
        unit.setUtype("类型");
        Area area = areaDAO.findById(6);
        unit.setArea(area);


        unit.getSubjects().add(subjects.get(0));
        unit.setCentrallng(45.45);
        unit.setCentrallat(45.45);
        unit.setCoveragearea(45.45);
        unit.setUaddress("北京昌平区");
        unit.setSynopsis("这是一个北京昌平区的小研究所");
        unit.setUlevel(1);
        unit.setOrderby(0);
//        unit.setNodekind();

        unitdao.save(unit);

    }

    /**
     * 测试插入基地
     * savebase方法 如果父级不为0，设置父级NodeLink 字段为null ,并设置基地Ulevel=2
     */
    @Test
    public void Test_insertbase() {
//        unitdao.findUnitByParentNameKeyWord("极客")
        List<Subjects> subjects = subjectsDAO.findSubjectsByNameKeyWord("放射");
        Unit unit = new Unit();
        List<Unit> units = unitdao.findUnitByNameKeyWord("医3学");
        Unit parent = units.get(0);
        unit.setParent(parent.getId());
        unit.setUname("冒牌学院下的基地三");
        unit.setShortname("冒牌学院下的基地三");
        unit.setKeyname("冒牌学院下的基地三");
        unit.setUtype("类型");
        Area area = areaDAO.findById(5);
        unit.setArea(area);


        unit.getSubjects().add(subjects.get(0));
        unit.setCentrallng(45.45);
        unit.setCentrallat(45.45);
        unit.setCoveragearea(45.45);
        unit.setUaddress("北京昌平区");
        unit.setSynopsis("这是一个北京昌平区的小研究所下的基地三");
        unit.setUlevel(2);
        unit.setOrderby(0);
        unitdao.saveBase(unit);
    }

    @Test
    public void Test_findUnitByArea() {
        List<Area> areas = areaDAO.findByAname("北京市");
        List<Unit> units = unitdao.findUnitByArea(areas.get(0));
        System.out.println(units.size());
    }

    //    @Test
    public void Test_delete() {
        List<Unit> units = unitdao.findAll(0, 10);
        if (units == null) {
            Assert.fail("没有查到单位");
        }
        unitdao.delete(units.get(0));
    }

    /**
     * 测试按照院所名称查询下属基地
     */
    @Test
    public void Test_findUnitByParentNameKeyWord() {
        List<Unit> units = unitdao.findUnitByParentNameKeyWord("垃圾");
        System.out.println(units.size());
    }

    /**
     * 测试 按院所名称查询下属基地的总行数
     */
    @Test
    public void Test_findUnitCountByParentNameKeyWord() {
        Long count = unitdao.findUnitCountByParentNameKeyWord("垃圾");
        System.out.println(count);
        Assert.assertTrue(count > 0);
    }

    /**
     * 测试 检查 order
     * insert
     */
    @Test
    public void Test_checkUnitOrderInsert() {
        List<Subjects> subjects = subjectsDAO.findSubjectsByNameKeyWord("血液");
        Unit unit = new Unit();
        unit.setParent(8);
        unit.setUname("极客学院");
        unit.setShortname("极客学院");
        unit.setKeyname("极客学院");
        unit.setUtype("类型");
        Area area = areaDAO.findById(2);
        unit.setArea(area);


        unit.getSubjects().add(subjects.get(0));
        unit.setCentrallng(45.45);
        unit.setCentrallat(45.45);
        unit.setCoveragearea(45.45);
        unit.setUaddress("北京昌平区");
        unit.setSynopsis("这是一个北京昌平区的小研究所");
        unit.setUlevel(1);
        unit.setOrderby(0);
        List<Unit> units = unitdao.checkOrder(unit);
        System.out.println(units.size());
        units.forEach(unit1 -> System.out.println(unit1.getId() + unit1.getUname()));

    }

    /**
     * 检查排序值，如果更新一个已经有的单位，检查出了它本身之外的所有的同级单位，是否orderby 值重复
     * 结果大于0标识重复
     */
    @Test
    public void checkUnitOrderUpdate() {
        List<Unit> units = unitdao.findByUname("垃圾学院");
        if (units.size() > 0) {
            Unit unit = units.get(0);
            List<Unit> units1 = unitdao.checkOrder(unit);
            System.out.println(units1.size());
        }
    }


    @Test
    public void testfindNull() {
        List<Unit> units = unitdao.findByParent(100);
        System.out.println(units);
    }

    @Test
    public void test_findUnitNotInculed() {
        List<Unit> notinculedUnits = unitdao.findUnitByNameKeyWord("极客");

        List<Unit> units = unitdao.findUnitNotIncluded(notinculedUnits);
        units.forEach(unit -> System.out.println(unit.getUname()));
    }

    @Test
    public void test_UnitParentDefaultValue() {
        List<Subjects> subjects = subjectsDAO.findSubjectsByNameKeyWord("放射科");
        Unit unit = new Unit();
//        unit.setParent(0);
        unit.setUname("医一学院");
        unit.setShortname("医3学院");
        unit.setKeyname("医3学院");
        unit.setUtype("类型");
        Area area = areaDAO.findById(6);
        unit.setArea(area);


        unit.getSubjects().add(subjects.get(0));
        unit.setCentrallng(45.45);
        unit.setCentrallat(45.45);
        unit.setCoveragearea(45.45);
        unit.setUaddress("北京昌平区");
        unit.setSynopsis("这是一个北京昌平区的小研究所");
        unit.setUlevel(1);
        unit.setOrderby(0);
//        unit.setNodekind();

        unitdao.save(unit);
    }

    @Test
    public void test_findUnitByMultipleonditions() {
        unitdao.findUnitByMultipleonditions(1, null, "aaa", new Area(), null, "aaa");
    }
}
