package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.AreaDAO;
import com.iezview.caas.entity.Area;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * test query area data
 * @author shishifanbuxie
 */
public class AreaDaoTest {

    private IAreaDAO  areaDAO;

    @Before
    public void initAreaDao(){

        areaDAO = new AreaDAO();
    }
    @Test
    public  void queryData(){
        List<Area>  areas =areaDAO.findAll(1,20);
        areas.stream().forEach(area-> System.out.println(area.getId()));
    }

    public void queryCount(){

    }
}
