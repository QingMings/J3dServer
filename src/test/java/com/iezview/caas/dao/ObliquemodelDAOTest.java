package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.ObliquemodelDAO;
import com.iezview.caas.daoimpl.UnitDAO;
import com.iezview.caas.entity.Obliquemodel;
import com.iezview.caas.entity.Unit;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 倾斜摄影模型单元测试
 */
public class ObliquemodelDAOTest {

    private IObliquemodelDAO obliquemodelDAO;
    private IUnitDAO unitDAO;

    @Before
    public void init(){

        obliquemodelDAO = new ObliquemodelDAO();
        unitDAO = new UnitDAO();
    }

    /**
     * 测试插入 倾斜摄影模型
     * 测试 单位和  倾斜摄影模型多对多插入 中间表
     */
    @Test
    public void test_insert(){
        List<Unit> units = unitDAO.findUnitByNameKeyWord("垃圾");

        Unit unit = units.get(0);

        Obliquemodel  obliquemodel = new Obliquemodel();
            obliquemodel.setName("倾斜摄影模型2");
            obliquemodel.setCentrallat(45.0);
            obliquemodel.setCentrallng(45.0);
            obliquemodel.setCentralheight(45.0);
            obliquemodel.setDataformat("不知道");
            obliquemodel.setAcquisitiontime(LocalDateTime.now());
            obliquemodel.setAltitudemode(2.0);
            obliquemodel.setHeading(2.0);
            obliquemodel.setTilt(2.0);
            obliquemodel.setRoll(2.0);
            obliquemodel.setXscale((short)2.0);
            obliquemodel.setYscale((short)2.0);
            obliquemodel.setZscale((short)2.0);
            obliquemodel.setTiletype("不知道");
            obliquemodel.setTilemin((short) 2.0);
            obliquemodel.setTilemax((short) 4.0);
            obliquemodel.setTilequantity(2);
            obliquemodel.setResourcepath("/test/obliquemodel/01/");
            obliquemodel.setDescription("倾斜摄影模型描述");
            obliquemodel.setIsload(0);
            obliquemodel.getUnits().add(unit);
            obliquemodelDAO.save(obliquemodel);
//            unit.getObliquemodels().add(obliquemodel);
//            unitDAO.update(unit);
    }

    @Test
    public void test_findAll(){
        List<Obliquemodel> obliquemodels = obliquemodelDAO.findAll();
        obliquemodels.forEach(obliquemodel -> System.out.println(obliquemodel));
    }

    /**
     * 测试删除 同时删除中间表数据
     */
//    @Test
    public void test_delete(){
        List<Obliquemodel> obliquemodels = obliquemodelDAO.findByName("倾斜摄影模型2");
         obliquemodelDAO.delete(obliquemodels.get(0));
    }

    @Test
    public void test_findObliquemodelByNameKeyWord(){
        List<Obliquemodel> obliquemodels = obliquemodelDAO.findObliquemodelByNameKeyWord("倾斜");
        obliquemodels.forEach(obliquemodel -> System.out.println(obliquemodel));
    }

    @Test
    public void test_findObliquemodelCountByNameKeyWord(){
        Long count = obliquemodelDAO.findObliquemodelCountByNameKeyWord("倾斜");
        System.out.println(count);
    }

    @Test
    public void test_count(){
        Long count = obliquemodelDAO.count();
        System.out.println(count);
    }
}
