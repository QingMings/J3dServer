package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.UsersDAO;
import com.iezview.caas.entity.Users;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class UserDaoTest {

    private IUsersDAO usersDAO;
    @Before
    public  void ininDao(){

        usersDAO=new UsersDAO();

    }

    /**
     * 插入用户
     */
    @Test
    public   void inssertUser(){
        Users user  = new Users();
        user.setUsername("qingmings"); //用户名，登录名
        user.setPlainpassword("123456"); //明文密码
        user.setFullname("QingMings");
        usersDAO.save(user);
    }

    /**
     *
     */
    @Test
    public void Test_queryData(){
        List<Users> users = usersDAO.findAll(0,20);
        users.forEach(user-> System.out.println(user));
    }
    /**
     * 测试登录
     */
    @Test
    public void  Test_login(){
        boolean result=   usersDAO.doLogin("qingmings","123456");
        Assert.assertTrue(usersDAO.error(),result);
    }
}
