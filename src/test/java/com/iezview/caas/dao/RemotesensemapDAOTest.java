package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.RemotesensemapDAO;
import com.iezview.caas.daoimpl.UnitDAO;
import com.iezview.caas.entity.Remotesensemap;
import com.iezview.caas.entity.Unit;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * 遥感图 单元测试
 */
public class RemotesensemapDAOTest {


    private IRemotesensemapDAO remotesensemapDAO;
    private  IUnitDAO unitDAO;

    @Before
    public void init() {

        remotesensemapDAO = new RemotesensemapDAO();
        unitDAO =new UnitDAO();
    }

    /**
     * 测试插入
     */
    @Test
    public void test_insert() {
        Remotesensemap  remotesensemap = new Remotesensemap();
        List<Unit>  units = unitDAO.findAll(0,2);
        if (units!=null){
            Unit unit = units.get(0);
            remotesensemap.setUnit(unit);
            remotesensemap.setSname("遥感图单位名称1");
            remotesensemap.setMaxlng(45.0);
            remotesensemap.setMaxlat(45.0);
            remotesensemap.setMinlng(20.0);
            remotesensemap.setMinlat(20.0);
            remotesensemap.setResourcepath("/test/remotesensemap/遥感图/");
            remotesensemapDAO.save(remotesensemap);

        }
    }

    @Test
    public void  test_findAll(){
        List<Remotesensemap>  remotesensemaps = remotesensemapDAO.findAll(0,20);
        remotesensemaps.forEach(remotesensemap -> System.out.println(remotesensemap));
    }

    @Test
    public void test_findRemotesensemapByNameKeyWord(){
        List<Remotesensemap> remotesensemaps = remotesensemapDAO.findRemotesensemapByNameKeyWord("遥感");
        remotesensemaps.forEach(remotesensemap -> System.out.println(remotesensemap));
    }

    @Test
    public void test_findRemotesensemapCountByNameKeyWord(){
        Long count = remotesensemapDAO.findRemotesensemapCountByNameKeyWord("遥感");
        System.out.println(count);
    }

    @Test
    public void test_count(){
        Long count = remotesensemapDAO.count();
        System.out.println(count);
    }
}
