package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.ProjectsummaryDAO;
import com.iezview.caas.daoimpl.UnitDAO;
import com.iezview.caas.entity.Projectsummary;
import com.iezview.caas.entity.Unit;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ProjectsummaryDAOTest {

    private IProjectsummaryDAO projectsummaryDAO;
    private IUnitDAO unitDAO;
    @Before
    public void init(){
        projectsummaryDAO = new ProjectsummaryDAO();
        unitDAO  = new UnitDAO();
    }

    /**
     * 测试插入
     */
    @Test
    public void test_insert(){
        List<Unit> unitList =unitDAO.findUnitByNameKeyWord("垃圾");
        if (unitList!=null){
            Unit unit = unitList.get(0);

            Projectsummary projectsummary = new Projectsummary();
            projectsummary.setPname("项目汇总表2");
            projectsummary.setResourcepath("/test/项目汇总表/");
            projectsummary.setUnit(unit);
            projectsummaryDAO.save(projectsummary);
        }
    }
    @Test
    public  void test_findProjectSummaryByNameKeyWord(){
        List<Projectsummary> projectsummaries =projectsummaryDAO.findProjectsummaryByNameKeyWord("项目");
        projectsummaries.forEach(projectsummary -> System.out.println(projectsummary));
    }
    @Test
    public void test_findProjectSummaryCountByNameKeyWord(){
        Long count = projectsummaryDAO.findProjectsummaryCountByNameKeyWord("项目");
        System.out.println(count);
    }

    @Test
    public  void test_count(){
        Long count = projectsummaryDAO.count();
        System.out.println(count);
    }
}
