package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.AreaDAO;
import com.iezview.caas.entity.Area;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueryCacheTest {


    private IAreaDAO areaDAO;
    private Logger logger;
    @Before
    public  void init(){
        areaDAO = new AreaDAO();
        logger=Logger.getLogger(QueryCacheTest.class.getName());
    }

    @Test
    public void test_add(){
        Area area = new Area();
        area.setAname("测试");
        areaDAO.save(area);
    }
    @Test
    public  void test_query_cache_findbyId(){

        Area area1 = areaDAO.findById(35);
        System.out.println(area1);
        area1.setAname("测试a");
        areaDAO.update(area1);
        Area area2 = areaDAO.findById(35);
        System.out.println(area2);

    }
    @Test
    public void test_query_cache_createQuery(){
        List<Area> areas = areaDAO.findAll();
        System.out.println(areas.size());
        List<Area> areas1 = areaDAO.findAll();
        System.out.println(areas1.size());
    }
}
