package com.iezview.caas.dao;

import com.iezview.caas.daoimpl.PlanmodelDAO;
import com.iezview.caas.daoimpl.UnitDAO;
import com.iezview.caas.entity.Planmodel;
import com.iezview.caas.entity.Unit;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

/**
 * 规划模型 单元测试
 */
public class PlanmodelDAOTest {


    private IPlanmodelDAO  planmodelDAO;
    private IUnitDAO unitDAO;

    @Before
    public void  init(){
        planmodelDAO = new PlanmodelDAO();
        unitDAO = new UnitDAO();
    }

    /**
     * 测试插入 规划模型数据
     */
    @Test
    public void test_insert(){
        Planmodel  planmodel = new Planmodel();
        List<Unit> units =unitDAO.findUnitByNameKeyWord("垃圾");
        if (units!=null){
            Unit unit = units.get(0);
            planmodel.setUnit(unit);
            planmodel.setMname("模型3");
            planmodel.setCentrallat(45.0);
            planmodel.setCentrallng(45.0);
            planmodel.setCentralheight(1000.0);
            planmodel.setDataformat("不知道");
            System.out.println(LocalDateTime.now().toString());
            planmodel.setAcquisitiontime(LocalDateTime.now());
            planmodel.setAltitudemode(1);
            planmodel.setHeading(45.0);
            planmodel.setTilt(45.0);
            planmodel.setRoll(45.0);
            planmodel.setXscale( (short)45);
            planmodel.setYscale((short)45);
            planmodel.setZscale((short)45);
            planmodel.setTiletype("tileType");
            planmodel.setResourcepath("/test/rsourc/hello/模型/");
            planmodel.setDescription("模型描述值");
            planmodel.setIsload(0);
            planmodelDAO.save(planmodel);
        }


    }

    /**
     * 测试查询规划模型
     */
    @Test
    public void test_findAll(){
       List<Planmodel> planmodels=  planmodelDAO.findAll(0,20);
       planmodels.forEach(planmodel -> System.out.println(planmodel.toString()));
    }

    /**
     * 测试名称关键字查询
     */
    @Test
    public void test_findPlanmodelByNameKeyWord(){
        List<Planmodel> planmodels = planmodelDAO.findPlanmodelByNameKeyWord("模型名");
         planmodels.forEach(planmodel -> System.out.println(planmodel));
    }

    /**
     * 测试名称关键字行数查询
     */
    @Test
    public  void tes_findPlanmodelCountByNameKeyWord(){
        Long count =planmodelDAO.findPlanmodelCountByNameKeyWord("模型名");
        System.out.println(count);
    }

    /**
     * 测试 总行数查询
     */
    @Test
    public  void test_count(){
        Long count = planmodelDAO.count();
        System.out.println(count);
    }
}
