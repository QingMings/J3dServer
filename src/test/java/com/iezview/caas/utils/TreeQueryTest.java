package com.iezview.caas.utils;

import com.iezview.caas.entity.*;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class TreeQueryTest {


    private TreeQuery treeQuery;
    @Before
    public void init(){
        treeQuery =new TreeQuery();
    }

    /**
     * 按行政区域查询单位
     */
    @Test
    public void test_groutByArea(){
        Map<Area,List<Unit>>  listMap=treeQuery.groupByArea();
        listMap.forEach((area,units)->{
            System.out.println("---"+area.getAname());
              units.forEach(unit -> {
                  System.out.println(unit.getId()+"--"+unit.getUname());
              });
            System.out.println();
        });

    }

    @Test
    public  void  test_groupByLevel(){
        Map<Unit ,List<Unit>> listMap = treeQuery.groupByLevel();
        listMap.forEach((unit,units)->{
            System.out.println("---"+unit.getId()+":"+unit.getUname());
             units.forEach(unit1 -> System.out.println("     "+unit1.getId()+":"+unit1.getUname()));
        });
    }

    @Test
    public  void test_groupBySubject(){
        Map<Subjects,List<Unit>> listMap = treeQuery.groupBySubject();
                listMap.forEach((subjects, units) ->{
                    System.out.println("---"+subjects.getSname());
                        units.forEach(unit -> System.out.println("     "+unit.getId()+":"+unit.getUname()));
                });
    }
    @Test
    public  void test_findallPlanModel(){
         Map<Unit,List<Planmodel>> listMap = treeQuery.findAllPlanModel();
         listMap.forEach((unit,planmodels)->{
             System.out.println("---"+unit.getUname());
                planmodels.forEach(planmodel -> System.out.println(planmodel.getMname()));
         });

    }
    @Test
    public  void test_findallObliqueModel(){
        Map<Unit,List<Obliquemodel>>  listMap = treeQuery.findAllObliqueModel();
         listMap.forEach((unit, obliquemodels) -> {
             System.out.println("---"+unit.getUname());
              obliquemodels.forEach( obliquemodel -> {
                  System.out.println(obliquemodel.getName());
              });
         });

    }
}
