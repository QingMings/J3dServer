package com.iezview.caas.utils.excel.model;

import com.github.crab2died.annotation.ExcelField;

/**
 * test model class Student
 * @author shishifanbuxie
 */
public class Student {
    public Student() {

    }

    public Student(int id, String name, String address, int age, String city) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.age = age;
        this.city = city;
    }
    @ExcelField(title = "标识",order = 1)
    private int id;
    @ExcelField(title = "名字",order = 2)
    private String name;
    @ExcelField(title = "住址",order = 3)
    private String address;
    @ExcelField(title = "年龄",order = 4)
    private int age;
    @ExcelField(title = "所在城市",order = 5)
    private String city;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Student{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", address='" + address + '\'' +
            ", age=" + age +
            ", city='" + city + '\'' +
            '}';
    }


}
