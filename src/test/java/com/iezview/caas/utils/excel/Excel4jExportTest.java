package com.iezview.caas.utils.excel;


import com.github.crab2died.ExcelUtils;
import com.iezview.caas.utils.excel.model.Student;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * test  model convert to excel
 * @author shishifanbuxie
 *
 */
public class Excel4jExportTest {


      private    List<Student>  students = new ArrayList<Student>();
      public static final String ExcelName="data/B.xlsx";
      public static String path="";

      @Before
      public  void  initStudents(){
          int  nums = 10; // 学生数量
          for (int i = 0; i < nums; i++) {
              students.add(new Student(i,"tom"+i,"昌平区"+i,10+i,"北京"));
          }
          path=Thread.currentThread().getContextClassLoader().getResource("").getPath()+ExcelName;
      }
      @Test
      public  void exportStudents2Excel() throws Exception {
          ExcelUtils.getInstance().exportObjects2Excel(students, Student.class, true, null, true, "B.xlsx");
      }

      @Test
      public void importSutdentfromExcel(){
          try {
              List<Student>  students =ExcelUtils.getInstance().readExcel2Objects(path,Student.class);
              System.out.println(students.size());
          } catch (Exception e) {
              e.printStackTrace();
          }
      }



}
