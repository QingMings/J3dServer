package com.iezview.caas.utils.excel;

import com.github.crab2died.ExcelUtils;
import com.iezview.caas.dao.IAreaDAO;
import com.iezview.caas.dao.IBuildingDAO;
import com.iezview.caas.dao.IUnitDAO;
import com.iezview.caas.daoimpl.AreaDAO;
import com.iezview.caas.daoimpl.BuildingDAO;
import com.iezview.caas.daoimpl.UnitDAO;
import com.iezview.caas.entity.Area;
import com.iezview.caas.entity.Building;
import com.iezview.caas.entity.Unit;
import com.iezview.caas.utils.excel.model.UnitExcelModel;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;

/**
 * 导入测试
 */
public class UnitExcelImportTest {

    public static final String ExcelPath = "data/1整理_全部单位_汇总表.xlsx";
    public static String path = "";
    private IUnitDAO unitDAO = new UnitDAO();
    private IBuildingDAO buildingDAO = new BuildingDAO();
    private IAreaDAO areaDAO = new AreaDAO();

    /**
     * 初始化 Excel 路径
     */
    @Before
    public void init() {
        path = Thread.currentThread().getContextClassLoader().getResource("").getPath() + ExcelPath;
    }

    /**
     * 测试加载 excel
     */
    @Test
    public void Test_ExcelLoad() {
        try {
            List<UnitExcelModel> unitExcelModelList = ExcelUtils.getInstance().readExcel2Objects(path, UnitExcelModel.class);
            unitExcelModelList.forEach(unitExcelModel -> {
                System.out.println(unitExcelModel.toString() + "****:" + unitExcelModel.isBlankOrNull());
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试excel路径
     */
    @Test
    public void Test_classPath() {
        System.out.println(Thread.currentThread().getContextClassLoader().getResource(""));
    }

    /**
     * final测试 选择单位
     */
    @Test
    public void Test_SelectUnit() {
        try {
            List<Unit> units = new ArrayList<>();
            List<UnitExcelModel> unitExcelModelList = ExcelUtils.getInstance().readExcel2Objects(path, UnitExcelModel.class);
            unitExcelModelList.stream().filter(unitExcelModel -> unitExcelModel.isUnit()).forEach(unitExcelModel -> {
                System.out.println(unitExcelModel.getId() + unitExcelModel.getArea() + "   " + unitExcelModel.getName() + "   " + unitExcelModel.getFullName());
//                List<Area>  area = areaDAO.findAreaByNameKeyWord(unitExcelModel.getArea());
                Unit unit = new Unit();
//                unit.setArea(area.get(0));
                unit.setParent(0);
                if (StringUtils.isNotEmpty(unitExcelModel.getFullName())) {
                    unit.setUname(unitExcelModel.getFullName());
                } else {
                    unit.setUname(unitExcelModel.getName());
                }
                unit.setShortname(unitExcelModel.getName());
                if (StringUtils.isNotEmpty(unitExcelModel.getFullName())) {
                    unit.setKeyname(unitExcelModel.getFullName());
                } else {
                    unit.setKeyname(unitExcelModel.getName());
                }
//                unit.setKeyname(unitExcelModel.getFullName());
                unit.setUtype("");
                unit.setCentrallng(Double.parseDouble(randomLonChina()));
                unit.setCentrallat(Double.parseDouble(randomLatChina()));
                unit.setCoveragearea(0.0);
                unit.setUaddress("");
                unit.setSynopsis("");
                unit.setUlevel(1);
                unit.setOrderby(Integer.parseInt(unitExcelModel.getId()));
                unit.setAlias("");
                units.add(unit);

            });
//            unitDAO.saveAll(units);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * final插入基地信息
     * shortName为空
     */
    @Test
    public void testSelectUnitAndUnitBase() {
        final int[] num = {0};
        final Unit[] unita = new Unit[1];
        List<Unit> units = new ArrayList<>();
        try {
            List<UnitExcelModel> unitExcelModelList = ExcelUtils.getInstance().readExcel2Objects(path, UnitExcelModel.class);
//            System.out.println(unitExcelModelList.stream().filter(unitExcelModel ->unitExcelModel.isUnit() || unitExcelModel.isUnitBase()).count());
            unitExcelModelList.stream().filter(unitExcelModel -> unitExcelModel.isUnit() || unitExcelModel.isUnitBase()).forEach(unitExcelModel -> {

                if (unitExcelModel.isUnit()) {
//                    num[0] =0;
                    System.out.println("----" + unitExcelModel.getId() + unitExcelModel.getArea() + "||" + unitExcelModel.getName() + "||" + unitExcelModel.getFullName());
//                    unita[0]=unitDAO.findUnitByShortNameKeyWord(unitExcelModel.getName()).get(0);
                } else {
                    num[0] = num[0] + 1;
                    System.out.println(num[0] + "     L" + unitExcelModel.getArea() + unitExcelModel.getFullName());
//                List<Area>  area = areaDAO.findAreaByNameKeyWord(unitExcelModel.getArea());
                    Unit unit = new Unit();
//                unit.setArea(area.get(0));
//                    unit.setParent(unita[0].getId());
                    if (StringUtils.isNotEmpty(unitExcelModel.getFullName())) {
                        unit.setUname(unitExcelModel.getFullName());
                    } else {
                        unit.setUname(unitExcelModel.getName());
                    }
                    unit.setShortname(unitExcelModel.getName());
                    if (StringUtils.isNotEmpty(unitExcelModel.getFullName())) {
                        unit.setKeyname(unitExcelModel.getFullName());
                    } else {
                        unit.setKeyname(unitExcelModel.getName());
                    }
                    unit.setUtype("");
                    unit.setCentrallng(Double.parseDouble(randomLonChina()));
                    unit.setCentrallat(Double.parseDouble(randomLatChina()));
                    unit.setCoveragearea(0.0);
                    unit.setUaddress("");
                    unit.setSynopsis("");
                    unit.setUlevel(2);
                    unit.setOrderby(num[0]);
                    unit.setAlias("");
                    units.add(unit);
                }
            });
//            unitDAO.saveAll(units);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * final插入站信息
     */
    @Test
    public void test_selectStation() {
        final int[] num = {0};
        final Unit[] unita = new Unit[1];
        List<Unit> units = new ArrayList<>();
        try {
            List<UnitExcelModel> unitExcelModelList = ExcelUtils.getInstance().readExcel2Objects(path, UnitExcelModel.class);
//            System.out.println(unitExcelModelList.stream().filter(unitExcelModel ->unitExcelModel.isUnit() || unitExcelModel.isUnitBase()).count());
            unitExcelModelList.stream().filter(unitExcelModel -> unitExcelModel.isUnitBaseWithStation() || unitExcelModel.isStation()).forEach(unitExcelModel -> {
                if (unitExcelModel.isUnitBaseWithStation()) {
                    System.out.println("----" + unitExcelModel.getArea() + "||" + unitExcelModel.getFullName());
                    num[0] = 0;
                    //根据全称查询单位
//                    unita[0] = unitDAO.findUnitByNameKeyWord(unitExcelModel.getFullName()).get(0);
                    if (unitExcelModel.isStation()) {
                        num[0] = num[0] + 1;
                        System.out.println(num[0] + "        " + unitExcelModel.getArea() + "||" + unitExcelModel.getStationName());
//                        List<Area> area = areaDAO.findAreaByNameKeyWord(unitExcelModel.getArea());
                        Unit unit = new Unit();
//                        unit.setArea(area.get(0));
//                        unit.setParent(unita[0].getParent());
//                        unit.setSubparent(unita[0].getId());
                        if (StringUtils.isNotEmpty(unitExcelModel.getStationName())) {
                            unit.setUname(unitExcelModel.getStationName());
                        } else {
                            unit.setUname(unitExcelModel.getName());
                        }
                        unit.setShortname(unitExcelModel.getStationName());
                        if (StringUtils.isNotEmpty(unitExcelModel.getStationName())) {
                            unit.setKeyname(unitExcelModel.getStationName());
                        } else {
                            unit.setKeyname(unitExcelModel.getName());
                        }
                        unit.setUtype("");
                        unit.setCentrallng(Double.parseDouble(randomLonChina()));
                        unit.setCentrallat(Double.parseDouble(randomLatChina()));
                        unit.setCoveragearea(0.0);
                        unit.setUaddress("");
                        unit.setSynopsis("");
                        unit.setUlevel(2);
                        unit.setOrderby(num[0]);
                        unit.setAlias("");
                        units.add(unit);
                    }
                } else {
                    num[0] = num[0] + 1;
                    System.out.println(num[0] + "        " + unitExcelModel.getArea() + "||" + unitExcelModel.getStationName());
//                    List<Area> area = areaDAO.findAreaByNameKeyWord(unitExcelModel.getArea());
                    Unit unit = new Unit();
//                    unit.setArea(area.get(0));
//                    unit.setParent(unita[0].getParent());
//                    unit.setSubparent(unita[0].getId());
                    if (StringUtils.isNotEmpty(unitExcelModel.getStationName())) {
                        unit.setUname(unitExcelModel.getStationName());
                    } else {
                        unit.setUname(unitExcelModel.getName());
                    }
                    unit.setShortname(unitExcelModel.getStationName());
                    if (StringUtils.isNotEmpty(unitExcelModel.getStationName())) {
                        unit.setKeyname(unitExcelModel.getStationName());
                    } else {
                        unit.setKeyname(unitExcelModel.getName());
                    }
                    unit.setUtype("");
                    unit.setCentrallng(Double.parseDouble(randomLonChina()));
                    unit.setCentrallat(Double.parseDouble(randomLatChina()));
                    unit.setCoveragearea(0.0);
                    unit.setUaddress("");
                    unit.setSynopsis("");
                    unit.setUlevel(2);
                    unit.setOrderby(num[0]);
                    unit.setAlias("");
                    units.add(unit);
                }

            });
//            unitDAO.saveAll(units);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 插入建筑信息
     */
    @Test
    public void test_selectBuilding() {
        final int[] num = {0};
        final Unit[] unita = new Unit[1];
        List<Building> buildings = new ArrayList<>();
        try {
            List<UnitExcelModel> unitExcelModelList = ExcelUtils.getInstance().readExcel2Objects(path, UnitExcelModel.class);
//            System.out.println(unitExcelModelList.stream().filter(unitExcelModel -> unitExcelModel.isBuilding()).count());
            unitExcelModelList.stream().filter(unitExcelModel -> unitExcelModel.isUnit() || unitExcelModel.isUnitBase() || unitExcelModel.isStation() || unitExcelModel.isBuilding()).forEach(unitExcelModel -> {
                if (unitExcelModel.isUnit()) {
//                    num[0]=0;
                    System.out.println("--------" + unitExcelModel.getId() + "||" + unitExcelModel.getArea() + "||" + unitExcelModel.getName());
                    unita[0] = unitDAO.findUnitByShortNameKeyWord(unitExcelModel.getName()).get(0);
                    if (unitExcelModel.isBuilding()) {
                        System.out.println("            " + unitExcelModel.getBuildingNum() + unitExcelModel.getBuildingName());

                        Building building = buildBuilding(unita[0], unitExcelModel);
                        buildings.add(building);

                    }
                } else if (unitExcelModel.isUnitBase()) {
                    num[0] = num[0] + 1;
                    System.out.println("------------" + num[0] + "||" + unitExcelModel.getArea() + "||" + unitExcelModel.getFullName());
                    unita[0] = unitDAO.findUnitByNameKeyWord(unitExcelModel.getFullName()).get(0);
                    if (unitExcelModel.isStation()) {
                        System.out.println("------------" + num[0] + "||" + unitExcelModel.getArea() + "||" + unitExcelModel.getStationName());
                        unita[0] = unitDAO.findUnitByNameKeyWord(unitExcelModel.getStationName()).get(0);
                        if (unitExcelModel.isBuilding()) {
                            System.out.println("            " + unitExcelModel.getBuildingNum() + unitExcelModel.getBuildingName());
                            Building building = buildBuilding(unita[0], unitExcelModel);
                            buildings.add(building);

                        }

                    } else if (unitExcelModel.isBuilding()) {
                        System.out.println("            " + unitExcelModel.getBuildingNum() + unitExcelModel.getBuildingName());
                        Building building = buildBuilding(unita[0], unitExcelModel);
                        buildings.add(building);

                    }

                } else if (unitExcelModel.isStation()) {
                    System.out.println("------------" + num[0] + "||" + unitExcelModel.getArea() + "||" + unitExcelModel.getStationName());
                    unita[0] = unitDAO.findUnitByNameKeyWord(unitExcelModel.getStationName()).get(0);
                    if (unitExcelModel.isBuilding()) {
                        System.out.println("            " + unitExcelModel.getBuildingNum() + unitExcelModel.getBuildingName());
                        Building building = buildBuilding(unita[0], unitExcelModel);
                        buildings.add(building);

                    }

                } else if (unitExcelModel.isBuilding()) {
                    System.out.println("            " + unitExcelModel.getBuildingNum() + unitExcelModel.getBuildingName());
                    Building building = buildBuilding(unita[0], unitExcelModel);
                    buildings.add(building);

                }
            });
            buildingDAO.saveAll(buildings);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试 选择 基地
     */
    @Test
    public void Test_SelectUnitBase() {
        try {
            List<UnitExcelModel> unitExcelModelList = ExcelUtils.getInstance().readExcel2Objects(path, UnitExcelModel.class);
            System.out.println(unitExcelModelList.stream().filter(unitExcelModel -> unitExcelModel.isUnitBase()).count());
            unitExcelModelList.stream().filter(unitExcelModel -> unitExcelModel.isUnitBase()).forEach(unitExcelModel -> {
                System.out.println(unitExcelModel);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试选择建筑
     */
    @Test
    public void Test_SelectBuilding() {
        try {
            List<UnitExcelModel> unitExcelModelList = ExcelUtils.getInstance().readExcel2Objects(path, UnitExcelModel.class);
            System.out.println(unitExcelModelList.stream().filter(unitExcelModel -> unitExcelModel.isBuilding()).count());
            unitExcelModelList.stream().filter(unitExcelModel -> unitExcelModel.isBuilding()).forEach(unitExcelModel -> {
                System.out.println(unitExcelModel);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 测试构建存储结构
     */
    @Test
    public void Test_buildStorageStructure() {
        UnitExcelModel unit;
        UnitExcelModel unitBase;
        UnitExcelModel station;
        UnitExcelModel building;
        int mode = 0; // 模式  0 单位   1  基地   2 站   3 建筑   0  初始值
        JsonObject treeRoot = new JsonObject();
        try {
            List<UnitExcelModel> unitExcelModelList = ExcelUtils.getInstance().readExcel2Objects(path, UnitExcelModel.class);
            int length = unitExcelModelList.size();

            for (int i = 0; i < length; i++) {
                UnitExcelModel unitExcelModel = unitExcelModelList.get(i);
                //判断不是空行
                if (!unitExcelModel.isBlankOrNull()) {
                    if (unitExcelModel.isUnit()) {
                        Unit insertUnit = new Unit();
                        insertUnit.setParent(0);
                        insertUnit.setUname(unitExcelModel.getFullName());
                        insertUnit.setShortname(unitExcelModel.getName());
                        insertUnit.setKeyname(unitExcelModel.getFullName());
                        insertUnit.setUtype("所");
                        List<Area> areas = areaDAO.findAreaByNameKeyWord(unitExcelModel.getArea());
                        if (areas.size() > 0) {
                            insertUnit.setArea(areas.get(0));
                        } else {
                            throw new RuntimeException("获取单位信息异常:" + unitExcelModel);
                        }


                    }

                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @param MinLon：最新经度 MaxLon： 最大经度   MinLat：最新纬度   MaxLat：最大纬度    type：设置返回经度还是纬度
     * @return
     * @throws
     * @Title: randomLonLat
     * @Description: 在矩形内随机生成经纬度
     */
    public String randomLonLat(double MinLon, double MaxLon, double MinLat, double MaxLat, String type) {
        Random random = new Random();
        BigDecimal db = new BigDecimal(Math.random() * (MaxLon - MinLon) + MinLon);
        String lon = db.setScale(6, BigDecimal.ROUND_HALF_UP).toString();// 小数后6位
        db = new BigDecimal(Math.random() * (MaxLat - MinLat) + MinLat);
        String lat = db.setScale(6, BigDecimal.ROUND_HALF_UP).toString();
        if (type.equals("Lon")) {
            return lon;
        } else {
            return lat;
        }
    }

    public String randomLonLatChina(String type) {
        return randomLonLat(73.66, 135.05, 3.86, 53.55, type);
    }

    public String randomLonChina() {
        return randomLonLatChina("Lon");
    }

    public String randomLatChina() {
        return randomLonLatChina("Lat");
    }

    private Building buildBuilding(Unit unit, UnitExcelModel unitExcelModel) {
        Building building = new Building();
        building.setUnit(unit);
        building.setNumber(unitExcelModel.getBuildingNum());
        building.setBname(unitExcelModel.getBuildingName());
        building.setResourcepath("");
        building.setYears(0);
        building.setHeight(0.0);
        if (StringUtils.isNoneBlank(unitExcelModel.getPurpose())) {
            building.setPurpose(unitExcelModel.getPurpose());
        } else {
            building.setPurpose("没有注明");
        }
        if (StringUtils.isNumeric(unitExcelModel.getBuildingTotalMeasureArea())) {
            building.setTotalarea(Double.parseDouble(unitExcelModel.getBuildingTotalMeasureArea()));
        } else {
            building.setTotalarea(0.0);
        }
        if (StringUtils.isNumeric(unitExcelModel.getBuildingUpperMeasureArea())) {
            building.setAbovearea(Double.parseDouble(unitExcelModel.getBuildingUpperMeasureArea()));
        } else {
            building.setAbovearea(0.0);
        }
        if (StringUtils.isNumeric(unitExcelModel.getBuildingLowerMeasureArea())) {
            building.setUnderarea(Double.parseDouble(unitExcelModel.getBuildingUpperMeasureArea()));
        } else {
            building.setUnderarea(0.0);
        }
        if (StringUtils.isNumeric(unitExcelModel.getBuildingCenterLong())) {
            building.setCentrallng(Double.parseDouble(unitExcelModel.getBuildingCenterLong()));
        } else {
            building.setCentrallng(Double.parseDouble(randomLonChina()));
        }
        if (StringUtils.isNumeric(unitExcelModel.getBuildingCenterLat())) {
            building.setCentrallat(Double.parseDouble(unitExcelModel.getBuildingCenterLat()));
        } else {
            building.setCentrallat(Double.parseDouble(randomLatChina()));
        }
        building.setConstructionunits(unitExcelModel.getConstructioUnits());
        building.setSynopsis("");
        if (StringUtils.isNotBlank(unitExcelModel.getBuildingTotalLayer())) {
            building.setLayernum(unitExcelModel.getBuildingTotalLayer());
        } else {
            building.setLayernum("未注明");
        }

        return building;
    }
}
