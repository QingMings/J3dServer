package com.iezview.caas.entity;


import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * Obliquemodel entity. @author MyEclipse Persistence Tools
 * 因为一个倾斜摄影模型可能对多个单位，所以 单位和倾斜摄影模型表有中间表  多对多关联关系
 */
@Entity
@Table(name = "obliquemodel", catalog = "caasdb")

public class Obliquemodel implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private Double centrallng;
	private Double centrallat;
	private Double centralheight;
	private String dataformat;
	private LocalDateTime acquisitiontime;
	private Double altitudemode;
	private Double heading;
	private Double tilt;
	private Double roll;
	private Short xscale;
	private Short yscale;
	private Short zscale;
	private String tiletype;
	private Short tilemin;
	private Short tilemax;
	private Integer tilequantity;
	private String resourcepath;
	private String description;
	private Integer isload;
//	private Set<Unitobliquerel> unitobliquerels = new HashSet<Unitobliquerel>(0);
    private Set<Unit> units =new HashSet<>(0);
	// Constructors

	/** default constructor */
	public Obliquemodel() {
	}

	/** minimal constructor */
	public Obliquemodel(Integer id, String name, String resourcepath) {
		this.id = id;
		this.name = name;
		this.resourcepath = resourcepath;
	}

	/** full constructor */
	public Obliquemodel(Integer id, String name, Double centrallng, Double centrallat, Double centralheight,
			String dataformat, LocalDateTime acquisitiontime, Double altitudemode, Double heading, Double tilt, Double roll,
			Short xscale, Short yscale, Short zscale, String tiletype, Short tilemin, Short tilemax,
			Integer tilequantity, String resourcepath, String description, Integer isload
			) {
		this.id = id;
		this.name = name;
		this.centrallng = centrallng;
		this.centrallat = centrallat;
		this.centralheight = centralheight;
		this.dataformat = dataformat;
		this.acquisitiontime = acquisitiontime;
		this.altitudemode = altitudemode;
		this.heading = heading;
		this.tilt = tilt;
		this.roll = roll;
		this.xscale = xscale;
		this.yscale = yscale;
		this.zscale = zscale;
		this.tiletype = tiletype;
		this.tilemin = tilemin;
		this.tilemax = tilemax;
		this.tilequantity = tilequantity;
		this.resourcepath = resourcepath;
		this.description = description;
		this.isload = isload;
//		this.unitobliquerels = unitobliquerels;
	}

	// Property accessors
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAME", nullable = false, length = 128)

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "CENTRALLNG", precision = 22, scale = 0)

	public Double getCentrallng() {
		return this.centrallng;
	}

	public void setCentrallng(Double centrallng) {
		this.centrallng = centrallng;
	}

	@Column(name = "CENTRALLAT", precision = 22, scale = 0)

	public Double getCentrallat() {
		return this.centrallat;
	}

	public void setCentrallat(Double centrallat) {
		this.centrallat = centrallat;
	}

	@Column(name = "CENTRALHEIGHT", precision = 22, scale = 0)

	public Double getCentralheight() {
		return this.centralheight;
	}

	public void setCentralheight(Double centralheight) {
		this.centralheight = centralheight;
	}

	@Column(name = "DATAFORMAT", length = 64)

	public String getDataformat() {
		return this.dataformat;
	}

	public void setDataformat(String dataformat) {
		this.dataformat = dataformat;
	}

	@Column(name = "ACQUISITIONTIME", length = 8)

	public LocalDateTime getAcquisitiontime() {
		return this.acquisitiontime;
	}

	public void setAcquisitiontime(LocalDateTime acquisitiontime) {
		this.acquisitiontime = acquisitiontime;
	}

	@Column(name = "ALTITUDEMODE", precision = 22, scale = 0)

	public Double getAltitudemode() {
		return this.altitudemode;
	}

	public void setAltitudemode(Double altitudemode) {
		this.altitudemode = altitudemode;
	}

	@Column(name = "HEADING", precision = 22, scale = 0)

	public Double getHeading() {
		return this.heading;
	}

	public void setHeading(Double heading) {
		this.heading = heading;
	}

	@Column(name = "TILT", precision = 22, scale = 0)

	public Double getTilt() {
		return this.tilt;
	}

	public void setTilt(Double tilt) {
		this.tilt = tilt;
	}

	@Column(name = "ROLL", precision = 22, scale = 0)

	public Double getRoll() {
		return this.roll;
	}

	public void setRoll(Double roll) {
		this.roll = roll;
	}

	@Column(name = "XSCALE")

	public Short getXscale() {
		return this.xscale;
	}

	public void setXscale(Short xscale) {
		this.xscale = xscale;
	}

	@Column(name = "YSCALE")

	public Short getYscale() {
		return this.yscale;
	}

	public void setYscale(Short yscale) {
		this.yscale = yscale;
	}

	@Column(name = "ZSCALE")

	public Short getZscale() {
		return this.zscale;
	}

	public void setZscale(Short zscale) {
		this.zscale = zscale;
	}

	@Column(name = "TILETYPE", length = 64)

	public String getTiletype() {
		return this.tiletype;
	}

	public void setTiletype(String tiletype) {
		this.tiletype = tiletype;
	}

	@Column(name = "TILEMIN")

	public Short getTilemin() {
		return this.tilemin;
	}

	public void setTilemin(Short tilemin) {
		this.tilemin = tilemin;
	}

	@Column(name = "TILEMAX")

	public Short getTilemax() {
		return this.tilemax;
	}

	public void setTilemax(Short tilemax) {
		this.tilemax = tilemax;
	}

	@Column(name = "TILEQUANTITY")

	public Integer getTilequantity() {
		return this.tilequantity;
	}

	public void setTilequantity(Integer tilequantity) {
		this.tilequantity = tilequantity;
	}

	@Column(name = "RESOURCEPATH", nullable = false, length = 256)

	public String getResourcepath() {
		return this.resourcepath;
	}

	public void setResourcepath(String resourcepath) {
		this.resourcepath = resourcepath;
	}

	@Column(name = "DESCRIPTION")

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "ISLOAD")

	public Integer getIsload() {
		return this.isload;
	}

	public void setIsload(Integer isload) {
		this.isload = isload;
	}

//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "obliquemodel")

//	public Set<Unitobliquerel> getUnitobliquerels() {
//		return this.unitobliquerels;
//	}

//	public void setUnitobliquerels(Set<Unitobliquerel> unitobliquerels) {
//		this.unitobliquerels = unitobliquerels;
//	}

    @ManyToMany
    @JoinTable(name = "UnitObliqueRel",joinColumns = @JoinColumn(name = "OBLIQUEID"),inverseJoinColumns = @JoinColumn(name = "UNITID"))
    public Set<Unit> getUnits() {
        return units;
    } public void setUnits(Set<Unit> units) {
        this.units = units;
    }
    @Override
    public String toString() {
        return "Obliquemodel{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", centrallng=" + centrallng +
            ", centrallat=" + centrallat +
            ", centralheight=" + centralheight +
            ", dataformat='" + dataformat + '\'' +
            ", acquisitiontime=" + acquisitiontime +
            ", altitudemode=" + altitudemode +
            ", heading=" + heading +
            ", tilt=" + tilt +
            ", roll=" + roll +
            ", xscale=" + xscale +
            ", yscale=" + yscale +
            ", zscale=" + zscale +
            ", tiletype='" + tiletype + '\'' +
            ", tilemin=" + tilemin +
            ", tilemax=" + tilemax +
            ", tilequantity=" + tilequantity +
            ", resourcepath='" + resourcepath + '\'' +
            ", description='" + description + '\'' +
            ", isload=" + isload +
//            ", unitobliquerels=" + unitobliquerels +
            '}';
    }
}
