//package com.iezview.caas.entity;
//
//import javax.persistence.*;
//
///**
// * Unitobliquerel entity. @author MyEclipse Persistence Tools
// */
//@Entity
//@Table(name = "unitobliquerel", catalog = "caasdb")
//
//public class Unitobliquerel implements java.io.Serializable {
//
//	// Fields
//
//	private Integer id;
//	private Obliquemodel obliquemodel;
//	private Unit unit;
//	private String remark;
//
//	// Constructors
//
//	/** default constructor */
//	public Unitobliquerel() {
//	}
//
//	/** minimal constructor */
//	public Unitobliquerel(Integer id, Obliquemodel obliquemodel, Unit unit) {
//		this.id = id;
//		this.obliquemodel = obliquemodel;
//		this.unit = unit;
//	}
//
//	/** full constructor */
//	public Unitobliquerel(Integer id, Obliquemodel obliquemodel, Unit unit, String remark) {
//		this.id = id;
//		this.obliquemodel = obliquemodel;
//		this.unit = unit;
//		this.remark = remark;
//	}
//
//	// Property accessors
//	@Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "ID", unique = true, nullable = false)
//
//	public Integer getId() {
//		return this.id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "OBLIQUEID", nullable = false)
//
//	public Obliquemodel getObliquemodel() {
//		return this.obliquemodel;
//	}
//
//	public void setObliquemodel(Obliquemodel obliquemodel) {
//		this.obliquemodel = obliquemodel;
//	}
//
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "UNITID", nullable = false)
//
//	public Unit getUnit() {
//		return this.unit;
//	}
//
//	public void setUnit(Unit unit) {
//		this.unit = unit;
//	}
//
//	@Column(name = "REMARK")
//
//	public String getRemark() {
//		return this.remark;
//	}
//
//	public void setRemark(String remark) {
//		this.remark = remark;
//	}
//
//    @Override
//    public String toString() {
//        return "Unitobliquerel{" +
//            "id=" + id +
//            ", obliquemodel=" + obliquemodel +
//            ", unit=" + unit +
//            ", remark='" + remark + '\'' +
//            '}';
//    }
//}
