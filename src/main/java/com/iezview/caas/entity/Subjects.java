package com.iezview.caas.entity;

import org.hibernate.annotations.Where;
import scala.Int;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Subjects entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "subjects", catalog = "caasdb")

public class Subjects implements java.io.Serializable {

	// Fields

	private Integer id;
	private String sname;
	private String remark;
    private Integer orderby;
	private Set<Unit> units =new HashSet<>(0);


    // Constructors

	/** default constructor */
	public Subjects() {
	}

	/** minimal constructor */
	public Subjects(Integer id) {
		this.id = id;
	}

	/** full constructor */
	public Subjects(Integer id, String sname, String remark) {
		this.id = id;
		this.sname = sname;
		this.remark = remark;
	}

	// Property accessors
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "SNAME", length = 128)

	public String getSname() {
		return this.sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	@Column(name = "REMARK")

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

    @Column(name = "ORDERBY", length = 64)
    public Integer getOrderby() {
        return orderby;
    }

    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }

    @ManyToMany
    @JoinTable(name = "SUBJECG_UNIT",joinColumns = @JoinColumn(name = "SUBJECTID"),inverseJoinColumns = @JoinColumn(name = "UNITID"))
    @OrderBy("orderBy,parent ASC")
    @Where(clause = "parent=0")

    public Set<Unit> getUnits() {
        return units;
    }

    public void setUnits(Set<Unit> units) {
        this.units = units;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Subjects)) return false;

        Subjects subjects = (Subjects) o;

        if (id != null ? !id.equals(subjects.id) : subjects.id != null) return false;
        if (sname != null ? !sname.equals(subjects.sname) : subjects.sname != null) return false;
        if (remark != null ? !remark.equals(subjects.remark) : subjects.remark != null) return false;
        if (orderby != null ? !orderby.equals(subjects.orderby) : subjects.orderby != null) return false;
        return units != null ? units.equals(subjects.units) : subjects.units == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (sname != null ? sname.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (orderby != null ? orderby.hashCode() : 0);
        result = 31 * result + (units != null ? units.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Subjects{" +
            "id=" + id +
            ", sname='" + sname + '\'' +
            ", remark='" + remark + '\'' +
            '}';
    }
}
