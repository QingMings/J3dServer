package com.iezview.caas.entity;

import javax.persistence.*;

/**
 * Users entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "users", catalog = "caasdb")

public class Users implements java.io.Serializable {

	// Fields

	private Integer id;
	private String username;
	private String password;
	private String selt;
	private String fullname;
	private String plainpassword;
	private Integer usertype;

	// Constructors

	/** default constructor */
	public Users() {
	}

	/** minimal constructor */
	public Users(Integer id, String username, String password, String selt,String plainpassword) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.plainpassword=plainpassword;
		this.selt = selt;
	}

	/** full constructor */
	public Users(Integer id, String username, String password, String selt, String fullname,String plainpassword) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.plainpassword =plainpassword;
		this.selt = selt;
		this.fullname = fullname;
	}

	// Property accessors
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "USERNAME", nullable = false)

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "PASSWORD", nullable = false)

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    @Column(name = "PLAINPASSWORD", nullable = false)
    public String getPlainpassword() {
        return plainpassword;
    }

    public void setPlainpassword(String plainpassword) {
        this.plainpassword = plainpassword;
    }

    @Column(name = "USERTYPE",nullable = false)
    public Integer getUsertype() {
        return usertype;
    }

    public void setUsertype(Integer usertype) {
        this.usertype = usertype;
    }

    @Column(name = "SELT", nullable = false)

	public String getSelt() {
		return this.selt;
	}

	public void setSelt(String selt) {
		this.selt = selt;
	}

	@Column(name = "FULLNAME")

	public String getFullname() {
		return this.fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}


    @Override
    public String toString() {
        return "Users{" +
            "id=" + id +
            ", username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", selt='" + selt + '\'' +
            ", fullname='" + fullname + '\'' +
            '}';
    }
}
