package com.iezview.caas.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * Area entity. @author MyEclipse Persistence Tools
 *
 */
@Entity
@Table(name = "area", catalog = "caasdb")

public class Area implements java.io.Serializable {

	// Fields

	private Integer id;
	private String aname;
	private Integer parent;
	private Integer alevel;
	private String nodekind;
	private String remark;
//	private Set<Unit> units = new HashSet<Unit>(0);

	// Constructors

	/** default constructor */
	public Area() {
	}

	/** minimal constructor */
	public Area(Integer id, String aname) {
		this.id = id;
		this.aname = aname;
	}

	/** full constructor */
	public Area(Integer id, String aname, Integer parent, Integer alevel, String nodekind, String remark,
			Set<Unit> units) {
		this.id = id;
		this.aname = aname;
		this.parent = parent;
		this.alevel = alevel;
		this.nodekind = nodekind;
		this.remark = remark;
//		this.units = units;
	}

	// Property accessors
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ANAME", nullable = false, length = 128)

	public String getAname() {
		return this.aname;
	}

	public void setAname(String aname) {
		this.aname = aname;
	}

	@Column(name = "PARENT")

	public Integer getParent() {
		return this.parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	@Column(name = "ALEVEL")

	public Integer getAlevel() {
		return this.alevel;
	}

	public void setAlevel(Integer alevel) {
		this.alevel = alevel;
	}

	@Column(name = "NODEKIND", length = 64)

	public String getNodekind() {
		return this.nodekind;
	}

	public void setNodekind(String nodekind) {
		this.nodekind = nodekind;
	}

	@Column(name = "REMARK")

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "area")

//	public Set<Unit> getUnits() {
//		return this.units;
//	}

//	public void setUnits(Set<Unit> units) {
//		this.units = units;
//	}


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Area)) return false;

        Area area = (Area) o;

        if (id != null ? !id.equals(area.id) : area.id != null) return false;
        if (aname != null ? !aname.equals(area.aname) : area.aname != null) return false;
        if (parent != null ? !parent.equals(area.parent) : area.parent != null) return false;
        if (alevel != null ? !alevel.equals(area.alevel) : area.alevel != null) return false;
        if (nodekind != null ? !nodekind.equals(area.nodekind) : area.nodekind != null) return false;
        return remark != null ? remark.equals(area.remark) : area.remark == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (aname != null ? aname.hashCode() : 0);
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        result = 31 * result + (alevel != null ? alevel.hashCode() : 0);
        result = 31 * result + (nodekind != null ? nodekind.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Area{" +
            "id=" + id +
            ", aname='" + aname + '\'' +
            ", parent=" + parent +
            ", alevel=" + alevel +
            ", nodekind='" + nodekind + '\'' +
            ", remark='" + remark + '\'' +
            '}';
    }
}
