package com.iezview.caas.entity;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * Unit entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "unit", catalog = "caasdb")

public class Unit implements java.io.Serializable {
    public static final String SPLIT_CHAR=","; // 分割符，用于  subjects 名称的分割
	// Fields

	private Integer id;
	private Area area;
	private Integer parent;
	private String uname;
	private String shortname;
	private String keyname;
	private String utype;
	private Double centrallng;
	private Double centrallat;
	private Double coveragearea;
	private String uaddress;
	private String synopsis;
	private Integer ulevel;
	private String nodekind="nkLeaf";//默认值
    private Integer orderby;
    private String alias;
    private Integer subparent;
	private Set<Remotesensemap> remotesensemaps = new HashSet<Remotesensemap>(0);
	private Set<Planmap> planmaps = new HashSet<Planmap>(0);
	private Set<Planmodel> planmodels = new HashSet<Planmodel>(0);
//	private Set<Unitobliquerel> unitobliquerels = new HashSet<Unitobliquerel>(0);
    private Set<Obliquemodel>  obliquemodels = new HashSet<>(0);
	private Set<Projectsummary> projectsummaries = new HashSet<Projectsummary>(0);
	private Set<Building> buildings = new HashSet<Building>(0);

    private Set<Subjects>  subjects = new HashSet<>(0);


    // Constructors

	/** default constructor */
	public Unit() {
	}

	/** minimal constructor */
	public Unit(Integer id, Area area, String uname) {
		this.id = id;
		this.area = area;
		this.uname = uname;
	}

	/** full constructor */
	public Unit(Integer id, Area area, Integer parent, String uname, String shortname, String keyname, String utype,
			String subjects, Double centrallng, Double centrallat, Double coveragearea, String uaddress,
			String synopsis, Integer ulevel, String nodekind,
			Set<Planmap> planmaps,
			Set<Projectsummary> projectsummaries, Set<Building> buildings) {
		this.id = id;
		this.area = area;
		this.parent = parent;
		this.uname = uname;
		this.shortname = shortname;
		this.keyname = keyname;
		this.utype = utype;
		this.centrallng = centrallng;
		this.centrallat = centrallat;
		this.coveragearea = coveragearea;
		this.uaddress = uaddress;
		this.synopsis = synopsis;
		this.ulevel = ulevel;
		this.nodekind = nodekind;
		this.remotesensemaps = remotesensemaps;
		this.planmaps = planmaps;
		this.planmodels = planmodels;
//		this.unitobliquerels = unitobliquerels;
		this.projectsummaries = projectsummaries;
		this.buildings = buildings;
	}

	// Property accessors
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AREAID", nullable = false)

	public Area getArea() {
		return this.area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	@Column(name = "PARENT")

	public Integer getParent() {
		return this.parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	@Column(name = "UNAME", nullable = false, length = 128)

	public String getUname() {
		return this.uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	@Column(name = "SHORTNAME", length = 128)

	public String getShortname() {
		return this.shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	@Column(name = "KEYNAME", length = 128)

	public String getKeyname() {
		return this.keyname;
	}

	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}

	@Column(name = "UTYPE")

	public String getUtype() {
		return this.utype;
	}

	public void setUtype(String utype) {
		this.utype = utype;
	}


	@Column(name = "CENTRALLNG", precision = 22, scale = 0)

	public Double getCentrallng() {
		return this.centrallng;
	}

	public void setCentrallng(Double centrallng) {
		this.centrallng = centrallng;
	}

	@Column(name = "CENTRALLAT", precision = 22, scale = 0)

	public Double getCentrallat() {
		return this.centrallat;
	}

	public void setCentrallat(Double centrallat) {
		this.centrallat = centrallat;
	}

	@Column(name = "COVERAGEAREA", precision = 22, scale = 0)

	public Double getCoveragearea() {
		return this.coveragearea;
	}

	public void setCoveragearea(Double coveragearea) {
		this.coveragearea = coveragearea;
	}

	@Column(name = "UADDRESS", length = 256)

	public String getUaddress() {
		return this.uaddress;
	}

	public void setUaddress(String uaddress) {
		this.uaddress = uaddress;
	}

	@Column(name = "SYNOPSIS", length = 512)

	public String getSynopsis() {
		return this.synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	@Column(name = "ULEVEL")

	public Integer getUlevel() {
		return this.ulevel;
	}

	public void setUlevel(Integer ulevel) {
		this.ulevel = ulevel;
	}

	@Column(name = "NODEKIND", length = 64)

	public String getNodekind() {
		return this.nodekind;
	}

	public void setNodekind(String nodekind) {
		this.nodekind = nodekind;
	}
    @Column(name = "ORDERBY", length = 64)
    public Integer getOrderby() {
        return orderby;
    }

    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }
    @Column(name = "ALIAS", length = 256)
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
    @Column(name = "SUBPARENT", length = 64)
    public Integer getSubparent() {
        return subparent;
    }

    public void setSubparent(Integer subparent) {
        this.subparent = subparent;
    }

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "unit")
    @OrderBy("id ASC")
	public Set<Remotesensemap> getRemotesensemaps() {
		return this.remotesensemaps;
	}

	public void setRemotesensemaps(Set<Remotesensemap> remotesensemaps) {
		this.remotesensemaps = remotesensemaps;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "unit")
    @OrderBy("id ASC")
	public Set<Planmap> getPlanmaps() {
		return this.planmaps;
	}

	public void setPlanmaps(Set<Planmap> planmaps) {
		this.planmaps = planmaps;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "unit")
    @OrderBy("id ASC")
	public Set<Planmodel> getPlanmodels() {
		return this.planmodels;
	}

	public void setPlanmodels(Set<Planmodel> planmodels) {
		this.planmodels = planmodels;
	}

//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "unit")

//	public Set<Unitobliquerel> getUnitobliquerels() {
//		return this.unitobliquerels;
//	}

//	public void setUnitobliquerels(Set<Unitobliquerel> unitobliquerels) {
//		this.unitobliquerels = unitobliquerels;
//	}

    @ManyToMany
    @JoinTable(name = "UnitObliqueRel",joinColumns = @JoinColumn(name = "UNITID"),inverseJoinColumns = @JoinColumn(name = "OBLIQUEID"))
    @OrderBy("id ASC")
    public Set<Obliquemodel> getObliquemodels() {
        return obliquemodels;
    }

    public void setObliquemodels(Set<Obliquemodel> obliquemodels) {
        this.obliquemodels = obliquemodels;
    }
    @ManyToMany
    @JoinTable(name = "SUBJECG_UNIT",joinColumns = @JoinColumn(name = "UNITID"),inverseJoinColumns = @JoinColumn(name = "SUBJECTID"))
    @OrderBy("id ASC")
    public Set<Subjects> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subjects> subjects) {
        this.subjects = subjects;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "unit")
    @OrderBy("id ASC")
	public Set<Projectsummary> getProjectsummaries() {
		return this.projectsummaries;
	}

	public void setProjectsummaries(Set<Projectsummary> projectsummaries) {
		this.projectsummaries = projectsummaries;
	}

	@OneToMany( cascade = CascadeType.ALL,fetch = FetchType.LAZY, mappedBy = "unit")
    @OrderBy("id ASC")
	public Set<Building> getBuildings() {
		return this.buildings;
	}

	public void setBuildings(Set<Building> buildings) {
		this.buildings = buildings;
	}




    @Override
    public String toString() {
        return "Unit{" +
            "id=" + id +
            ", area=" + area +
            ", parent=" + parent +
            ", uname='" + uname + '\'' +
            ", shortname='" + shortname + '\'' +
            ", keyname='" + keyname + '\'' +
            ", utype='" + utype + '\'' +
            ", centrallng=" + centrallng +
            ", centrallat=" + centrallat +
            ", coveragearea=" + coveragearea +
            ", uaddress='" + uaddress + '\'' +
            ", synopsis='" + synopsis + '\'' +
            ", ulevel=" + ulevel +
            ", nodekind='" + nodekind + '\'' +
            ", order=" + orderby +
//            ", remotesensemaps=" + remotesensemaps +
//            ", planmaps=" + planmaps +
//            ", planmodels=" + planmodels +
//            ", unitobliquerels=" + unitobliquerels +
//            ", projectsummaries=" + projectsummaries +
            '}';
    }
}
