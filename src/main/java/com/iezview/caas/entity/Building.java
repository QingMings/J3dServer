package com.iezview.caas.entity;

import javax.persistence.*;

/**
 * Building entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "building", catalog = "caasdb")

public class Building implements java.io.Serializable {

	// Fields

	private Integer id;
	private Unit unit;
	private String number;
	private String bname;
	private String resourcepath;
	private String btype;
	private Integer years;
	private Double height;
	private String purpose;
	private Double totalarea;
	private Double abovearea;
	private Double underarea;
	private String constructionunits;
	private Double centrallng;
	private Double centrallat;
	private String synopsis;
	private String layernum;

	// Constructors

	/** default constructor */
	public Building() {
	}



    /** minimal constructor */
	public Building(Integer id, Unit unit, String number, String bname, String resourcepath) {
		this.id = id;
		this.unit = unit;

		this.number = number;
		this.bname = bname;
		this.resourcepath = resourcepath;
	}

	/** full constructor */
    public Building(Integer id, Unit unit, String number, String bname, String resourcepath, String btype, Integer years, Double height, String purpose, Double totalarea, Double abovearea, Double underarea, String constructionunits, Double centrallng, Double centrallat, String synopsis, String layernum) {
        this.id = id;
        this.unit = unit;
        this.number = number;
        this.bname = bname;
        this.resourcepath = resourcepath;
        this.btype = btype;
        this.years = years;
        this.height = height;
        this.purpose = purpose;
        this.totalarea = totalarea;
        this.abovearea = abovearea;
        this.underarea = underarea;
        this.constructionunits = constructionunits;
        this.centrallng = centrallng;
        this.centrallat = centrallat;
        this.synopsis = synopsis;
        this.layernum = layernum;
    }

	// Property accessors
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNITID", nullable = false)

	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Column(name = "NUMBER", nullable = false, length = 64)

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Column(name = "BNAME", nullable = false, length = 128)

	public String getBname() {
		return this.bname;
	}

	public void setBname(String bname) {
		this.bname = bname;
	}

	@Column(name = "RESOURCEPATH", nullable = false, length = 300)

	public String getResourcepath() {
		return this.resourcepath;
	}

	public void setResourcepath(String resourcepath) {
		this.resourcepath = resourcepath;
	}

	@Column(name = "BTYPE", length = 64)

	public String getBtype() {
		return this.btype;
	}

	public void setBtype(String btype) {
		this.btype = btype;
	}

	@Column(name = "YEARS")

	public Integer getYears() {
		return this.years;
	}

	public void setYears(Integer years) {
		this.years = years;
	}

	@Column(name = "HEIGHT", precision = 22, scale = 0)

	public Double getHeight() {
		return this.height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	@Column(name = "PURPOSE", length = 300)

	public String getPurpose() {
		return this.purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	@Column(name = "TOTALAREA", precision = 22, scale = 0)

	public Double getTotalarea() {
		return this.totalarea;
	}

	public void setTotalarea(Double totalarea) {
		this.totalarea = totalarea;
	}

	@Column(name = "ABOVEAREA", precision = 22, scale = 0)

	public Double getAbovearea() {
		return this.abovearea;
	}

	public void setAbovearea(Double abovearea) {
		this.abovearea = abovearea;
	}

	@Column(name = "UNDERAREA", precision = 22, scale = 0)

	public Double getUnderarea() {
		return this.underarea;
	}

	public void setUnderarea(Double underarea) {
		this.underarea = underarea;
	}

	@Column(name = "CONSTRUCTIONUNITS", length = 256)

	public String getConstructionunits() {
		return this.constructionunits;
	}

	public void setConstructionunits(String constructionunits) {
		this.constructionunits = constructionunits;
	}

	@Column(name = "CENTRALLNG", precision = 22, scale = 0)

	public Double getCentrallng() {
		return this.centrallng;
	}

	public void setCentrallng(Double centrallng) {
		this.centrallng = centrallng;
	}

	@Column(name = "CENTRALLAT", precision = 22, scale = 0)

	public Double getCentrallat() {
		return this.centrallat;
	}

	public void setCentrallat(Double centrallat) {
		this.centrallat = centrallat;
	}

	@Column(name = "SYNOPSIS")

	public String getSynopsis() {
		return this.synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}
    @Column(name = "LAYERNUM")
    public String getLayernum() {
        return layernum;
    }

    public void setLayernum(String layernum) {
        this.layernum = layernum;
    }
    @Override
    public String toString() {
        return "Building{" +
            "id=" + id +
            ", unit=" + unit +
            ", number='" + number + '\'' +
            ", bname='" + bname + '\'' +
            ", resourcepath='" + resourcepath + '\'' +
            ", btype='" + btype + '\'' +
            ", years=" + years +
            ", height=" + height +
            ", purpose='" + purpose + '\'' +
            ", totalarea=" + totalarea +
            ", abovearea=" + abovearea +
            ", underarea=" + underarea +
            ", constructionunits='" + constructionunits + '\'' +
            ", centrallng=" + centrallng +
            ", centrallat=" + centrallat +
            ", synopsis='" + synopsis + '\'' +
            ", layernum='" + layernum + '\'' +
            '}';
    }



}
