package com.iezview.caas.entity;

import javax.persistence.*;

/**
 * Planmap entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "planmap", catalog = "caasdb")

public class Planmap implements java.io.Serializable {

	// Fields

	private Integer id;
	private Unit unit;
	private String pname;
	private String resourcepath;
	private String remark;

	// Constructors

	/** default constructor */
	public Planmap() {
	}

	/** minimal constructor */
	public Planmap(Integer id, Unit unit, String pname, String resourcepath) {
		this.id = id;
		this.unit = unit;
		this.pname = pname;
		this.resourcepath = resourcepath;
	}

	/** full constructor */
	public Planmap(Integer id, Unit unit, String pname, String resourcepath, String remark) {
		this.id = id;
		this.unit = unit;
		this.pname = pname;
		this.resourcepath = resourcepath;
		this.remark = remark;
	}

	// Property accessors
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNITID", nullable = false)

	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Column(name = "PNAME", nullable = false, length = 128)

	public String getPname() {
		return this.pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	@Column(name = "RESOURCEPATH", nullable = false, length = 256)

	public String getResourcepath() {
		return this.resourcepath;
	}

	public void setResourcepath(String resourcepath) {
		this.resourcepath = resourcepath;
	}

	@Column(name = "REMARK")

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

    @Override
    public String toString() {
        return "Planmap{" +
            "id=" + id +
//            ", unit=" + unit +
            ", pname='" + pname + '\'' +
            ", resourcepath='" + resourcepath + '\'' +
            ", remark='" + remark + '\'' +
            '}';
    }
}
