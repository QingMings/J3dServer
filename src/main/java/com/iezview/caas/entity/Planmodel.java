package com.iezview.caas.entity;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;
import javax.persistence.*;

/**
 * Planmodel entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "planmodel", catalog = "caasdb")

public class Planmodel implements java.io.Serializable {

	// Fields

	private Integer id;
	private Unit unit;
	private String mname;
	private Double centrallng;
	private Double centrallat;
	private Double centralheight;
	private String dataformat;
	private LocalDateTime acquisitiontime;
	private Integer altitudemode;
	private Double heading;
	private Double tilt;
	private Double roll;
	private Short xscale;
	private Short yscale;
	private Short zscale;
	private String tiletype;
	private String resourcepath;
	private String description;
	private Integer isload;

	// Constructors

	/** default constructor */
	public Planmodel() {
	}

	/** minimal constructor */
	public Planmodel(Integer id, Unit unit, String mname, String resourcepath) {
		this.id = id;
		this.unit = unit;
		this.mname = mname;
		this.resourcepath = resourcepath;
	}

	/** full constructor */
	public Planmodel(Integer id, Unit unit, String mname, Double centrallng, Double centrallat, Double centralheight,
			String dataformat, LocalDateTime acquisitiontime, Integer altitudemode, Double heading, Double tilt, Double roll,
			Short xscale, Short yscale, Short zscale, String tiletype, String resourcepath, String description,
			Integer isload) {
		this.id = id;
		this.unit = unit;
		this.mname = mname;
		this.centrallng = centrallng;
		this.centrallat = centrallat;
		this.centralheight = centralheight;
		this.dataformat = dataformat;
		this.acquisitiontime = acquisitiontime;
		this.altitudemode = altitudemode;
		this.heading = heading;
		this.tilt = tilt;
		this.roll = roll;
		this.xscale = xscale;
		this.yscale = yscale;
		this.zscale = zscale;
		this.tiletype = tiletype;
		this.resourcepath = resourcepath;
		this.description = description;
		this.isload = isload;
	}

	// Property accessors
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNITID", nullable = false)

	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Column(name = "MNAME", nullable = false, length = 128)

	public String getMname() {
		return this.mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	@Column(name = "CENTRALLNG", precision = 22, scale = 0)

	public Double getCentrallng() {
		return this.centrallng;
	}

	public void setCentrallng(Double centrallng) {
		this.centrallng = centrallng;
	}

	@Column(name = "CENTRALLAT", precision = 22, scale = 0)

	public Double getCentrallat() {
		return this.centrallat;
	}

	public void setCentrallat(Double centrallat) {
		this.centrallat = centrallat;
	}

	@Column(name = "CENTRALHEIGHT", precision = 22, scale = 0)

	public Double getCentralheight() {
		return this.centralheight;
	}

	public void setCentralheight(Double centralheight) {
		this.centralheight = centralheight;
	}

	@Column(name = "DATAFORMAT", length = 64)

	public String getDataformat() {
		return this.dataformat;
	}

	public void setDataformat(String dataformat) {
		this.dataformat = dataformat;
	}

	@Column(name = "ACQUISITIONTIME", length = 8)

	public LocalDateTime getAcquisitiontime() {
		return this.acquisitiontime;
	}

	public void setAcquisitiontime(LocalDateTime acquisitiontime) {
		this.acquisitiontime = acquisitiontime;
	}

	@Column(name = "ALTITUDEMODE")

	public Integer getAltitudemode() {
		return this.altitudemode;
	}

	public void setAltitudemode(Integer altitudemode) {
		this.altitudemode = altitudemode;
	}

	@Column(name = "HEADING", precision = 22, scale = 0)

	public Double getHeading() {
		return this.heading;
	}

	public void setHeading(Double heading) {
		this.heading = heading;
	}

	@Column(name = "TILT", precision = 22, scale = 0)

	public Double getTilt() {
		return this.tilt;
	}

	public void setTilt(Double tilt) {
		this.tilt = tilt;
	}

	@Column(name = "ROLL", precision = 22, scale = 0)

	public Double getRoll() {
		return this.roll;
	}

	public void setRoll(Double roll) {
		this.roll = roll;
	}

	@Column(name = "XSCALE")

	public Short getXscale() {
		return this.xscale;
	}

	public void setXscale(Short xscale) {
		this.xscale = xscale;
	}

	@Column(name = "YSCALE")

	public Short getYscale() {
		return this.yscale;
	}

	public void setYscale(Short yscale) {
		this.yscale = yscale;
	}

	@Column(name = "ZSCALE")

	public Short getZscale() {
		return this.zscale;
	}

	public void setZscale(Short zscale) {
		this.zscale = zscale;
	}

	@Column(name = "TILETYPE", length = 64)

	public String getTiletype() {
		return this.tiletype;
	}

	public void setTiletype(String tiletype) {
		this.tiletype = tiletype;
	}

	@Column(name = "RESOURCEPATH", nullable = false, length = 256)

	public String getResourcepath() {
		return this.resourcepath;
	}

	public void setResourcepath(String resourcepath) {
		this.resourcepath = resourcepath;
	}

	@Column(name = "DESCRIPTION")

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "ISLOAD")

	public Integer getIsload() {
		return this.isload;
	}

	public void setIsload(Integer isload) {
		this.isload = isload;
	}

    @Override
    public String toString() {
        return "Planmodel{" +
            "id=" + id +
//            ", unit=" + unit +
            ", mname='" + mname + '\'' +
            ", centrallng=" + centrallng +
            ", centrallat=" + centrallat +
            ", centralheight=" + centralheight +
            ", dataformat='" + dataformat + '\'' +
            ", acquisitiontime=" + acquisitiontime +
            ", altitudemode=" + altitudemode +
            ", heading=" + heading +
            ", tilt=" + tilt +
            ", roll=" + roll +
            ", xscale=" + xscale +
            ", yscale=" + yscale +
            ", zscale=" + zscale +
            ", tiletype='" + tiletype + '\'' +
            ", resourcepath='" + resourcepath + '\'' +
            ", description='" + description + '\'' +
            ", isload=" + isload +
            '}';
    }
}
