package com.iezview.caas.entity;

import javax.persistence.*;

/**
 * Remotesensemap entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "remotesensemap", catalog = "caasdb")

public class Remotesensemap implements java.io.Serializable {

	// Fields

	private Integer id;
	private Unit unit;
	private String sname;
	private Double maxlng;
	private Double maxlat;
	private Double minlng;
	private Double minlat;
	private String resourcepath;
	private String remark;

	// Constructors

	/** default constructor */
	public Remotesensemap() {
	}

	/** minimal constructor */
	public Remotesensemap(Integer id, Unit unit, String sname, String resourcepath) {
		this.id = id;
		this.unit = unit;
		this.sname = sname;
		this.resourcepath = resourcepath;
	}

	/** full constructor */
	public Remotesensemap(Integer id, Unit unit, String sname, Double maxlng, Double maxlat, Double minlng,
			Double minlat, String resourcepath, String remark) {
		this.id = id;
		this.unit = unit;
		this.sname = sname;
		this.maxlng = maxlng;
		this.maxlat = maxlat;
		this.minlng = minlng;
		this.minlat = minlat;
		this.resourcepath = resourcepath;
		this.remark = remark;
	}

	// Property accessors
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNITID", nullable = false)

	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Column(name = "SNAME", nullable = false, length = 128)

	public String getSname() {
		return this.sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	@Column(name = "MAXLNG", precision = 22, scale = 0)

	public Double getMaxlng() {
		return this.maxlng;
	}

	public void setMaxlng(Double maxlng) {
		this.maxlng = maxlng;
	}

	@Column(name = "MAXLAT", precision = 22, scale = 0)

	public Double getMaxlat() {
		return this.maxlat;
	}

	public void setMaxlat(Double maxlat) {
		this.maxlat = maxlat;
	}

	@Column(name = "MINLNG", precision = 22, scale = 0)

	public Double getMinlng() {
		return this.minlng;
	}

	public void setMinlng(Double minlng) {
		this.minlng = minlng;
	}

	@Column(name = "MINLAT", precision = 22, scale = 0)

	public Double getMinlat() {
		return this.minlat;
	}

	public void setMinlat(Double minlat) {
		this.minlat = minlat;
	}

	@Column(name = "RESOURCEPATH", nullable = false, length = 300)

	public String getResourcepath() {
		return this.resourcepath;
	}

	public void setResourcepath(String resourcepath) {
		this.resourcepath = resourcepath;
	}

	@Column(name = "REMARK")

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

    @Override
    public String toString() {
        return "Remotesensemap{" +
            "id=" + id +
//            ", unit=" + unit +
            ", sname='" + sname + '\'' +
            ", maxlng=" + maxlng +
            ", maxlat=" + maxlat +
            ", minlng=" + minlng +
            ", minlat=" + minlat +
            ", resourcepath='" + resourcepath + '\'' +
            ", remark='" + remark + '\'' +
            '}';
    }
}
