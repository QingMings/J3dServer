package com.iezview.caas.http.client;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * 上传接口定义
 *  <pre>
 *      上传类型
 *       "1": "遥感图",
 *       "2": "规划图",
 *       "3":"实景模型",
 *       "4":"规划模型",
 *       "5":"建筑图片",
 *       "6":"规划汇总表"
 *
 *  </pre>
 */
public interface CaasService {

    @GET("/caasServer/")
    Call<ResponseBody> hello();

    @POST("/caasServer/user/login")
    @FormUrlEncoded
    Call<ResponseBody> login(@Field("username") String username, @Field("password") String password);

    /**
     * 上传文件 研究所
     * @param unitId 研究所ID
     * @param typeId 上传类型
     * @param file   文件
     * @return
     */
    @POST("/caasServer/upload/{unitId}/{typeId}")
    @Multipart
    Call<ResponseBody> uplaodYanJiuSuo(@Path("unitId") String unitId, @Path("typeId") String typeId, @Part MultipartBody.Part file);

    /**
     * 上传文件  基地
     * @param unitId 研究所ID
     * @param unitId2 基地ID
     * @param typeId  上传类型
     * @param file   文件
     * @return
     */
    @POST("/caasServer/upload/{unitId}/{unitId2}/{typeId}")
    @Multipart
    Call<ResponseBody> uplaodJiDi(@Path("unitId") String unitId, @Path("unitId2") String unitId2, @Path("typeId") String typeId, @Part MultipartBody.Part file);

    /**
     * 下载文件  研究所
     * @param unitId 研究所ID
     * @param typeId  上传类型
     * @param resourceId  资源Id
     * @return
     */
    @Streaming
    @GET("/caasServer/download/{unitId}/{typeId}/{resourceId}")
    Call<ResponseBody> downloadYanJiuSuo(@Path("unitId") String unitId, @Path("typeId") String typeId, @Path("resourceId") String resourceId);

    /**
     * 下载文件  基地
     * @param unitId  研究所Id
     * @param unitId2  基地Id
     * @param typeId  上传类型
     * @param resourceId  资源Id
     * @return
     */
    @Streaming
    @GET("/caasServer/download/{unitId}/{unitId2}/{typeId}/{resourceId}")
    Call<ResponseBody> downloadJiDi(@Path("unitId") String unitId,@Path("unitId2") String unitId2, @Path("typeId") String typeId, @Path("resourceId") String resourceId);
}
