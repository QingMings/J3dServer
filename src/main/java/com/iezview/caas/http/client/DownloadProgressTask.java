package com.iezview.caas.http.client;

import com.iezview.caas.fx.ProgressTask;

/**
 * 下载进度任务
 */
public class DownloadProgressTask extends ProgressTask<CaasService> {
    public DownloadProgressTask(String baseUrl) {
        super(baseUrl);
    }
}
