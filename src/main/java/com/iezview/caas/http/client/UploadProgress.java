package com.iezview.caas.http.client;

import com.iezview.caas.fx.Progress;
import io.vertx.core.json.JsonObject;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * 上传进度
 */
public class UploadProgress extends Progress<CaasService> {

    private String[] mParams;

    public UploadProgress(MultipartBody.Part file, String[] params) {
        super(file);
        mParams = params;
    }

    /**
     * 处理 上传参数
     * @return
     * @throws Exception
     */
    @Override
    public CaasService call() throws Exception {
        if (mParams.length == 2) {
            call = this.mservice.uplaodYanJiuSuo(mParams[0], mParams[1], mfile);
            Response<ResponseBody> response = call.execute();
            JsonObject body = new JsonObject(response.body().string());
            reportError(body);
        } else if (mParams.length == 3) {
            call = this.mservice.uplaodJiDi(mParams[0], mParams[1], mParams[2], mfile);
            Response<ResponseBody> response = call.execute();
            JsonObject body = new JsonObject(response.body().string());
            reportError(body);
        } else {
            throw new RuntimeException("上传参数不正确");
        }


        return mservice;
    }

    /**
     * 报告错误
     * @param body
     */
    public void reportError(JsonObject body) {
        if (body.getString("result").equals("0")) {
            throw new RuntimeException(body.getString("resultMessage"));
        }else {
            //执行 成功 回调
            accept(body.getString("resultData"));
        }
    }

}
