package com.iezview.caas.http.client;

import com.iezview.caas.entity.*;
import com.iezview.caas.fx.ProgressTask;
import com.iezview.caas.http.retrofit.ProgressListener;
import com.iezview.caas.http.retrofit.body.ProgressRequestBody;
import javafx.application.Platform;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.controlsfx.control.Notifications;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * 上传进度处理器
 */
public class UploadProgressTask<T> extends ProgressTask<CaasService> {
    private String mfileUrl; //文件路径
    private T mentity; // 具体实体
    private Consumer<T> mconsumer; //交由具体实现,用户处理后续
    private String[] mParams; //上传参数

    /**
     * @param baseUrl  基础请求Url
     * @param fileUrl  文件路径
     * @param taskName 任务名称
     * @param entity   实体类型
     * @param consumer 回调函数
     * @param params   url需要的参数
     */
    public UploadProgressTask(String baseUrl, String fileUrl, String taskName, T entity, Consumer<T> consumer, String... params) {
        super(baseUrl);
        mfileUrl = fileUrl;
        mParams = params;
        mentity = entity;
        mconsumer = consumer;
        updateTitle(taskName); //设置标题
    }

    @Override
    protected CaasService call() throws Exception {
        CaasService service = createService(CaasService.class);
        File file = new File(mfileUrl);
        RequestBody requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), file);
        List<ProgressListener> listeners = new ArrayList<>();
        listeners.add(this);
        //构建 Progress对象
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), new ProgressRequestBody(requestFile, listeners, 150));
        progress = new UploadProgress(filePart, mParams);
        //注入service 和成功回调
        progress.setService(service, uuid -> {
            onSuccess(uuid);
        });

        return progress.call();
    }

    /**
     * 取消上传
     */
    @Override
    protected void cancelled() {
        progress.cancelled();
    }

    /**
     * 成功之后回调
     */
    @Override
    public void onSuccess(String uuid) {
        if (mconsumer != null && mentity != null) {
            //设置resourcesPath
            if (mentity instanceof Remotesensemap) {
                //遥感图
                ((Remotesensemap) mentity).setResourcepath(uuid);
            } else if (mentity instanceof Planmap) {
                //规划图
                ((Planmap) mentity).setResourcepath(uuid);
            } else if (mentity instanceof Obliquemodel) {
                //是景模型
                ((Obliquemodel) mentity).setResourcepath(uuid);
            } else if (mentity instanceof Planmodel) {
                //规划模型
                ((Planmodel) mentity).setResourcepath(uuid);
            } else if (mentity instanceof Building) {
                //建筑图片
                ((Building) mentity).setResourcepath(uuid);
            } else if (mentity instanceof Projectsummary) {
                //项目汇总表
                ((Projectsummary) mentity).setResourcepath(uuid);
            }
            mconsumer.accept(mentity);
        }
    }

    /**
     * 失败
     */
    @Override
    protected void failed() {
        Platform.runLater(() -> {
            Notifications.create()
                .title("Error")
                .text(getException().getMessage())
                .showError();
        });
    }

    /**
     * 成功
     */
    @Override
    protected void succeeded() {
        Platform.runLater(() -> {
            Notifications.create()
                .title("Success")
                .text("Task  success")
                .showInformation();
        });
    }
}
