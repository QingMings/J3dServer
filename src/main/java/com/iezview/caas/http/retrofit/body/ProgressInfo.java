package com.iezview.caas.http.retrofit.body;

/**
 * 上传进度消息封装
 */
public class ProgressInfo {


    private long currentBytes; //当前已上传或下载的总长度
    private long contentLength; //数据总长度
    private long intervalTime; //本次调用距离上一次被调用所间隔的时间(毫秒)
    private long eachBytes; //本次调用距离上一次被调用的间隔时间内上传或下载的byte长度
    private long id; //如果同一个 Url 地址,上一次的上传或下载操作都还没结束,
    //又开始了新的上传或下载操作(比如用户点击多次点击上传或下载同一个 Url 地址,当然你也可以在上层屏蔽掉用户的重复点击),
    //此 id (请求开始时的时间)就变得尤为重要,用来区分正在执行的进度信息,因为是以请求开始时的时间作为 id ,所以值越大,说明该请求越新
    private boolean finish; //进度是否完成


    public ProgressInfo(long id) {
        this.id = id;
    }

    void setCurrentbytes(long currentbytes) {
        this.currentBytes = currentbytes;
    }

    void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    void setIntervalTime(long intervalTime) {
        this.intervalTime = intervalTime;
    }

    void setEachBytes(long eachBytes) {
        this.eachBytes = eachBytes;
    }

    void setFinish(boolean finish) {
        this.finish = finish;
    }

    public long getCurrentbytes() {
        return currentBytes;
    }

    public long getContentLength() {
        return contentLength;
    }

    public long getIntervalTime() {
        return intervalTime;
    }

    public long getEachBytes() {
        return eachBytes;
    }

    public long getId() {
        return id;
    }

    public boolean isFinish() {
        return finish;
    }

    /**
     * 获取百分比,该计算舍去了小数点,如果你想得到更精确的值,请自行计算
     *
     * @return
     */
    public int getPercent() {
        if (getCurrentbytes() <= 0 || getContentLength() <= 0) return 0;
        return (int) ((100 * getCurrentbytes()) / getContentLength());
    }

    /**
     * 获取上传或下载网络速度,单位为byte/s,如果你想得到更精确的值,请自行计算
     *
     * @return
     */
    public long getSpeed() {
        if (getEachBytes() <= 0 || getIntervalTime() <= 0) return 0;
        return getEachBytes() * 1000 / getIntervalTime();
    }

    @Override
    public String toString() {
        return "ProgressInfo{" +
            "currentBytes=" + currentBytes +
            ", contentLength=" + contentLength +
            ", intervalTime=" + intervalTime +
            ", eachBytes=" + eachBytes +
            ", id=" + id +
            ", finish=" + finish +
            '}';
    }
}
