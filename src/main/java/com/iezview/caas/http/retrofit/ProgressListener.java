package com.iezview.caas.http.retrofit;

import com.iezview.caas.http.retrofit.body.ProgressInfo;

/**
 * 上传监听器
 */
public interface ProgressListener {

    void  onSuccess(String uuid);
    void  onProgress(ProgressInfo progressInfo);

    void onError(long id,Exception e);
}
