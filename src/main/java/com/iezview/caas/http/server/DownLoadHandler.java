package com.iezview.caas.http.server;

import com.iezview.caas.entity.Unit;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 文件下载处理程序
 */
public class DownLoadHandler extends AbstractHandler {
    private static Logger logger = LoggerFactory.getLogger(DownLoadHandler.class);

    /**
     *研究所数据下载
     * @param context
     */
    public static  void downloadYanJiuSuo(RoutingContext context){
        Path path = Paths.get(getRootPath());
        HttpServerRequest request = context.request();
        String unitId = request.getParam("unitId");//单位ID
        String typeId = request.getParam("typeId"); //下载类型
        String resourcesId = request.getParam("resourceId"); //资源id
        //判断都是数字
        if (isNumeric(unitId,typeId,resourcesId)){
            Unit  unit = unitDAO.findById(Integer.parseInt(unitId));
//            判断unit 是否是研究所
            if (unit.getUlevel()!=null && unit.getUlevel().equals(1)){
                String keyName = unit.getKeyname();
                if (isNotEmpty(keyName)){
                    path = path.resolve(keyName).resolve(getUnitKey());
                    String type = getSubStructurekey(typeId);
                    if (isNotEmpty(type)){
                        path = path.resolve(type);
                        String resource = getResourcePath(typeId,resourcesId);
                        if(isNotEmpty(resource)){
                            path=path.resolve(resource);
                            if (Files.exists(path)){
                                context.response().putHeader(HttpHeaders.CONTENT_TYPE,"application/octet-stream");
                                context.response().putHeader("Content-Disposition", "attachment; filename=\""+resource+"\"");
                                context.response().sendFile(path.toUri().getPath());
                            }else {
                                sendError("can't find File with resourceId ["+resourcesId+"]",context.response());
                            }

                        }else {
                            sendError("resource path with resourcesId :["+resourcesId+"] is Empty! ",context.response());
                        }

                    }else {
                        //typeId 非已知参数
                        sendError("TypeId value with " + typeId + " is unknown！", context.response());
                    }

                }else {
                    //单位存储映射字段不能为空
                    sendError("Unit keyName property must be not null!", context.response());
                }

            }else {
                // 无效的单位Id ,可能Ulevel 为空或者 ulevel 不等于1
                sendError("Invalid unitId ,maybe Ulevel is null or notEq 1", context.response());
            }

        }else {
            //单位Id不是数字
            sendError("unitId or type value  must be numeric", context.response());
        }


    }

    /**
     * 基地数据下载
     * @param context
     */
    public static  void downloadJidi(RoutingContext context){
        Path path = Paths.get(getRootPath());
        HttpServerRequest request = context.request();
        String unitId = request.getParam("unitId");//单位ID
        String unitId2= request.getParam("unitId2");//基地Id
        String typeId = request.getParam("typeId"); //下载类型
        String resourcesId = request.getParam("resourceId"); //资源id
        if (isNumeric(unitId,unitId2,typeId,resourcesId)){
            Unit unit2 = unitDAO.findById(Integer.parseInt(unitId2));
            if (unit2.getUlevel() != null && unit2.getUlevel().equals(2) && unit2.getParent().equals(Integer.parseInt(unitId))){
                Unit unit = unitDAO.findById(Integer.parseInt(unitId));
                if (isNotEmpty(unit.getKeyname(),unit2.getKeyname())){
                    path =path.resolve(unit.getKeyname()).resolve(getBaseUnitKey()).resolve(unit2.getKeyname());
                    String type = getSubStructurekey(typeId);
                    if (isNotEmpty(type)){
                        path = path.resolve(type);
                        String resource = getResourcePath(typeId,resourcesId);
                        if (isNotEmpty(resource)){
                            path=path.resolve(resource);
                            if (Files.exists(path)){
                                context.response().putHeader(HttpHeaders.CONTENT_TYPE,"application/octet-stream");
                                context.response().putHeader("Content-Disposition", "attachment; filename=\""+resource+"\"");
                                context.response().sendFile(path.toUri().getPath());
                            }else {
                                sendError("can't find File with resourceId ["+resourcesId+"]",context.response());
                            }
                        }else {
                            sendError("resource path with resourcesId :["+resourcesId+"] is Empty! ",context.response());
                        }

                    }else {

                        //typeId 非已知参数
                        sendError("TypeId value with " + typeId + " is unknown！", context.response());
                    }
                }else {
                    //基地存储映射字段不能为空
                    sendError("Unit keyName property or Unit2 keyName property must be not null!", context.response());
                }
            }else {
                // 无效的基地Id ,可能Ulevel 为空或者 ulevel 不等于2 或者 基地的父级id 与 参数 unitId 不相等
                sendError("Invalid unitId2 ,maybe Ulevel is null or notEq 2 or parentid noEq unitId", context.response());
            }
        }else {
            //单位Id不是数字
            sendError("unitId or type value  must be numeric", context.response());
        }
    }
}
