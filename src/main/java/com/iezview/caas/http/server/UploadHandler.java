package com.iezview.caas.http.server;

import com.iezview.caas.dao.IBuildingDAO;
import com.iezview.caas.dao.IUnitDAO;
import com.iezview.caas.daoimpl.BuildingDAO;
import com.iezview.caas.daoimpl.UnitDAO;
import com.iezview.caas.entity.Unit;
import com.iezview.caas.utils.ResourcesUtils;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * 文件上传处理程序
 *
 * 目前上传仅仅做了文件的保存，保持了存储目录结构，
 * 对资源所属数据的修改打算交由客户端完成
 *
 * 上传成功返回 资源重命名之后的名字
 * 失败提供详细信息
 */
public class UploadHandler extends AbstractHandler {
    private static Logger logger = LoggerFactory.getLogger(UploadHandler.class);

    /**
     * 研究所上传 处理程序
     *
     * @param context
     */
    public static void uploadYanJiuSuo(RoutingContext context) {
        Path path = Paths.get(getRootPath());
        HttpServerRequest request = context.request();
        request.setExpectMultipart(true);
        String unitId = request.getParam("unitId");
        String typeId = request.getParam("typeId");
        //判断 院所Id和上传类型Id都是数字
        if (isNumeric(unitId,typeId)) {
            Unit unit = unitDAO.findById(Integer.parseInt(unitId));
            //根据院所的Ulevel属性判断 是否是研究所
            if (unit.getUlevel() != null && unit.getUlevel().equals(1)) {
                String keyName = unit.getKeyname();
                //判断研究所存储映射名字不为空
                if (isNotEmpty(keyName)) {
                    path = path.resolve(keyName).resolve(getUnitKey());
                    String type = getSubStructurekey(typeId);
                    //判断上传类型是否有效
                    if (isNotEmpty(type)) {
                        Path finalPath = path.resolve(type);
                        //UUID 重命名文件
                        final String[] fileName = {UUID.randomUUID().toString()};
                        //判断文件存储路径是否存在
                        if (!createDirectories(finalPath, context.response())){
                            return;
                        }

//                        request.headers().forEach(stringStringEntry -> System.out.println(stringStringEntry.getKey()+"::"+stringStringEntry.getValue()));
                        request.uploadHandler(upload -> {
                            fileName[0] +="."+FilenameUtils.getExtension(upload.filename());
                            String uploadFileName = new File(finalPath.toUri().getPath(), fileName[0]).getPath();
                            upload.streamToFileSystem(uploadFileName);
                            logger.info("File [" + upload.filename() + "]  will be renamed to [" + fileName[0] + "] ");
                            logger.info("File [" + fileName[0] + "]  will be saved to [" + finalPath + "] ");

                            upload.exceptionHandler(cause->{
                                throw new RuntimeException(cause);
                            });

                        });

                        request.endHandler(a-> sendSuccess(fileName[0], context.response()));
                    } else {
                        //typeId 非已知参数
                        sendError("TypeId value with " + typeId + " is unknown！", context.response());
                    }

                } else {
                    //单位存储映射字段不能为空
                    sendError("Unit keyName property must be not null!", context.response());
                }
            } else {
                // 无效的单位Id ,可能Ulevel 为空或者 ulevel 不等于1
                sendError("Invalid unitId ,maybe Ulevel is null or notEq 1", context.response());
            }
        } else {
            //单位Id不是数字
            sendError("unitId or type value  must be numeric", context.response());
        }

    }

    /**
     * 基地 上传处理程序
     *
     * @param context
     */
    public static void uploadJiDi(RoutingContext context) {
        Path path = Paths.get(getRootPath());
        HttpServerRequest request = context.request();
        request.setExpectMultipart(true);
        String unitId = request.getParam("unitId");//研究所
        String unitId2 = request.getParam("unitId2");//基地
        String typeId = request.getParam("typeId");//上传类型
        //判断 研究所id , 基地Id,typeId 都是 数字
        if (isNumeric(unitId,unitId2,typeId)) {
            //基地  unit2
            Unit unit2 = unitDAO.findById(Integer.parseInt(unitId2));
            //判断 基地的ulevel不为空，并且 ulevel ==2 并且 基地的父级id ==unitid
            if (unit2.getUlevel() != null && unit2.getUlevel().equals(2) && unit2.getParent().equals(Integer.parseInt(unitId))) {
                //研究所   unit
                Unit unit = unitDAO.findById(Integer.parseInt(unitId));
                //判断 基地和研究所的存储映射都不为空
                if (isNotEmpty(unit.getKeyname(),unit2.getKeyname())) {
                    path = path.resolve(unit.getKeyname()).resolve(getBaseUnitKey()).resolve(unit2.getKeyname());
                    String type =getSubStructurekey(typeId);
                    //判断上传类型是否有效
                    if (isNotEmpty(type)) {
                        Path finalPath = path.resolve(type);
                        //UUID 重命名文件
                        final String[] fileName = {UUID.randomUUID().toString()};
                        //判断文件存储路径是否存在
                        if (!createDirectories(finalPath, context.response())){
                            return;
                        }
                        request.uploadHandler(upload -> {
                            fileName[0] +="."+FilenameUtils.getExtension(upload.filename());
                            String uploadFileName = new File(finalPath.toUri().getPath(), fileName[0]).getPath();
                            upload.streamToFileSystem(uploadFileName);
                            logger.info("File [" + upload.filename() + "]  will be renamed to [" + fileName[0] + "] ");
                            logger.info("File [" + fileName[0] + "]  will be saved to [" + finalPath + "] ");
                            upload.exceptionHandler(cause->{
                                throw new RuntimeException(cause);
                            });

                        });
                        request.endHandler(a-> sendSuccess(fileName[0], context.response()));
                    } else {
                        //typeId 非已知参数
                        sendError("TypeId value with " + typeId + " is unknown！", context.response());
                    }
                } else {
                    //基地存储映射字段不能为空
                    sendError("Unit keyName property or Unit2 keyName property must be not null!", context.response());
                }

            } else {
                // 无效的基地Id ,可能Ulevel 为空或者 ulevel 不等于2 或者 基地的父级id 与 参数 unitId 不相等
                sendError("Invalid unitId2 ,maybe Ulevel is null or notEq 2 or parentid noEq unitId", context.response());
            }

        } else {
            //单位Id 或 基地Id 或 typeId不是数字
            sendError("unitId or unitId2 or typeId   must be numeric", context.response());
        }
    }


}
