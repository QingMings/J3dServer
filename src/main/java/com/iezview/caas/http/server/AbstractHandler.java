package com.iezview.caas.http.server;

import com.iezview.caas.dao.*;
import com.iezview.caas.daoimpl.*;
import com.iezview.caas.entity.*;
import com.iezview.caas.utils.ResourcesUtils;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 *
 */
public abstract class AbstractHandler {
    public static final String UnitID = "unitId"; //json key
    public static final String ResourcesID = "id"; //json key
    public static final String TypeId="typeId";
    public static final String URL = "url"; //json key
    public static final String Resources = "resources"; //json key
    public static final String Child = "children"; //json key
    public static IUnitDAO unitDAO = new UnitDAO();
    public static IBuildingDAO buildingDAO = new BuildingDAO();
    public static IObliquemodelDAO obliquemodelDAO = new ObliquemodelDAO();
    public static IPlanmodelDAO planmodelDAO = new PlanmodelDAO();
    public static IPlanmapDAO planmapDAO = new PlanmapDAO();
    public static IRemotesensemapDAO remotesensemapDAO = new RemotesensemapDAO();
    public static IProjectsummaryDAO projectsummaryDAO = new ProjectsummaryDAO();
    private static io.vertx.core.logging.Logger logger = LoggerFactory.getLogger(AbstractHandler.class);

    public static void sendError(String errorMessage, HttpServerResponse response) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("result", "0");
        jsonObject.put("resultMessage", errorMessage);
        response.putHeader("content-type", "application/json; charset=utf-8").end(jsonObject.encode());
    }

    public static void sendSuccess(HttpServerResponse response) {
        response
            .setStatusCode(200)
            .putHeader("content-type", "application/json; charset=utf-8")
            .end();
    }

    public static void sendSuccess(String responseBody, HttpServerResponse response) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("result", "1");
        jsonObject.put("resultData", responseBody);
        response
            .setStatusCode(200)
            .putHeader("content-type", "application/json; charset=utf-8")
            .end(jsonObject.encode());
    }

    public static void sendSuccess(JsonObject responseBody, HttpServerResponse response) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("result", "1");
        jsonObject.put("resultData", responseBody);
        response
            .setStatusCode(200)
            .putHeader("content-type", "application/json; charset=utf-8")
            .end(jsonObject.encode());
    }

    public static void sendSuccess(JsonArray responseBody, HttpServerResponse response) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("result", "1");
        jsonObject.put("resultData", responseBody);
        response
            .setStatusCode(200)
            .putHeader("content-type", "application/json; charset=utf-8")
            .end(jsonObject.encode());
    }

    /**
     * 创建必要的文件夹
     *
     * @param finalPath 文件夹路径
     * @param response  HttpServerResponse
     */
    public static boolean createDirectories(Path finalPath, HttpServerResponse response) {
        boolean flag = true;
        if (Files.notExists(finalPath)) {
            try {
                Files.createDirectories(finalPath);
                flag = true;
            } catch (IOException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
                flag = false;
                sendError("An error occurred while creating the save directory", response);
            }
        }
        return flag;
    }

    /**
     * 判断是否都是数字
     *
     * @param params
     * @return
     */
    public static boolean isNumeric(String... params) {
        boolean flag = true;
        for (String param : params) {
            if (!StringUtils.isNumeric(param)) {
                flag = false;
            }
        }
        return flag;
    }

    /**
     * 判断不为空
     *
     * @param params
     * @return
     */
    public static boolean isNotEmpty(String... params) {
        boolean flag = true;
        for (String param : params) {
            if (!StringUtils.isNotEmpty(param)) {
                flag = false;
            }
        }
        return flag;
    }

    /**
     * 包装ResourcesUtils
     *
     * @return
     */
    public static String getUnitKey() {
        return ResourcesUtils.getInstance().getUnitKey();
    }
    /**
     * 包装ResourcesUtils
     *
     * @return
     */
    public static String getBaseUnitKey() {
        return ResourcesUtils.getInstance().getBaseUnitKey();
    }

    /**
     * 包装ResourcesUtils
     * @param typeId 上传类型
     * @return
     */
    public static String getSubStructurekey(String typeId) {
        return ResourcesUtils.getInstance().getSubStructurekey(typeId);
    }

    /**
     * 包装ResourcesUtils
     * @return
     */
    public static String getRootPath() {
        return ResourcesUtils.getInstance().getRootPath();
    }

    /**
     * 获取资源路径
     *
     * @param typeId  上传类型
     * @param resourcesId   资源主键
     * @return
     */
    public static String getResourcePath(String typeId, String resourcesId) {
        StringBuilder sb = new StringBuilder();
        Integer rId = Integer.parseInt(resourcesId);
        switch (typeId) {
            case "1":
                // 遥感图
                Remotesensemap remotesensemap = remotesensemapDAO.findById(rId);
                if (remotesensemap != null && isNotEmpty(remotesensemap.getResourcepath())) {
                    sb.append(remotesensemap.getResourcepath());
                }

                break;
            case "2":
                Planmap planmap = planmapDAO.findById(rId);
                if (planmap != null && isNotEmpty(planmap.getResourcepath())) {
                    sb.append(planmap.getResourcepath());
                }
                break;
            case "3":
                Obliquemodel obliquemodel = obliquemodelDAO.findById(rId);
                if (obliquemodel != null && isNotEmpty(obliquemodel.getResourcepath())) {
                    sb.append(obliquemodel.getResourcepath());
                }
                break;
            case "4":
                Planmodel planmodel = planmodelDAO.findById(rId);
                if (planmodel != null && isNotEmpty(planmodel.getResourcepath())) {
                    sb.append(planmodel.getResourcepath());
                }
                break;
            case "5":
                Building building = buildingDAO.findById(rId);
                if (building != null && isNotEmpty(building.getResourcepath())) {
                    sb.append(building.getResourcepath());
                }
                break;
            case "6":
                Projectsummary projectsummary = projectsummaryDAO.findById(rId);
                if (projectsummary != null && isNotEmpty(projectsummary.getResourcepath())) {
                    sb.append(projectsummary.getResourcepath());
                }
                break;
            default:

                break;
        }

        return sb.toString();
    }

    /**
     * 获取研究所资源
     *
     * @param baseUrl 基础url
     * @param unit  研究所
     * @return
     */
    public static JsonObject getResources(String baseUrl, Unit unit) {
        JsonObject  resources = new JsonObject();
        JsonArray arrayRemotesensemaps = new JsonArray();
        unit.getRemotesensemaps().stream().filter(remotesensemap -> isNotEmpty(remotesensemap.getResourcepath())).forEach(remotesensemap -> {
            arrayRemotesensemaps.add(new JsonObject().put(ResourcesID, remotesensemap.getId()).put(URL, baseUrl + "/1/" + remotesensemap.getId()));
        });
        JsonArray arrayPlanmaps = new JsonArray();
        unit.getPlanmaps().stream().filter(planmap -> isNotEmpty(planmap.getResourcepath())).forEach(planmap -> {
            arrayPlanmaps.add(new JsonObject().put(ResourcesID, planmap.getId()).put(URL, baseUrl + "/2/" + planmap.getId()));
        });
        JsonArray arrayObliquemodel = new JsonArray();
        unit.getObliquemodels().stream().filter(obliquemodel -> isNotEmpty(obliquemodel.getResourcepath())).forEach(obliquemodel -> {
            arrayObliquemodel.add(new JsonObject().put(ResourcesID, obliquemodel.getId()).put(URL, baseUrl + "/3/" + obliquemodel.getId()));
        });
        JsonArray arrayPlanmodel = new JsonArray();
        unit.getPlanmodels().stream().filter(planmodel -> isNotEmpty(planmodel.getResourcepath())).forEach(planmodel -> {
            arrayPlanmodel.add(new JsonObject().put(ResourcesID, planmodel.getId()).put(URL, baseUrl + "/4/" + planmodel.getId()));
        });
        JsonArray arrayBuilding = new JsonArray();
        unit.getBuildings().stream().filter(building -> isNotEmpty(building.getResourcepath())).forEach(building -> {
            arrayBuilding.add(new JsonObject().put(ResourcesID, building.getId()).put(URL, baseUrl + "/5/" + building.getId()));
        });

        JsonArray arrayProjectsummary = new JsonArray();
        unit.getProjectsummaries().stream().filter(projectsummary -> isNotEmpty(projectsummary.getResourcepath())).forEach(projectsummary -> {
            arrayProjectsummary.add(new JsonObject().put(ResourcesID, projectsummary.getId()).put(URL, baseUrl + "/6/" + projectsummary.getId()));
        });
            if(isNotEmpty(arrayRemotesensemaps,arrayPlanmaps,arrayObliquemodel,arrayPlanmodel,arrayBuilding,arrayProjectsummary)){
                resources.put("remoteSensingImage", arrayRemotesensemaps)
                    .put("planningImage", arrayPlanmaps)
                    .put("liveModel", arrayObliquemodel)
                    .put("planningModel", arrayPlanmodel)
                    .put("buildingImage", arrayBuilding)
                    .put("planningTable", arrayProjectsummary);
            }
        return resources;
    }


    /**
     * 获取基地资源
     * @param unit  研究所
     * @param request  HttpServerRequest
     * @return
     */
    public static JsonArray getChildrenResources(Unit unit, HttpServerRequest request) {
        List<Unit> units = unitDAO.findJidiByYanJiuSuo(unit);
        StringBuilder baseUrl = getBaseUrl(request).append(unit.getId()).append("/");
        JsonArray resourcesArray = new JsonArray();
        units.forEach(jidi -> {
            JsonObject resources = getResources(baseUrl.append(jidi.getId()).toString(), jidi);
            if (!resources.isEmpty()){
                JsonObject jidiJson = new JsonObject();
                jidiJson.put(UnitID, jidi.getId())
                    .put(Resources,resources);
                resourcesArray.add(jidiJson);
            }

        });

        return resourcesArray;
    }


    /**
     * 获取研究所资源
     *
     * @param baseUrl 基础url
     * @param unit  研究所
     * @return
     */
    public static JsonArray getResourcesList(String baseUrl, Unit unit) {
        JsonArray  resources = new JsonArray();
        JsonArray arrayRemotesensemaps = new JsonArray();
        unit.getRemotesensemaps().stream().filter(remotesensemap -> isNotEmpty(remotesensemap.getResourcepath())).forEach(remotesensemap -> {
            arrayRemotesensemaps.add(new JsonObject().put(ResourcesID, remotesensemap.getId()).put(TypeId,1).put(URL, baseUrl + "/1/" + remotesensemap.getId()));
        });
        JsonArray arrayPlanmaps = new JsonArray();
        unit.getPlanmaps().stream().filter(planmap -> isNotEmpty(planmap.getResourcepath())).forEach(planmap -> {
            arrayPlanmaps.add(new JsonObject().put(ResourcesID, planmap.getId()).put(TypeId,2).put(URL, baseUrl + "/2/" + planmap.getId()));
        });
        JsonArray arrayObliquemodel = new JsonArray();
        unit.getObliquemodels().stream().filter(obliquemodel -> isNotEmpty(obliquemodel.getResourcepath())).forEach(obliquemodel -> {
            arrayObliquemodel.add(new JsonObject().put(ResourcesID, obliquemodel.getId()).put(TypeId,3).put(URL, baseUrl + "/3/" + obliquemodel.getId()));
        });
        JsonArray arrayPlanmodel = new JsonArray();
        unit.getPlanmodels().stream().filter(planmodel -> isNotEmpty(planmodel.getResourcepath())).forEach(planmodel -> {
            arrayPlanmodel.add(new JsonObject().put(ResourcesID, planmodel.getId()).put(TypeId,4).put(URL, baseUrl + "/4/" + planmodel.getId()));
        });
        JsonArray arrayBuilding = new JsonArray();
        unit.getBuildings().stream().filter(building -> isNotEmpty(building.getResourcepath())).forEach(building -> {
            arrayBuilding.add(new JsonObject().put(ResourcesID, building.getId()).put(TypeId,5).put(URL, baseUrl + "/5/" + building.getId()));
        });

        JsonArray arrayProjectsummary = new JsonArray();
        unit.getProjectsummaries().stream().filter(projectsummary -> isNotEmpty(projectsummary.getResourcepath())).forEach(projectsummary -> {
            arrayProjectsummary.add(new JsonObject().put(ResourcesID, projectsummary.getId()).put(TypeId,6).put(URL, baseUrl + "/6/" + projectsummary.getId()));
        });
        if(isNotEmpty(arrayRemotesensemaps,arrayPlanmaps,arrayObliquemodel,arrayPlanmodel,arrayBuilding,arrayProjectsummary)){
            resources.addAll(arrayRemotesensemaps);
            resources.addAll(arrayPlanmaps);
            resources.addAll(arrayObliquemodel);
            resources.addAll(arrayPlanmodel);
            resources.addAll(arrayBuilding);
            resources.addAll(arrayProjectsummary);
        }
        return resources;
    }

    /**
     * 获取基地资源
     * @param unit  研究所
     * @param request  HttpServerRequest
     * @return
     */
    public static JsonArray getChildrenResourcesList(Unit unit, HttpServerRequest request) {
        List<Unit> units = unitDAO.findJidiByYanJiuSuo(unit);
        StringBuilder baseUrl = getBaseUrl(request).append(unit.getId()).append("/");
        JsonArray resourcesArray = new JsonArray();
        units.forEach(jidi -> {
            JsonArray resources = getResourcesList(baseUrl.append(jidi.getId()).toString(), jidi);
            if (!resources.isEmpty()){
                resourcesArray.addAll(resources);
            }
        });

        return resourcesArray;
    }
    /**
     * 判断JsonArray是不是空的
     * @param jsonArrays
     * @return
     */
    public static boolean isNotEmpty(JsonArray... jsonArrays){
        boolean flag = false;
        for (JsonArray jsonArray : jsonArrays) {
            if (!jsonArray.isEmpty()){
                flag=true;
            }
        }
        return flag;
    }

    /**
     * 获取基础Url
     * @param request
     * @return
     */
    public static StringBuilder getBaseUrl(HttpServerRequest request) {
        return new StringBuilder("http://")
            .append(request.localAddress().host()).append(":")
            .append(request.localAddress().port())
            .append("/caasServer/download/");
    }

    public static void faiureHandler(RoutingContext context){
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("result", "0");
        jsonObject.put("resultMessage", "404");
        context.response().setStatusCode(404).putHeader(HttpHeaders.CONTENT_TYPE,"application/json").end(jsonObject.encode());
    }
}


































