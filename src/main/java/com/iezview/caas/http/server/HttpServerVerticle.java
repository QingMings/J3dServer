package com.iezview.caas.http.server;

import com.iezview.caas.dao.IUsersDAO;
import com.iezview.caas.daoimpl.UsersDAO;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.FaviconHandler;
import org.jfree.data.general.Dataset;

import java.io.File;

/**
 * HttpServer
 */
public class HttpServerVerticle extends AbstractVerticle {
    private final Logger logger;
    private final IUsersDAO usersDAO;
    public HttpServerVerticle(){
        logger= LoggerFactory.getLogger(HttpServerVerticle.class);
        usersDAO =new UsersDAO();
    }
    @Override
    public void start() throws Exception {
        JsonObject server =config().getJsonObject("server");
        vertx.createHttpServer().requestHandler(createRouter()::accept).listen(server.getInteger("port"),server.getString("address"));
    }

    @Override
    public void stop() throws Exception {
        vertx.close();
    }

    private Router createRouter(){
        Router mainRouter = Router.router(vertx);
        mainRouter.route().handler(CorsHandler.create("*")
            .allowedMethod(HttpMethod.GET)
            .allowedMethod(HttpMethod.POST)
            .allowedMethod(HttpMethod.PUT)
            .allowedMethod(HttpMethod.DELETE)
            .allowedMethod(HttpMethod.OPTIONS)
            .allowedMethod(HttpMethod.CONNECT)
            .allowedMethod(HttpMethod.OTHER)
            .allowedHeader("Origin")
            .allowedHeader("X-Requested-With")
            .allowedHeader("Content-Type")
            .allowedHeader("Accept"));
        mainRouter.route().handler(FaviconHandler.create());
        Router  router =Router.router(vertx);

        router.get("/").handler(this::hello);

        ///Allow the router to retrieve the body of any request made
        router.route("/user/login").handler(BodyHandler.create(config().getJsonObject("server").getString("rootStoragePath")));
        router.post("/user/login").handler(this::login);
        router.post("/upload/:unitId/:unitId2/:typeId").handler(UploadHandler::uploadJiDi);
        router.post("/upload/:unitId/:typeId").handler(UploadHandler::uploadYanJiuSuo);

        router.get("/download/:unitId/:typeId/:resourceId").handler(DownLoadHandler::downloadYanJiuSuo);
        router.get("/download/:unitId/:unitId2/:typeId/:resourceId").handler(DownLoadHandler::downloadJidi);


        router.get("/dataSync").handler(DataSyncHandler::dataSync);
        router.get("/dataSync/list").handler(DataSyncHandler::dataSyncList);
        router.get("/dataSync/:unitId").handler(DataSyncHandler::dataSyncSub);
        router.get("/dataSync/:unitId/self").handler(DataSyncHandler::dataSyncSelf);
        router.route().last().handler(AbstractHandler::faiureHandler);
        mainRouter.mountSubRouter("/caasServer",router);
        return mainRouter;
    }

    private void hello(RoutingContext context){
        context.response().putHeader("content-type","text/plain").end("hello from caasServer!");
    }

    private  void login(RoutingContext context){
        String username=context.request().getParam("username");
        String password=context.request().getParam("password");
        if (usersDAO.doLogin(username,password)) {
            context.response().putHeader("content-type","application/json; charset=utf-8").end(new JsonObject().put("result","1").put("resultMessage","success").encode());
        }else {
            context.response().putHeader("content-type","application/json; charset=utf-8").end(new JsonObject().put("result","0").put("resultMessage",usersDAO.error()).encode());
        }
    }


    private  void  testRestParam(RoutingContext context){
        HttpServerRequest request =context.request();
        request.setExpectMultipart(true);
        String unitid =request.getParam("unitId");
        String buildingId =request.getParam("buildingId");
        String type =request.getParam("type");
        request.uploadHandler(upload->{
            String uploadFileName = new File(config().getJsonObject("server").getString("rootStoragePath"),upload.filename()).getPath();
            upload.streamToFileSystem(uploadFileName);
        });

        context.response().putHeader("content-type","application/json").end(new JsonObject().put("unidId",unitid).put("buildingId",buildingId).put("type",type).encode());
    }
}

