package com.iezview.caas.http.server;

import com.iezview.caas.entity.Unit;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.List;

/**
 * 数据同步
 */
public class DataSyncHandler extends AbstractHandler {

    /**
     * 同步所有数据
     * 返回Json 列表
     *
     * @param context
     */
    public static void dataSync(RoutingContext context) {
       HttpServerRequest request = context.request();
        JsonArray  unitJsonArray = new JsonArray();
        List<Unit>  units = unitDAO.findAllYanJiuSuo();
        units.forEach(unit -> {
            String  baseUrl = getBaseUrl(request).append(unit.getId()).toString();
            JsonObject  resources =getResources(baseUrl,unit);
            JsonArray   childrenResources = getChildrenResources(unit,request);

            if (!(resources.isEmpty()&&childrenResources.isEmpty())){
                //resources 和  childresources 都不为空
                JsonObject  unitJson = new JsonObject();
                unitJson.put(UnitID,unit.getId());
                unitJson.put(Resources,resources);
                unitJson.put(Child,childrenResources);
                unitJsonArray.add(unitJson);
            }
        });
        sendSuccess(unitJsonArray,context.response());
    }

    /**
     * 返回所有资源列表
     * @param context
     */
    public static void dataSyncList(RoutingContext context){
        HttpServerRequest request = context.request();
        JsonArray  resourcesArray = new JsonArray();
        List<Unit>  units = unitDAO.findAllYanJiuSuo();
        units.forEach(unit -> {
            String  baseUrl = getBaseUrl(request).append(unit.getId()).toString();
               resourcesArray.addAll(getResourcesList(baseUrl,unit));
               resourcesArray.addAll(getChildrenResourcesList(unit,request));

        });
        sendSuccess(resourcesArray,context.response());
    }
    /**
     * 同步部分数据
     *
     * @param context
     */
    public static void dataSyncSub(RoutingContext context) {
        HttpServerRequest request = context.request();
        String unitId = request.getParam("unitId");
        JsonArray unitJsonArray = new JsonArray();
        if (isNumeric(unitId)) {
            Unit  unit = unitDAO.findById(Integer.parseInt(unitId));
            JsonObject  unitJson = new JsonObject();
            if (unit != null && unit.getUlevel() != null && unit.getUlevel().equals(1)){
                //研究所数据
                String baseUrl = getBaseUrl(request).append(unit.getId()).toString();
                unitJson.put(UnitID, unit.getId());
                unitJson.put(Resources, getResources(baseUrl, unit));
                unitJson.put(Child,getChildrenResources(unit,request));
                sendSuccess(unitJsonArray.add(unitJson),context.response());
            }else {
                // 无效的单位Id ,可能Ulevel 为空或者 ulevel 不等于1
                sendError("Invalid unitId ,maybe Ulevel is null ", context.response());
            }

        } else {
            //单位Id
            sendError("unitId or unitId2 or typeId   must be numeric", context.response());
        }

    }

    /**
     * 同步研究所本部数据
     *
     * @param context
     */
    public static void dataSyncSelf(RoutingContext context) {
        HttpServerRequest request = context.request();
        JsonArray unitJsonArray = new JsonArray();
        String unitId = request.getParam("unitId");
        if (isNumeric(unitId)) {
            Unit unit = unitDAO.findById(Integer.parseInt(unitId));
            JsonObject unitJson = new JsonObject();
            if (unit != null && unit.getUlevel() != null && unit.getUlevel().equals(1)) {
                //研究所数据
                String baseUrl = getBaseUrl(request).append(unit.getId()).toString();
                unitJson.put(UnitID, unit.getId());
                unitJson.put(Resources, getResources(baseUrl, unit));
                sendSuccess(unitJsonArray.add(unitJson), context.response());
            }else if(unit != null && unit.getUlevel() != null && unit.getUlevel().equals(2)){
                //基地数据
                String baseUrl = getBaseUrl(request).append(unit.getParent()).append("/").append(unit.getId()).toString();
                JsonObject  resources=getResources(baseUrl, unit);
                if (!resources.isEmpty()){
                    unitJson.put(UnitID,unit.getId());
                    unitJson.put(Resources,resources);
                }
                sendSuccess(unitJsonArray.add(unitJson),context.response());
            } else {
                // 无效的单位Id ,可能Ulevel 为空或者 ulevel 不等于1
                sendError("Invalid unitId ,maybe Ulevel is null or notEq 1", context.response());
            }
        } else {
            //单位Id
            sendError("unitId or unitId2 or typeId   must be numeric", context.response());
        }
    }


}
