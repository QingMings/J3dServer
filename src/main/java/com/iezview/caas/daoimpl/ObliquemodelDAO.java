package com.iezview.caas.daoimpl;

import java.sql.Time;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.iezview.caas.dao.IObliquemodelDAO;
import com.iezview.caas.entity.Obliquemodel;

/**
 * A data access object (DAO) providing persistence and search support for
 * Obliquemodel entities. Transaction control of the save(), update() and
 * delete() operations must be handled externally by senders of these methods or
 * must be manually added to each of these methods for data to be persisted to
 * the JPA datastore.
 *
 * @see Obliquemodel
 * @author MyEclipse Persistence Tools
 */
public class ObliquemodelDAO implements IObliquemodelDAO {
	// property constants
	public static final String NAME = "name";
	public static final String CENTRALLNG = "centrallng";
	public static final String CENTRALLAT = "centrallat";
	public static final String CENTRALHEIGHT = "centralheight";
	public static final String DATAFORMAT = "dataformat";
	public static final String ALTITUDEMODE = "altitudemode";
	public static final String HEADING = "heading";
	public static final String TILT = "tilt";
	public static final String ROLL = "roll";
	public static final String XSCALE = "xscale";
	public static final String YSCALE = "yscale";
	public static final String ZSCALE = "zscale";
	public static final String TILETYPE = "tiletype";
	public static final String TILEMIN = "tilemin";
	public static final String TILEMAX = "tilemax";
	public static final String TILEQUANTITY = "tilequantity";
	public static final String RESOURCEPATH = "resourcepath";
	public static final String DESCRIPTION = "description";
	public static final String ISLOAD = "isload";

	private EntityManager getEntityManager() {
		return EntityManagerHelper.getEntityManager();
	}

	/**
	 * Perform an initial save of a previously unsaved Obliquemodel entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * ObliquemodelDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Obliquemodel entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Obliquemodel entity) {
		EntityManagerHelper.log("saving Obliquemodel instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			getEntityManager().persist(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Obliquemodel entity. This operation must be performed
	 * within the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * ObliquemodelDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Obliquemodel entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Obliquemodel entity) {
		EntityManagerHelper.log("deleting Obliquemodel instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			entity = getEntityManager().getReference(Obliquemodel.class, entity.getId());
			getEntityManager().remove(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Obliquemodel entity and return it or a copy of
	 * it to the sender. A copy of the Obliquemodel entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = ObliquemodelDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Obliquemodel entity to update
	 * @return Obliquemodel the persisted Obliquemodel entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Obliquemodel update(Obliquemodel entity) {
		EntityManagerHelper.log("updating Obliquemodel instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			Obliquemodel result = getEntityManager().merge(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Obliquemodel findById(Integer id) {
		EntityManagerHelper.log("finding Obliquemodel instance with id: " + id, Level.INFO, null);
		try {
            getEntityManager().clear();//禁用hibernate 缓存  xml配置不起作用
			Obliquemodel instance = getEntityManager().find(Obliquemodel.class, id);
			return instance;
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Obliquemodel entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Obliquemodel property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<Obliquemodel> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<Obliquemodel> findByProperty(String propertyName, final Object value,
			final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding Obliquemodel instance with property: " + propertyName + ", value: " + value,
				Level.INFO, null);
		try {
			final String queryString = "select model from Obliquemodel model where model." + propertyName
					+ "= :propertyValue";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<Obliquemodel> findByName(Object name, int... rowStartIdxAndCount) {
		return findByProperty(NAME, name, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByCentrallng(Object centrallng, int... rowStartIdxAndCount) {
		return findByProperty(CENTRALLNG, centrallng, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByCentrallat(Object centrallat, int... rowStartIdxAndCount) {
		return findByProperty(CENTRALLAT, centrallat, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByCentralheight(Object centralheight, int... rowStartIdxAndCount) {
		return findByProperty(CENTRALHEIGHT, centralheight, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByDataformat(Object dataformat, int... rowStartIdxAndCount) {
		return findByProperty(DATAFORMAT, dataformat, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByAltitudemode(Object altitudemode, int... rowStartIdxAndCount) {
		return findByProperty(ALTITUDEMODE, altitudemode, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByHeading(Object heading, int... rowStartIdxAndCount) {
		return findByProperty(HEADING, heading, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByTilt(Object tilt, int... rowStartIdxAndCount) {
		return findByProperty(TILT, tilt, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByRoll(Object roll, int... rowStartIdxAndCount) {
		return findByProperty(ROLL, roll, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByXscale(Object xscale, int... rowStartIdxAndCount) {
		return findByProperty(XSCALE, xscale, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByYscale(Object yscale, int... rowStartIdxAndCount) {
		return findByProperty(YSCALE, yscale, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByZscale(Object zscale, int... rowStartIdxAndCount) {
		return findByProperty(ZSCALE, zscale, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByTiletype(Object tiletype, int... rowStartIdxAndCount) {
		return findByProperty(TILETYPE, tiletype, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByTilemin(Object tilemin, int... rowStartIdxAndCount) {
		return findByProperty(TILEMIN, tilemin, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByTilemax(Object tilemax, int... rowStartIdxAndCount) {
		return findByProperty(TILEMAX, tilemax, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByTilequantity(Object tilequantity, int... rowStartIdxAndCount) {
		return findByProperty(TILEQUANTITY, tilequantity, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount) {
		return findByProperty(RESOURCEPATH, resourcepath, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByDescription(Object description, int... rowStartIdxAndCount) {
		return findByProperty(DESCRIPTION, description, rowStartIdxAndCount);
	}

	public List<Obliquemodel> findByIsload(Object isload, int... rowStartIdxAndCount) {
		return findByProperty(ISLOAD, isload, rowStartIdxAndCount);
	}

	/**
	 * Find all Obliquemodel entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Obliquemodel> all Obliquemodel entities
	 */
	@SuppressWarnings("unchecked")
	public List<Obliquemodel> findAll(final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding all Obliquemodel instances", Level.INFO, null);
		try {
			final String queryString = "select model from Obliquemodel model";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

    /**
     * 模糊查询倾斜摄影实体 属性
     * @param propertyName
     * @param value
     * @param rowStartIdxAndCount
     * @return
     */
    @Override
    public List<Obliquemodel> findByPropertylike(String propertyName, Object value, int... rowStartIdxAndCount) {

            EntityManagerHelper.log("finding Obliquemodel instance with property: " + propertyName + ", value: " + value,
                Level.INFO, null);
            try {
                final String queryString = "select model from Obliquemodel model where model." + propertyName
                    + " like  :propertyValue";
                Query query = getEntityManager().createQuery(queryString);
                query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
                query.setParameter("propertyValue", "%" + value + "%");
                if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                    int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                    if (rowStartIdx > 0) {
                        query.setFirstResult(rowStartIdx);
                    }

                    if (rowStartIdxAndCount.length > 1) {
                        int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                        if (rowCount > 0) {
                            query.setMaxResults(rowCount);
                        }
                    }
                }
                return query.getResultList();
            } catch (RuntimeException re) {
                EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
                throw re;
            }
    }

    /**
     * 模糊查询倾斜摄影实体 属性 的行数
     * @param propertyName
     * @param value
     * @return
     */
    @Override
    public Long findCountByPropertylike(String propertyName, Object value) {
        EntityManagerHelper.log("finding Obliquemodel count with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Obliquemodel model where model." + propertyName
                + " like  :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", "%" + value + "%");

            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 查询总行数
     * @return
     */
    @Override
    public Long count() {
        EntityManagerHelper.log("finding Obliquemodel count",Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Obliquemodel model";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 按名称关键字 查询 倾斜摄影模型
     * @param keyWord
     * @param rowStartIdxAndCount
     * @return
     */
    @Override
    public List<Obliquemodel> findObliquemodelByNameKeyWord(String keyWord, int... rowStartIdxAndCount) {
        return findByPropertylike(NAME,keyWord,rowStartIdxAndCount);
    }

    /**
     * 按名称关键字查询倾斜摄影模型行数
     * @param keyWord
     * @return
     */
    @Override
    public Long findObliquemodelCountByNameKeyWord(String keyWord) {
        return findCountByPropertylike(NAME,keyWord);
    }

}
