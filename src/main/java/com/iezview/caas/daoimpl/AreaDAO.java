package com.iezview.caas.daoimpl;

import com.iezview.caas.dao.IAreaDAO;
import com.iezview.caas.entity.Area;

import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Level;

/**
 * A data access object (DAO) providing persistence and search support for Area
 * entities. Transaction control of the save(), update() and delete() operations
 * must be handled externally by senders of these methods or must be manually
 * added to each of these methods for data to be persisted to the JPA datastore.
 *
 * @see Area
 * @author MyEclipse Persistence Tools
 */
public class AreaDAO implements IAreaDAO {
	// property constants
	public static final String ANAME = "aname";
	public static final String PARENT = "parent";
	public static final String ALEVEL = "alevel";
	public static final String NODEKIND = "nodekind";
	public static final String REMARK = "remark";

	private EntityManager getEntityManager() {
		return EntityManagerHelper.getEntityManager();
	}

	/**
	 * Perform an initial save of a previously unsaved Area entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * AreaDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Area entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Area entity) {
		EntityManagerHelper.log("saving Area instance", Level.INFO, null);
		try {
		    getEntityManager().getTransaction().begin();
			getEntityManager().persist(entity);
			getEntityManager().getTransaction().commit();
			EntityManagerHelper.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    getEntityManager().getTransaction().rollback();
			EntityManagerHelper.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Area entity. This operation must be performed within
	 * the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * AreaDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Area entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Area entity) {
		EntityManagerHelper.log("deleting Area instance", Level.INFO, null);
		try {
            getEntityManager().getTransaction().begin();
			entity = getEntityManager().getReference(Area.class, entity.getId());
			getEntityManager().remove(entity);
            getEntityManager().getTransaction().commit();
			EntityManagerHelper.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    getEntityManager().getTransaction().rollback();
			EntityManagerHelper.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Area entity and return it or a copy of it to
	 * the sender. A copy of the Area entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = AreaDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Area entity to update
	 * @return Area the persisted Area entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Area update(Area entity) {
		EntityManagerHelper.log("updating Area instance", Level.INFO, null);
		try {
		    getEntityManager().getTransaction().begin();
			Area result = getEntityManager().merge(entity);
			getEntityManager().getTransaction().commit();
			EntityManagerHelper.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
		    getEntityManager().getTransaction().rollback();
			EntityManagerHelper.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Area findById(Integer id) {
		EntityManagerHelper.log("finding Area instance with id: " + id, Level.INFO, null);
		try {
		    getEntityManager().clear();//禁用hibernate 缓存  xml配置不起作用
//            Map<String,Object>  map = new HashMap<>();
//            map.put("org.hibernate.cacheable","false");
			Area instance = getEntityManager().find(Area.class, id);
			return instance;
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Area entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Area property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<Area> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<Area> findByProperty(String propertyName, final Object value, final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding Area instance with property: " + propertyName + ", value: " + value,
				Level.INFO, null);
		try {
			final String queryString = "select model from Area model where model." + propertyName + "= :propertyValue";
			Query query = getEntityManager().createQuery(queryString);
//			query.setHint("org.hibernate.cacheable","false");
			query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);

			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

    /**
     * Find all Area entities with a specific property value.
     *
     * @param propertyName
     *            the name of the Area property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<Area> found by query
     */
    @SuppressWarnings("unchecked")
    public List<Area> findByPropertylike(String propertyName, final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding Area instance with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select model from Area model where model." + propertyName + " like :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", "%"+value+"%");
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }
	public List<Area> findByAname(Object aname, int... rowStartIdxAndCount) {
		return findByProperty(ANAME, aname, rowStartIdxAndCount);
	}

	public List<Area> findByParent(Object parent, int... rowStartIdxAndCount) {
		return findByProperty(PARENT, parent, rowStartIdxAndCount);
	}

	public List<Area> findByAlevel(Object alevel, int... rowStartIdxAndCount) {
		return findByProperty(ALEVEL, alevel, rowStartIdxAndCount);
	}

	public List<Area> findByNodekind(Object nodekind, int... rowStartIdxAndCount) {
		return findByProperty(NODEKIND, nodekind, rowStartIdxAndCount);
	}

	public List<Area> findByRemark(Object remark, int... rowStartIdxAndCount) {
		return findByProperty(REMARK, remark, rowStartIdxAndCount);
	}

	/**
	 * Find all Area entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Area> all Area entities
	 */
	@SuppressWarnings("unchecked")
	public List<Area> findAll(final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding all Area instances", Level.INFO, null);
		try {
			final String queryString = "select model from Area model";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);

            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

    @Override
    public Long count() {
        EntityManagerHelper.log("finding count Area instances", Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Area model";

            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            return (Long) query.getSingleResult();
        }catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }

    }

    /**
     * 根据名称关键字查询 行政区域
     * @param keyWord
     * @param rowStartIdxAndCount
     * @return
     */
    @Override
    public List<Area> findAreaByNameKeyWord(String keyWord, int... rowStartIdxAndCount) {
        return  findByPropertylike(ANAME,keyWord,rowStartIdxAndCount);
    }

    /**
     * 根据名称关键字 模糊查询 行政区域 行数
     * @param keyWord
     * @return
     */
    @Override
    public Long findAreaCountByNameKeyWord(String keyWord) {
        return findAreaCountByPropertyLike(ANAME,keyWord);
    }


    /**
     * 根据属性查询符合范围的行政区域总数 用于分页
     *
     * @param propertyName
     * @param value
     * @return
     */
    public Long findAreaCountByPropertyLike(String propertyName, Object value) {
        EntityManagerHelper.log("finding Area count with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Area model where model." + propertyName
                + " like  :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", "%" + value + "%");

            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }


}
