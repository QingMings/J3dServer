package com.iezview.caas.daoimpl;

import com.iezview.caas.dao.IBuildingDAO;
import com.iezview.caas.entity.Area;
import com.iezview.caas.entity.Building;
import com.iezview.caas.entity.Subjects;
import com.iezview.caas.entity.Unit;

import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Level;

/**
 * A data access object (DAO) providing persistence and search support for
 * Building entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @author MyEclipse Persistence Tools
 * @see Building
 */
public class BuildingDAO implements IBuildingDAO {
    // property constants
    public static final String NUMBER = "number";
    public static final String BNAME = "bname";
    public static final String RESOURCEPATH = "resourcepath";
    public static final String BTYPE = "btype";
    public static final String YEARS = "years";
    public static final String HEIGHT = "height";
    public static final String PURPOSE = "purpose";
    public static final String TOTALAREA = "totalarea";
    public static final String ABOVEAREA = "abovearea";
    public static final String UNDERAREA = "underarea";
    public static final String CONSTRUCTIONUNITS = "constructionunits";
    public static final String CENTRALLNG = "centrallng";
    public static final String CENTRALLAT = "centrallat";
    public static final String SYNOPSIS = "synopsis";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved Building entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link EntityManager#persist(Object)
     * EntityManager#persist} operation.
     * <p>
     * <pre>
     *
     * EntityManagerHelper.beginTransaction();
     * BuildingDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity Building entity to persist
     * @throws RuntimeException when the operation fails
     */
    public void save(Building entity) {
        EntityManagerHelper.log("saving Building instance", Level.INFO, null);
        try {
            getEntityManager().getTransaction().begin();
            getEntityManager().persist(entity);
            getEntityManager().getTransaction().commit();
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            getEntityManager().getTransaction().rollback();
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent Building entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the {@link EntityManager#remove(Object)
     * EntityManager#delete} operation.
     * <p>
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BuildingDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity Building entity to delete
     * @throws RuntimeException when the operation fails
     */
    public void delete(Building entity) {
        EntityManagerHelper.log("deleting Building instance", Level.INFO, null);
        try {
            getEntityManager().getTransaction().begin();
            entity = getEntityManager().getReference(Building.class, entity.getId());
            getEntityManager().remove(entity);
            getEntityManager().getTransaction().commit();
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            getEntityManager().getTransaction().rollback();
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved Building entity and return it or a copy of it
     * to the sender. A copy of the Building entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link EntityManager#merge(Object) EntityManager#merge}
     * operation.
     * <p>
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = BuildingDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity Building entity to update
     * @return Building the persisted Building entity instance, may not be the
     * same
     * @throws RuntimeException if the operation fails
     */
    public Building update(Building entity) {
        EntityManagerHelper.log("updating Building instance", Level.INFO, null);
        try {
            getEntityManager().getTransaction().begin();
            Building result = getEntityManager().merge(entity);
            getEntityManager().getTransaction().commit();
            EntityManagerHelper.log("update successful", Level.INFO, null);
            return result;
        } catch (RuntimeException re) {
            getEntityManager().getTransaction().commit();
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public Building findById(Integer id) {
        EntityManagerHelper.log("finding Building instance with id: " + id, Level.INFO, null);
        try {
            getEntityManager().clear();//禁用hibernate 缓存  xml配置不起作用
            Building instance = getEntityManager().find(Building.class, id);
            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all Building entities with a specific property value.
     *
     * @param propertyName        the name of the Building property to query
     * @param value               the property value to match
     * @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *                            row index in the query result-set to begin collecting the
     *                            results. rowStartIdxAndCount[1] specifies the the maximum
     *                            number of results to return.
     * @return List<Building> found by query
     */
    @SuppressWarnings("unchecked")
    public List<Building> findByProperty(String propertyName, final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding Building instance with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select model from Building model where model." + propertyName
                + "= :propertyValue order by model.number ,model.unit.id";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", value);
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 模糊查询
     *
     * @param propertyName
     * @param value
     * @param rowStartIdxAndCount
     * @return
     * @antuor qingmings
     */
    public List<Building> findByPropertylike(String propertyName, final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding Building instance with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select model from Building model where model." + propertyName
                + " like  :propertyValue order by model.number ,model.unit.id";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", "%" + value + "%");
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<Building> findByNumber(Object number, int... rowStartIdxAndCount) {
        return findByProperty(NUMBER, number, rowStartIdxAndCount);
    }

    public List<Building> findByBname(Object bname, int... rowStartIdxAndCount) {
        return findByProperty(BNAME, bname, rowStartIdxAndCount);
    }

    public List<Building> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount) {
        return findByProperty(RESOURCEPATH, resourcepath, rowStartIdxAndCount);
    }

    public List<Building> findByBtype(Object btype, int... rowStartIdxAndCount) {
        return findByProperty(BTYPE, btype, rowStartIdxAndCount);
    }

    public List<Building> findByYears(Object years, int... rowStartIdxAndCount) {
        return findByProperty(YEARS, years, rowStartIdxAndCount);
    }

    public List<Building> findByHeight(Object height, int... rowStartIdxAndCount) {
        return findByProperty(HEIGHT, height, rowStartIdxAndCount);
    }

    public List<Building> findByPurpose(Object purpose, int... rowStartIdxAndCount) {
        return findByProperty(PURPOSE, purpose, rowStartIdxAndCount);
    }

    public List<Building> findByTotalarea(Object totalarea, int... rowStartIdxAndCount) {
        return findByProperty(TOTALAREA, totalarea, rowStartIdxAndCount);
    }

    public List<Building> findByAbovearea(Object abovearea, int... rowStartIdxAndCount) {
        return findByProperty(ABOVEAREA, abovearea, rowStartIdxAndCount);
    }

    public List<Building> findByUnderarea(Object underarea, int... rowStartIdxAndCount) {
        return findByProperty(UNDERAREA, underarea, rowStartIdxAndCount);
    }

    public List<Building> findByConstructionunits(Object constructionunits, int... rowStartIdxAndCount) {
        return findByProperty(CONSTRUCTIONUNITS, constructionunits, rowStartIdxAndCount);
    }

    public List<Building> findByCentrallng(Object centrallng, int... rowStartIdxAndCount) {
        return findByProperty(CENTRALLNG, centrallng, rowStartIdxAndCount);
    }

    public List<Building> findByCentrallat(Object centrallat, int... rowStartIdxAndCount) {
        return findByProperty(CENTRALLAT, centrallat, rowStartIdxAndCount);
    }

    public List<Building> findBySynopsis(Object synopsis, int... rowStartIdxAndCount) {
        return findByProperty(SYNOPSIS, synopsis, rowStartIdxAndCount);
    }

    /**
     * Find all Building entities.
     *
     * @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *                            row index in the query result-set to begin collecting the
     *                            results. rowStartIdxAndCount[1] specifies the the maximum
     *                            count of results to return.
     * @return List<Building> all Building entities
     */
    @SuppressWarnings("unchecked")
    public List<Building> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all Building instances", Level.INFO, null);
        try {
            final String queryString = "select model from Building model  order by model.number ,model.unit.id";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * @param keywork             名称关键字
     * @param rowStartIdxAndCount index 0  从第几条开始查，  index 1 查多少条
     * @return
     * @desc 根据名称关键字模糊查询 建筑信息
     * @author qingmings
     */
    @Override
    public List<Building> findBuildingByNameKeyWord(String keywork, int... rowStartIdxAndCount) {
        return findByPropertylike(BNAME, keywork, rowStartIdxAndCount);
    }

    /**
     * 根据属性查询符合范围的建筑总数 用于分页
     *
     * @param propertyName
     * @param value
     * @return
     */
    @Override
    public Long findBuildingCountByPropertyLike(String propertyName, Object value) {
        EntityManagerHelper.log("finding Building count with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Building model where model." + propertyName
                + " like  :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", "%" + value + "%");

            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 根据名称关键字查询建筑总数
     *
     * @param keyword
     * @return
     */
    @Override
    public Long findBuildingCountByNameKeyWord(String keyword) {
        return findBuildingCountByPropertyLike(BNAME, keyword);
    }

    /**
     * 查询单位总数
     *
     * @return
     */
    @Override
    public Long count() {
        EntityManagerHelper.log("finding count Building instances", Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Building model";
            Query query = getEntityManager().createQuery(queryString);
            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }

    }

    /**
     * 根据单位查询建筑
     *
     * @param unit 单位
     * @return
     */
    @Override
    public List<Building> findBuildingByUnit(Unit unit, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding  Building instances with Unid.id :" + unit.getId(), Level.INFO, null);

        try {
            final String queryString = "select model from Building model where model.unit.id = :unitId  order by model.number ,model.unit.id";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("unitId", unit.getId());
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 根据单位id查询地址建筑总行数
     *
     * @param unit
     * @return
     */
    @Override
    public Long findBuildingCountByUnit(Unit unit) {
        EntityManagerHelper.log("finding  Buildings count with Unid.id :" + unit.getId(), Level.INFO, null);

        try {
            final String queryString = "select count(model.id) from Building model where model.unit.id =:unitId";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("unitId", unit.getId());
            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find Buildings count  failed", Level.SEVERE, re);
            throw re;
        }

    }

    /**
     * 按院所名称关键字查询下属建筑
     * @param keyWord 院所名称关键字
     * @param rowStartIdxAndCount index 0  从第几条开始查，  index 1 查多少条
     * @return
     */
    @Override
    public List<Building> findBuildingByUnitNameKeyWord(String keyWord, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding  Building instances with Unid.unamme like  :" + keyWord, Level.INFO, null);
        try {
            String queryString = "select model from Building model where model.unit.id in (select model1.id from Unit model1 where model1.uname like :unitNameKeyWord)  order by model.number ,model.unit.id";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("unitNameKeyWord", "%" + keyWord + "%");
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        }catch (RuntimeException re) {
            EntityManagerHelper.log("find Buildings   failed", Level.SEVERE, re);
            throw re;
        }
    }
    /**
     * 按院所名称关键字查询下属建筑总行数
     * @param keyWord 院所名称关键字
     * @return
     */
    @Override
    public Long findBuildingCountByUnitNameKeyWord(String keyWord) {
        EntityManagerHelper.log("finding  Building count with Unid.unamme like  :" + keyWord, Level.INFO, null);
        try {
            String queryString = "select count(model.id) from Building model where model.unit.id in (select model1.id from Unit model1 where model1.uname like :unitNameKeyWord)";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("unitNameKeyWord", "%" + keyWord + "%");

            return (Long) query.getSingleResult();
        }catch (RuntimeException re) {
            EntityManagerHelper.log("find Buildings   failed", Level.SEVERE, re);
            throw re;
        }

    }

    @Override
    public void saveAll(List<Building> buildings) {
        EntityManagerHelper.log("saving Building instance", Level.INFO, null);
        try {
            getEntityManager().getTransaction().begin();
            buildings.forEach(building ->getEntityManager().persist(building));
//            getEntityManager().persist(entity);
            getEntityManager().getTransaction().commit();
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            getEntityManager().getTransaction().rollback();
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public List<Building> findBuildingByArea(Area area, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding  Building instances with Area.ID  =  " + area.getId(), Level.INFO, null);
        try {
            String queryString = "select model from Building model where model.unit.id in (select model1.id from Unit model1 where model1.area.id = :areaId)  order by model.number ,model.unit.id";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("areaId",area.getId() );
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        }catch (RuntimeException re) {
            EntityManagerHelper.log("find Buildings   failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public Long findBuildingCountByArea(Area area) {
        EntityManagerHelper.log("finding  Building count with Area.id  :" + area.getId(), Level.INFO, null);
        try {
            String queryString = "select count(model.id) from Building model where model.unit.id in (select model1.id from Unit model1 where model1.area = :areaId)";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("areaId", area.getId());

            return (Long) query.getSingleResult();
        }catch (RuntimeException re) {
            EntityManagerHelper.log("find Buildings   failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public List<Building> findBuildingBySubject(Subjects subjects, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding  Building instances with Area.ID  =  " + subjects.getId(), Level.INFO, null);
        try {
            String queryString = "select model from Building model where model.unit.id in (select model1.id from Unit model1 join model1.subjects  subject where subject.id = :subjectId)  order by model.number ,model.unit.id";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("subjectId",subjects.getId() );
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        }catch (RuntimeException re) {
            EntityManagerHelper.log("find Buildings   failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public Long findBuildingCountBySubject(Subjects subjects) {
        EntityManagerHelper.log("finding  Building count with subjects.id  :" + subjects.getId(), Level.INFO, null);
        try {
            String queryString = "select count(model.id) from Building model where model.unit.id  in (select model1.id from Unit model1 join model1.subjects  subject where subject.id = :subjectId) ";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("subjectId", subjects.getId());

            return (Long) query.getSingleResult();
        }catch (RuntimeException re) {
            EntityManagerHelper.log("find Buildings   failed", Level.SEVERE, re);
            throw re;
        }
    }


}
