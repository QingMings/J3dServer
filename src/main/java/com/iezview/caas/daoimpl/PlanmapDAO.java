package com.iezview.caas.daoimpl;

import java.util.List;
import java.util.logging.Level;
import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.iezview.caas.dao.IPlanmapDAO;
import com.iezview.caas.entity.Planmap;

/**
 * A data access object (DAO) providing persistence and search support for
 * Planmap entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see Planmap
 * @author MyEclipse Persistence Tools
 */
public class PlanmapDAO implements IPlanmapDAO {
	// property constants
	public static final String PNAME = "pname";
	public static final String RESOURCEPATH = "resourcepath";
	public static final String REMARK = "remark";

	private EntityManager getEntityManager() {
		return EntityManagerHelper.getEntityManager();
	}

	/**
	 * Perform an initial save of a previously unsaved Planmap entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * PlanmapDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Planmap entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Planmap entity) {
		EntityManagerHelper.log("saving Planmap instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			getEntityManager().persist(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Planmap entity. This operation must be performed
	 * within the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * PlanmapDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Planmap entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Planmap entity) {
		EntityManagerHelper.log("deleting Planmap instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			entity = getEntityManager().getReference(Planmap.class, entity.getId());
			getEntityManager().remove(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Planmap entity and return it or a copy of it
	 * to the sender. A copy of the Planmap entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = PlanmapDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Planmap entity to update
	 * @return Planmap the persisted Planmap entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Planmap update(Planmap entity) {
		EntityManagerHelper.log("updating Planmap instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			Planmap result = getEntityManager().merge(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Planmap findById(Integer id) {
		EntityManagerHelper.log("finding Planmap instance with id: " + id, Level.INFO, null);
		try {
            getEntityManager().clear();//禁用hibernate 缓存  xml配置不起作用
			Planmap instance = getEntityManager().find(Planmap.class, id);
			return instance;
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Planmap entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Planmap property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<Planmap> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<Planmap> findByProperty(String propertyName, final Object value, final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding Planmap instance with property: " + propertyName + ", value: " + value,
				Level.INFO, null);
		try {
			final String queryString = "select model from Planmap model where model." + propertyName
					+ "= :propertyValue";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<Planmap> findByPname(Object pname, int... rowStartIdxAndCount) {
		return findByProperty(PNAME, pname, rowStartIdxAndCount);
	}

	public List<Planmap> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount) {
		return findByProperty(RESOURCEPATH, resourcepath, rowStartIdxAndCount);
	}

	public List<Planmap> findByRemark(Object remark, int... rowStartIdxAndCount) {
		return findByProperty(REMARK, remark, rowStartIdxAndCount);
	}

	/**
	 * Find all Planmap entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Planmap> all Planmap entities
	 */
	@SuppressWarnings("unchecked")
	public List<Planmap> findAll(final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding all Planmap instances", Level.INFO, null);
		try {
			final String queryString = "select model from Planmap model";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

    /**
     * 查询总行数
     * @return
     */
    @Override
    public Long count() {
        EntityManagerHelper.log("finding Planmap count",Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Planmap model";
            Query query = getEntityManager().createQuery(queryString);
            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 测试名称关键字查询 规划图
     * @param keyWord
     * @param rowStartIdxAndCount
     * @return
     */
    @Override
    public List<Planmap> findPlanmapByNameKeyWord(String keyWord, int... rowStartIdxAndCount) {
        return findByPropertylike(PNAME,keyWord,rowStartIdxAndCount);
    }

    /**
     * 名称关键字查询规划图总行数
     * @param keyWord
     * @return
     */
    @Override
    public Long findPlanmapCountByNameKeyWord(String keyWord) {
        return findCountByProperty(PNAME,keyWord);
    }

    /**
     * 按照规划图属性模糊查询
     * @param propertyName
     * @param value
     * @param rowStartIdxAndCount
     * @return
     */
    @Override
    public List<Planmap> findByPropertylike(String propertyName, Object value, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding Planmap instance with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select model from Planmap model where model." + propertyName
                + " like :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue","%"+ value+"%");
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 按照规划图属性模糊查询 行数
     * @param propertyName
     * @param value
     * @return
     */
    @Override
    public Long findCountByProperty(String propertyName, Object value) {
        EntityManagerHelper.log("finding Planmap count with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Planmap model where model." + propertyName
                + " like  :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", "%" + value + "%");

            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

}
