//package com.iezview.caas.daoimpl;
//
//import java.util.List;
//import java.util.logging.Level;
//import javax.persistence.EntityManager;
//import javax.persistence.Query;
//
//import com.iezview.caas.dao.IUnitobliquerelDAO;
//import com.iezview.caas.entity.Unitobliquerel;
//
///**
// * A data access object (DAO) providing persistence and search support for
// * Unitobliquerel entities. Transaction control of the save(), update() and
// * delete() operations must be handled externally by senders of these methods or
// * must be manually added to each of these methods for data to be persisted to
// * the JPA datastore.
// *
// * @see Unitobliquerel
// * @author MyEclipse Persistence Tools
// */
//public class UnitobliquerelDAO implements IUnitobliquerelDAO {
//	// property constants
//	public static final String REMARK = "remark";
//
//	private EntityManager getEntityManager() {
//		return EntityManagerHelper.getEntityManager();
//	}
//
//	/**
//	 * Perform an initial save of a previously unsaved Unitobliquerel entity.
//	 * All subsequent persist actions of this entity should use the #update()
//	 * method. This operation must be performed within the a database
//	 * transaction context for the entity's data to be permanently saved to the
//	 * persistence store, i.e., database. This method uses the
//	 * {@link EntityManager#persist(Object)
//	 * EntityManager#persist} operation.
//	 *
//	 * <pre>
//	 *
//	 * EntityManagerHelper.beginTransaction();
//	 * UnitobliquerelDAO.save(entity);
//	 * EntityManagerHelper.commit();
//	 * </pre>
//	 *
//	 * @param entity
//	 *            Unitobliquerel entity to persist
//	 * @throws RuntimeException
//	 *             when the operation fails
//	 */
//	public void save(Unitobliquerel entity) {
//		EntityManagerHelper.log("saving Unitobliquerel instance", Level.INFO, null);
//		try {
//		    EntityManagerHelper.beginTransaction();
//			getEntityManager().persist(entity);
//			EntityManagerHelper.commit();
//			EntityManagerHelper.log("save successful", Level.INFO, null);
//		} catch (RuntimeException re) {
//		    EntityManagerHelper.rollback();
//			EntityManagerHelper.log("save failed", Level.SEVERE, re);
//			throw re;
//		}
//	}
//
//	/**
//	 * Delete a persistent Unitobliquerel entity. This operation must be
//	 * performed within the a database transaction context for the entity's data
//	 * to be permanently deleted from the persistence store, i.e., database.
//	 * This method uses the
//	 * {@link EntityManager#remove(Object)
//	 * EntityManager#delete} operation.
//	 *
//	 * <pre>
//	 * EntityManagerHelper.beginTransaction();
//	 * UnitobliquerelDAO.delete(entity);
//	 * EntityManagerHelper.commit();
//	 * entity = null;
//	 * </pre>
//	 *
//	 * @param entity
//	 *            Unitobliquerel entity to delete
//	 * @throws RuntimeException
//	 *             when the operation fails
//	 */
//	public void delete(Unitobliquerel entity) {
//		EntityManagerHelper.log("deleting Unitobliquerel instance", Level.INFO, null);
//		try {
//		    EntityManagerHelper.beginTransaction();
//			entity = getEntityManager().getReference(Unitobliquerel.class, entity.getId());
//			getEntityManager().remove(entity);
//			EntityManagerHelper.commit();
//			EntityManagerHelper.log("delete successful", Level.INFO, null);
//		} catch (RuntimeException re) {
//		    EntityManagerHelper.rollback();
//			EntityManagerHelper.log("delete failed", Level.SEVERE, re);
//			throw re;
//		}
//	}
//
//	/**
//	 * Persist a previously saved Unitobliquerel entity and return it or a copy
//	 * of it to the sender. A copy of the Unitobliquerel entity parameter is
//	 * returned when the JPA persistence mechanism has not previously been
//	 * tracking the updated entity. This operation must be performed within the
//	 * a database transaction context for the entity's data to be permanently
//	 * saved to the persistence store, i.e., database. This method uses the
//	 * {@link EntityManager#merge(Object) EntityManager#merge}
//	 * operation.
//	 *
//	 * <pre>
//	 * EntityManagerHelper.beginTransaction();
//	 * entity = UnitobliquerelDAO.update(entity);
//	 * EntityManagerHelper.commit();
//	 * </pre>
//	 *
//	 * @param entity
//	 *            Unitobliquerel entity to update
//	 * @return Unitobliquerel the persisted Unitobliquerel entity instance, may
//	 *         not be the same
//	 * @throws RuntimeException
//	 *             if the operation fails
//	 */
//	public Unitobliquerel update(Unitobliquerel entity) {
//		EntityManagerHelper.log("updating Unitobliquerel instance", Level.INFO, null);
//		try {
//		    EntityManagerHelper.beginTransaction();
//			Unitobliquerel result = getEntityManager().merge(entity);
//			EntityManagerHelper.commit();
//			EntityManagerHelper.log("update successful", Level.INFO, null);
//			return result;
//		} catch (RuntimeException re) {
//		    EntityManagerHelper.rollback();
//			EntityManagerHelper.log("update failed", Level.SEVERE, re);
//			throw re;
//		}
//	}
//
//	public Unitobliquerel findById(Integer id) {
//		EntityManagerHelper.log("finding Unitobliquerel instance with id: " + id, Level.INFO, null);
//		try {
//			Unitobliquerel instance = getEntityManager().find(Unitobliquerel.class, id);
//			return instance;
//		} catch (RuntimeException re) {
//			EntityManagerHelper.log("find failed", Level.SEVERE, re);
//			throw re;
//		}
//	}
//
//	/**
//	 * Find all Unitobliquerel entities with a specific property value.
//	 *
//	 * @param propertyName
//	 *            the name of the Unitobliquerel property to query
//	 * @param value
//	 *            the property value to match
//	 * @param rowStartIdxAndCount
//	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
//	 *            row index in the query result-set to begin collecting the
//	 *            results. rowStartIdxAndCount[1] specifies the the maximum
//	 *            number of results to return.
//	 * @return List<Unitobliquerel> found by query
//	 */
//	@SuppressWarnings("unchecked")
//	public List<Unitobliquerel> findByProperty(String propertyName, final Object value,
//			final int... rowStartIdxAndCount) {
//		EntityManagerHelper.log("finding Unitobliquerel instance with property: " + propertyName + ", value: " + value,
//				Level.INFO, null);
//		try {
//			final String queryString = "select model from Unitobliquerel model where model." + propertyName
//					+ "= :propertyValue";
//			Query query = getEntityManager().createQuery(queryString);
//			query.setParameter("propertyValue", value);
//			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
//				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
//				if (rowStartIdx > 0) {
//					query.setFirstResult(rowStartIdx);
//				}
//
//				if (rowStartIdxAndCount.length > 1) {
//					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
//					if (rowCount > 0) {
//						query.setMaxResults(rowCount);
//					}
//				}
//			}
//			return query.getResultList();
//		} catch (RuntimeException re) {
//			EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
//			throw re;
//		}
//	}
//
//	public List<Unitobliquerel> findByRemark(Object remark, int... rowStartIdxAndCount) {
//		return findByProperty(REMARK, remark, rowStartIdxAndCount);
//	}
//
//	/**
//	 * Find all Unitobliquerel entities.
//	 *
//	 * @param rowStartIdxAndCount
//	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
//	 *            row index in the query result-set to begin collecting the
//	 *            results. rowStartIdxAndCount[1] specifies the the maximum
//	 *            count of results to return.
//	 * @return List<Unitobliquerel> all Unitobliquerel entities
//	 */
//	@SuppressWarnings("unchecked")
//	public List<Unitobliquerel> findAll(final int... rowStartIdxAndCount) {
//		EntityManagerHelper.log("finding all Unitobliquerel instances", Level.INFO, null);
//		try {
//			final String queryString = "select model from Unitobliquerel model";
//			Query query = getEntityManager().createQuery(queryString);
//			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
//				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
//				if (rowStartIdx > 0) {
//					query.setFirstResult(rowStartIdx);
//				}
//
//				if (rowStartIdxAndCount.length > 1) {
//					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
//					if (rowCount > 0) {
//						query.setMaxResults(rowCount);
//					}
//				}
//			}
//			return query.getResultList();
//		} catch (RuntimeException re) {
//			EntityManagerHelper.log("find all failed", Level.SEVERE, re);
//			throw re;
//		}
//	}
//
//}
