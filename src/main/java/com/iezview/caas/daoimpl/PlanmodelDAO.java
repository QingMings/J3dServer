package com.iezview.caas.daoimpl;

import java.sql.Time;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.iezview.caas.dao.IPlanmodelDAO;
import com.iezview.caas.entity.Planmodel;
import com.iezview.caas.entity.Unit;

/**
 * A data access object (DAO) providing persistence and search support for
 * Planmodel entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see Planmodel
 * @author MyEclipse Persistence Tools
 */
public class PlanmodelDAO implements IPlanmodelDAO {
	// property constants
	public static final String MNAME = "mname";
	public static final String CENTRALLNG = "centrallng";
	public static final String CENTRALLAT = "centrallat";
	public static final String CENTRALHEIGHT = "centralheight";
	public static final String DATAFORMAT = "dataformat";
	public static final String ALTITUDEMODE = "altitudemode";
	public static final String HEADING = "heading";
	public static final String TILT = "tilt";
	public static final String ROLL = "roll";
	public static final String XSCALE = "xscale";
	public static final String YSCALE = "yscale";
	public static final String ZSCALE = "zscale";
	public static final String TILETYPE = "tiletype";
	public static final String RESOURCEPATH = "resourcepath";
	public static final String DESCRIPTION = "description";
	public static final String ISLOAD = "isload";

	private EntityManager getEntityManager() {
		return EntityManagerHelper.getEntityManager();
	}

	/**
	 * Perform an initial save of a previously unsaved Planmodel entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * PlanmodelDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Planmodel entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Planmodel entity) {
		EntityManagerHelper.log("saving Planmodel instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			getEntityManager().persist(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Planmodel entity. This operation must be performed
	 * within the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * PlanmodelDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Planmodel entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Planmodel entity) {
		EntityManagerHelper.log("deleting Planmodel instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			entity = getEntityManager().getReference(Planmodel.class, entity.getId());
			getEntityManager().remove(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Planmodel entity and return it or a copy of it
	 * to the sender. A copy of the Planmodel entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = PlanmodelDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Planmodel entity to update
	 * @return Planmodel the persisted Planmodel entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Planmodel update(Planmodel entity) {
		EntityManagerHelper.log("updating Planmodel instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			Planmodel result = getEntityManager().merge(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Planmodel findById(Integer id) {
		EntityManagerHelper.log("finding Planmodel instance with id: " + id, Level.INFO, null);
		try {
            getEntityManager().clear();//禁用hibernate 缓存  xml配置不起作用
			Planmodel instance = getEntityManager().find(Planmodel.class, id);
			return instance;
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Planmodel entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Planmodel property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<Planmodel> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<Planmodel> findByProperty(String propertyName, final Object value, final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding Planmodel instance with property: " + propertyName + ", value: " + value,
				Level.INFO, null);
		try {
			final String queryString = "select model from Planmodel model where model." + propertyName
					+ "= :propertyValue";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<Planmodel> findByMname(Object mname, int... rowStartIdxAndCount) {
		return findByProperty(MNAME, mname, rowStartIdxAndCount);
	}

	public List<Planmodel> findByCentrallng(Object centrallng, int... rowStartIdxAndCount) {
		return findByProperty(CENTRALLNG, centrallng, rowStartIdxAndCount);
	}

	public List<Planmodel> findByCentrallat(Object centrallat, int... rowStartIdxAndCount) {
		return findByProperty(CENTRALLAT, centrallat, rowStartIdxAndCount);
	}

	public List<Planmodel> findByCentralheight(Object centralheight, int... rowStartIdxAndCount) {
		return findByProperty(CENTRALHEIGHT, centralheight, rowStartIdxAndCount);
	}

	public List<Planmodel> findByDataformat(Object dataformat, int... rowStartIdxAndCount) {
		return findByProperty(DATAFORMAT, dataformat, rowStartIdxAndCount);
	}

	public List<Planmodel> findByAltitudemode(Object altitudemode, int... rowStartIdxAndCount) {
		return findByProperty(ALTITUDEMODE, altitudemode, rowStartIdxAndCount);
	}

	public List<Planmodel> findByHeading(Object heading, int... rowStartIdxAndCount) {
		return findByProperty(HEADING, heading, rowStartIdxAndCount);
	}

	public List<Planmodel> findByTilt(Object tilt, int... rowStartIdxAndCount) {
		return findByProperty(TILT, tilt, rowStartIdxAndCount);
	}

	public List<Planmodel> findByRoll(Object roll, int... rowStartIdxAndCount) {
		return findByProperty(ROLL, roll, rowStartIdxAndCount);
	}

	public List<Planmodel> findByXscale(Object xscale, int... rowStartIdxAndCount) {
		return findByProperty(XSCALE, xscale, rowStartIdxAndCount);
	}

	public List<Planmodel> findByYscale(Object yscale, int... rowStartIdxAndCount) {
		return findByProperty(YSCALE, yscale, rowStartIdxAndCount);
	}

	public List<Planmodel> findByZscale(Object zscale, int... rowStartIdxAndCount) {
		return findByProperty(ZSCALE, zscale, rowStartIdxAndCount);
	}

	public List<Planmodel> findByTiletype(Object tiletype, int... rowStartIdxAndCount) {
		return findByProperty(TILETYPE, tiletype, rowStartIdxAndCount);
	}

	public List<Planmodel> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount) {
		return findByProperty(RESOURCEPATH, resourcepath, rowStartIdxAndCount);
	}

	public List<Planmodel> findByDescription(Object description, int... rowStartIdxAndCount) {
		return findByProperty(DESCRIPTION, description, rowStartIdxAndCount);
	}

	public List<Planmodel> findByIsload(Object isload, int... rowStartIdxAndCount) {
		return findByProperty(ISLOAD, isload, rowStartIdxAndCount);
	}

	/**
	 * Find all Planmodel entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Planmodel> all Planmodel entities
	 */
	@SuppressWarnings("unchecked")
	public List<Planmodel> findAll(final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding all Planmodel instances", Level.INFO, null);
		try {
			final String queryString = "select model from Planmodel model";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

    /**
     * 查询规划模型总行数
     * @return
     */
    @Override
    public Long count() {
        EntityManagerHelper.log("finding Planmodel count",Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Planmodel model";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }



    /**
     * 按名称关键字查询规划模型
     * @param keyWord
     * @param rowStartIdxAndCount
     * @return
     */
    @Override
    public List<Planmodel> findPlanmodelByNameKeyWord(String keyWord, int... rowStartIdxAndCount) {
        return findByPropertylike(MNAME,keyWord,rowStartIdxAndCount);
    }

    /**
     * 按名称关键字查询规划模型行数
     * @param keyWord
     * @return
     */
    @Override
    public Long findPlanmodelCountByNameKeyWord(String keyWord) {
        return findCountByProperty(MNAME,keyWord);
    }

    /**
     * 按照属性名称模糊查询规划模型
     * @param propertyName
     * @param value
     * @param rowStartIdxAndCount
     * @return
     */
    @Override
    public List<Planmodel> findByPropertylike(String propertyName, Object value, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding Planmodel instance with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select model from Planmodel model where model." + propertyName
                + " like :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue","%"+ value+"%");
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     *  根据科目的属性查询 总条数
     * @param propertyName
     * @param value
     * @return
     */
    @Override
    public Long findCountByProperty(String propertyName, Object value) {
        EntityManagerHelper.log("finding Planmodel count with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Planmodel model where model." + propertyName
                + " like  :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", "%" + value + "%");

            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<Planmodel>  findPlanModelByUnit(Unit unit,int... rowStartIdxAndCount){
        EntityManagerHelper.log("finding Planmodel instance by unit : " +unit.getId(),
            Level.INFO, null);
        try {
            final String queryString = "select model from Planmodel model where model.unit.id"
                + "= :unitId";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("unitId", unit.getId());
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find  PlanModel by unitid name failed", Level.SEVERE, re);
            throw re;
        }
    }
    public Long  findPlanModelCountByUnit(Unit unit){
        EntityManagerHelper.log("finding Planmodel count with unit: " +  unit.getId(),
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Planmodel model where model.unit.id"
                + " =  :unitId";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("unitId", unit.getId());

            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }
}

