package com.iezview.caas.daoimpl;

import java.util.List;
import java.util.logging.Level;
import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.iezview.caas.dao.IUsersDAO;
import com.iezview.caas.entity.Users;
import com.iezview.caas.utils.Digests;
import com.iezview.caas.utils.EncodeUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * A data access object (DAO) providing persistence and search support for Users
 * entities. Transaction control of the save(), update() and delete() operations
 * must be handled externally by senders of these methods or must be manually
 * added to each of these methods for data to be persisted to the JPA datastore.
 *
 * @see Users
 * @author MyEclipse Persistence Tools
 */
public class UsersDAO implements IUsersDAO {
	// property constants
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String SELT = "selt";
	public static final String FULLNAME = "fullname";
    public String error="";
    private static final  int SALT_SIZE=8;
	private EntityManager getEntityManager() {
		return EntityManagerHelper.getEntityManager();
	}

	/**
	 * Perform an initial save of a previously unsaved Users entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * UsersDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Users entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Users entity) {
		EntityManagerHelper.log("saving Users instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
		    if (StringUtils.isNotBlank(entity.getPlainpassword())){
                entryptPassword(entity);
            }
			getEntityManager().persist(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Users entity. This operation must be performed within
	 * the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * UsersDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Users entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Users entity) {
		EntityManagerHelper.log("deleting Users instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			entity = getEntityManager().getReference(Users.class, entity.getId());
            if (StringUtils.isNotBlank(entity.getPlainpassword())){
                entryptPassword(entity);
            }
			getEntityManager().remove(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Users entity and return it or a copy of it to
	 * the sender. A copy of the Users entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = UsersDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Users entity to update
	 * @return Users the persisted Users entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Users update(Users entity) {
		EntityManagerHelper.log("updating Users instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();

			Users result = getEntityManager().merge(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Users findById(Integer id) {
		EntityManagerHelper.log("finding Users instance with id: " + id, Level.INFO, null);
		try {
            getEntityManager().clear();//禁用hibernate 缓存  xml配置不起作用
			Users instance = getEntityManager().find(Users.class, id);
			return instance;
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Users entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Users property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<Users> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<Users> findByProperty(String propertyName, final Object value, final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding Users instance with property: " + propertyName + ", value: " + value,
				Level.INFO, null);
		try {
			final String queryString = "select model from Users model where model." + propertyName + "= :propertyValue";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<Users> findByUsername(Object username, int... rowStartIdxAndCount) {
		return findByProperty(USERNAME, username, rowStartIdxAndCount);
	}

	public List<Users> findByPassword(Object password, int... rowStartIdxAndCount) {
		return findByProperty(PASSWORD, password, rowStartIdxAndCount);
	}

	public List<Users> findBySelt(Object selt, int... rowStartIdxAndCount) {
		return findByProperty(SELT, selt, rowStartIdxAndCount);
	}

	public List<Users> findByFullname(Object fullname, int... rowStartIdxAndCount) {
		return findByProperty(FULLNAME, fullname, rowStartIdxAndCount);
	}

	/**
	 * Find all Users entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Users> all Users entities
	 */
	@SuppressWarnings("unchecked")
	public List<Users> findAll(final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding all Users instances", Level.INFO, null);
		try {
			final String queryString = "select model from Users model";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}


    /**
     * 用户登录时候对比
     * 设定安全的密码，生成随机的salt并经过1024次 sha-1 hash
     */
    public String entryptPassword(String userPass, Users user) {
        byte[] salt = EncodeUtils.hexDecode(user.getSelt());
        String saltStr = EncodeUtils.hexEncode(salt);
        byte[] hashPassword = Digests.sha1(userPass.getBytes(), salt, 1024);
        return EncodeUtils.hexEncode(hashPassword);
    }

    /**
     * 用于保存用户时候加密
     * 设定安全的密码，生成随机的salt并经过1024次 sha-1 hash
     * @param user
     */
    public void entryptPassword(Users user){
        byte[] salt =Digests.generateSalt(SALT_SIZE);
        user.setSelt(EncodeUtils.hexEncode(salt));
        byte[] hashPassword=Digests.sha1(user.getPlainpassword().getBytes(),salt,1024);
        user.setPassword(EncodeUtils.hexEncode(hashPassword));
    }
    /**
     * 密码相等
     *
     * @param userPass
     * @param user
     * @return
     */
    public boolean passwordEq(String userPass, Users user) {
        String inputPass = entryptPassword(userPass, user);
        if (user.getPassword().equals(inputPass))
            return true;
        else
            return false;

    }

    /**
     * 登录方法
     * 检查用户是否存在，检查用户名密码是否匹配
     *
     * @return
     */
    private boolean login(String userName,String passWord,Users user){
        if (user==null){
            this.error="用户不存在";
            return false;
        }
        if (!passwordEq(passWord,user)){
            this.error="用户名密码不匹配";
            return false;
        }
        else {
            return true;
        }

    }

    /**
     * 登录
     * @param userName 用户名
     * @param passWord 密码
     * @return
     */
    public boolean doLogin(String userName,String passWord){
        List<Users> users=findByUsername(userName);
        if (users.size()>0){
            return login(userName,passWord,findByUsername(userName).get(0));
        }else {
            this.error="用户不存在";
            return false;
        }

    }

    @Override
    public String error() {
       return this.error;
    }
}
