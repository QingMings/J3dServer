package com.iezview.caas.daoimpl;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.iezview.caas.dao.IUnitDAO;
import com.iezview.caas.entity.Area;
import com.iezview.caas.entity.Building;
import com.iezview.caas.entity.Subjects;
import com.iezview.caas.entity.Unit;

/**
 * A data access object (DAO) providing persistence and search support for Unit
 * entities. Transaction control of the save(), update() and delete() operations
 * must be handled externally by senders of these methods or must be manually
 * added to each of these methods for data to be persisted to the JPA datastore.
 *
 * @author MyEclipse Persistence Tools
 * @see Unit
 */
public class UnitDAO implements IUnitDAO {
    // property constants
    public static final String PARENT = "parent";
    public static final String UNAME = "uname";
    public static final String SHORTNAME = "shortname";
    public static final String KEYNAME = "keyname";
    public static final String UTYPE = "utype";
    public static final String SUBJECTS = "subjects";
    public static final String CENTRALLNG = "centrallng";
    public static final String CENTRALLAT = "centrallat";
    public static final String COVERAGEAREA = "coveragearea";
    public static final String UADDRESS = "uaddress";
    public static final String SYNOPSIS = "synopsis";
    public static final String ULEVEL = "ulevel";
    public static final String NODEKIND = "nodekind";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved Unit entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link EntityManager#persist(Object)
     * EntityManager#persist} operation.
     * <p>
     * <pre>
     *
     * EntityManagerHelper.beginTransaction();
     * UnitDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity Unit entity to persist
     * @throws RuntimeException when the operation fails
     */
    public void save(Unit entity) {
        EntityManagerHelper.log("saving Unit instance", Level.INFO, null);
        try {
            getEntityManager().getTransaction().begin();
            getEntityManager().persist(entity);
            getEntityManager().getTransaction().commit();
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            getEntityManager().getTransaction().rollback();
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 保存基地
     * 修改父级NodeLink 值为null
     * 设置基地ULevel =2
     *
     * @param entity 基地
     */
    @Override
    public void saveBase(Unit entity) {
        EntityManagerHelper.log("saving Unit instance", Level.INFO, null);
        try {
            getEntityManager().getTransaction().begin();
            if (entity.getParent() != null && entity.getParent() != 0) {
                Unit parentUnit = getEntityManager().find(Unit.class, entity.getParent());
                parentUnit.setNodekind(null);
                entity.setUlevel(2);
            }

            getEntityManager().persist(entity);
            getEntityManager().getTransaction().commit();
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            getEntityManager().getTransaction().rollback();
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }

    }

//    public String setSubjectsString(List<String> subjects) {
//        StringBuilder sb = new StringBuilder();
//        subjects.stream().forEach(subject -> sb.append(Unit.SPLIT_CHAR + subject));
//        return sb.deleteCharAt(0).toString();
//    }


    /**
     * Delete a persistent Unit entity. This operation must be performed within
     * the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the {@link EntityManager#remove(Object)
     * EntityManager#delete} operation.
     * <p>
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * UnitDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity Unit entity to delete
     * @throws RuntimeException when the operation fails
     */
    public void delete(Unit entity) {
        EntityManagerHelper.log("deleting Unit instance", Level.INFO, null);
        try {
            EntityManagerHelper.beginTransaction();
            entity = getEntityManager().getReference(Unit.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.commit();
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.rollback();
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved Unit entity and return it or a copy of it to
     * the sender. A copy of the Unit entity parameter is returned when the JPA
     * persistence mechanism has not previously been tracking the updated
     * entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link EntityManager#merge(Object) EntityManager#merge}
     * operation.
     * <p>
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = UnitDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity Unit entity to update
     * @return Unit the persisted Unit entity instance, may not be the same
     * @throws RuntimeException if the operation fails
     */
    public Unit update(Unit entity) {
        EntityManagerHelper.log("updating Unit instance", Level.INFO, null);
        try {
            EntityManagerHelper.beginTransaction();
            Unit result = getEntityManager().merge(entity);
            EntityManagerHelper.commit();
            EntityManagerHelper.log("update successful", Level.INFO, null);
            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.rollback();
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public Unit findById(Integer id) {
        EntityManagerHelper.log("finding Unit instance with id: " + id, Level.INFO, null);
        try {
            getEntityManager().clear();//禁用hibernate 缓存  xml配置不起作用
            Unit instance = getEntityManager().find(Unit.class, id);
            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }


    /**
     * Find all Unit entities with a specific property value.
     *
     * @param propertyName        the name of the Unit property to query
     * @param value               the property value to match
     * @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *                            row index in the query result-set to begin collecting the
     *                            results. rowStartIdxAndCount[1] specifies the the maximum
     *                            number of results to return.
     * @return List<Unit> found by query
     */
    @SuppressWarnings("unchecked")
    public List<Unit> findByProperty(String propertyName, final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding Unit instance with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select model from Unit model where model." + propertyName + "= :propertyValue order by model.orderby";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", value);
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 根据属性模糊查询单位
     *
     * @param propertyName
     * @param value
     * @param rowStartIdxAndCount
     * @return
     * @author qingmings
     */
    public List<Unit> findByPropertylike(String propertyName, final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding Unit instance with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select model from Unit model where model." + propertyName + " like :propertyValue order by model.orderby";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", "%" + value + "%");
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<Unit> findByParent(Object parent, int... rowStartIdxAndCount) {
        return findByProperty(PARENT, parent, rowStartIdxAndCount);
    }

    /**
     * 按关键字【UName】 模糊查询 单位
     *
     * @param keyWord             查询关键字
     * @param rowStartIdxAndCount index 0   从第几条开始查， index  1  查多少条
     * @return
     * @author qingmings
     */
    public List<Unit> findUnitByNameKeyWord(String keyWord, int... rowStartIdxAndCount) {
        return findByPropertylike(UNAME, keyWord, rowStartIdxAndCount);
    }

    @Override
    public List<Unit> findUnitByShortNameKeyWord(String keyWord, int... rowStartIdxAndCount) {
        return findByPropertylike(SHORTNAME, keyWord, rowStartIdxAndCount);
    }


    /**
     * 根据行政区域查询单位
     *
     * @param area                行政区域
     * @param rowStartIdxAndCount index 0   从第几条开始查， index  1  查多少条
     * @return
     * @author qingmings
     */
    @Override
    public List<Unit> findUnitByArea(Area area, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("find Unit instances with Area.id:" + area.getId(), Level.INFO, null);
        try {
            final String queryString = "select model from Unit model where model.area.id= :areaId  order by model.orderby";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("areaId", area.getId());
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by Area.id failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public List<Unit> findYanJjiuSuoByArea(Area area, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("find Unit instances with Area.id:" + area.getId(), Level.INFO, null);
        try {
            final String queryString = "select model from Unit model where model.area.id= :areaId and model.parent=0 and model.ulevel=1  order by model.orderby";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("areaId", area.getId());
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by Area.id failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public List<Unit> findJiDiByYanJiuSuoAndAreaID(Unit unit, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("find Unit instances with parent:" + unit.getParent(), Level.INFO, null);
        try {
            final String queryString = "select model from Unit model where model.area.id= :areaId and model.parent=:parentId  order by model.orderby";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("areaId", unit.getArea().getId());
            query.setParameter("parentId", unit.getId());
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by parent failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public List<Unit> findJidiByYanJiuSuo(Unit unit, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("find Unit instances with parent:" + unit.getParent(), Level.INFO, null);
        try {
            final String queryString = "select model from Unit model where   model.parent=:parentId  order by model.orderby";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("parentId", unit.getId());
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by parent failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public List<Unit> findUnitByAddressKeyWord(String keyWord, int... rowStartIdxAndCount) {
        return findByPropertylike(UADDRESS, keyWord, rowStartIdxAndCount);
    }

    public List<Unit> findByUname(Object uname, int... rowStartIdxAndCount) {
        return findByProperty(UNAME, uname, rowStartIdxAndCount);
    }

    public List<Unit> findByShortname(Object shortname, int... rowStartIdxAndCount) {
        return findByProperty(SHORTNAME, shortname, rowStartIdxAndCount);
    }

    public List<Unit> findByKeyname(Object keyname, int... rowStartIdxAndCount) {
        return findByProperty(KEYNAME, keyname, rowStartIdxAndCount);
    }

    public List<Unit> findByUtype(Object utype, int... rowStartIdxAndCount) {
        return findByProperty(UTYPE, utype, rowStartIdxAndCount);
    }

    public List<Unit> findBySubjects(Object subjects, int... rowStartIdxAndCount) {
        return findByProperty(SUBJECTS, subjects, rowStartIdxAndCount);
    }

    public List<Unit> findByCentrallng(Object centrallng, int... rowStartIdxAndCount) {
        return findByProperty(CENTRALLNG, centrallng, rowStartIdxAndCount);
    }

    public List<Unit> findByCentrallat(Object centrallat, int... rowStartIdxAndCount) {
        return findByProperty(CENTRALLAT, centrallat, rowStartIdxAndCount);
    }

    public List<Unit> findByCoveragearea(Object coveragearea, int... rowStartIdxAndCount) {
        return findByProperty(COVERAGEAREA, coveragearea, rowStartIdxAndCount);
    }

    public List<Unit> findByUaddress(Object uaddress, int... rowStartIdxAndCount) {
        return findByProperty(UADDRESS, uaddress, rowStartIdxAndCount);
    }

    public List<Unit> findBySynopsis(Object synopsis, int... rowStartIdxAndCount) {
        return findByProperty(SYNOPSIS, synopsis, rowStartIdxAndCount);
    }

    public List<Unit> findByUlevel(Object ulevel, int... rowStartIdxAndCount) {
        return findByProperty(ULEVEL, ulevel, rowStartIdxAndCount);
    }

    @Override
    public Long findCountByUlevel(Object ulevel) {
        return findUnitCountByProperty(ULEVEL, ulevel);
    }

    public List<Unit> findByNodekind(Object nodekind, int... rowStartIdxAndCount) {
        return findByProperty(NODEKIND, nodekind, rowStartIdxAndCount);
    }

    /**
     * Find all Unit entities.
     *
     * @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *                            row index in the query result-set to begin collecting the
     *                            results. rowStartIdxAndCount[1] specifies the the maximum
     *                            count of results to return.
     * @return List<Unit> all Unit entities
     */
    @SuppressWarnings("unchecked")
    public List<Unit> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all Unit instances", Level.INFO, null);
        try {
            final String queryString = "select model from Unit model order by model.orderby";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 查询研究所
     *
     * @param rowStartIdxAndCount
     * @return
     */
    @Override
    public List<Unit> findAllYanJiuSuo(int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all Unit instances", Level.INFO, null);
        try {
            final String queryString = "select model from Unit model where model.parent=0 and model.ulevel=1 order by model.orderby";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 根据属性查询符合范围的单位总数 用于分页
     *
     * @param propertyName
     * @param value
     * @return
     */
    @Override
    public Long findUnitCountByPropertylike(String propertyName, Object value) {
        EntityManagerHelper.log("finding Unit count with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Unit model where model." + propertyName
                + " like  :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", "%" + value + "%");

            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public Long findUnitCountByProperty(String propertyName, Object value) {
        EntityManagerHelper.log("finding Unit count with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Unit model where model." + propertyName
                + " =  :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", value);

            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 查询符合名称关键字的单位总条数
     *
     * @param keyWord 名称关键字
     * @return
     */
    @Override
    public Long findUnitCountByNameKeyWord(String keyWord) {
        return findUnitCountByPropertylike(UNAME, keyWord);
    }

    /**
     * 查询符合地名关键字的单位总条数
     *
     * @param keyWord
     * @return
     */
    @Override
    public Long findUnitCountByAddressKeyWord(String keyWord) {
        return findUnitCountByPropertylike(UADDRESS, keyWord);
    }

    /**
     * @param area
     * @return
     * @description 查询某个行政区域的单位总条数
     */
    @Override
    public Long findUnitCountByArea(Area area) {
        EntityManagerHelper.log("finding Unit count with Area.id : " + area.getId(),
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Unit model where model.area.id= :areaId";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("areaId", area.getId());
            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 根据院所名称关键字查询下属基地
     *
     * @param keyWord             院所名称关键字
     * @param rowStartIdxAndCount index 0   从第几条开始查， index  1  查多少条
     * @return
     */
    @Override
    public List<Unit> findUnitByParentNameKeyWord(String keyWord, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding Unit instance with parent.name like: " + keyWord,
            Level.INFO, null);
        try {
            final String queryString = "select model from Unit model where model.parent in (select model1.id from Unit model1 where model1.uname like :parentNameKeyWord and model1.ulevel=1) order by model.orderby";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("parentNameKeyWord", "%" + keyWord + "%");
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 根据院所名称关键字查询下属基地总行数
     *
     * @param keyWord 院所名称关键字
     * @return
     */
    @Override
    public Long findUnitCountByParentNameKeyWord(String keyWord) {
        EntityManagerHelper.log("finding Units count with parent.name like: " + keyWord,
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Unit model where model.parent in (select model1.id from Unit model1 where model1.uname like :parentNameKeyWord and model1.ulevel=1)";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("parentNameKeyWord", "%" + keyWord + "%");

            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 查询单位 count()
     *
     * @return
     */
    @Override
    public Long count() {
        EntityManagerHelper.log("finding count Unit instances", Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Unit model";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }

    }

    /**
     * 研究所-->  基地-->   站
     * Ulevel:  1  -->   2  -->   3
     * 检查 排序字段
     * 如果是 研究所，  查询所有研究所排序字段的值，对比参数的排序值，如果结果集为0 说明排序值可用，否则不可用
     * 如果是 基地 , 查询其所属研究所下的所有基地的排序值，对比参数的排序值，如果结果集为0,说明排序值可用，否则不可用
     * 如果是 站   查询器所属的基地下的所有站的排序值，对比参数的排序值，如果结果集为0，说明排序值可用，否则不可用
     *
     * @param unit 单位
     * @return
     */
    @Override
    public List<Unit> checkOrder(Unit unit) {
        if (unit.getParent() != null) {
            EntityManagerHelper.log("check  Unit orderby value:" + unit.getUlevel(), Level.INFO, null);
            String queryString = "select model from Unit model where model.parent= :parentId and model.orderby =:orderBy";
            if (unit.getId() != null) {
                queryString += " and model.id != :unitId";
            }
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("parentId", unit.getParent());
            query.setParameter("orderBy", unit.getOrderby());
            if (unit.getId() != null) {
                query.setParameter("unitId", unit.getId());
            }

            return query.getResultList();
        } else {
            throw new RuntimeException("单位 父级Id 不能为空");
        }

    }

    /**
     * 查询不包括的单位信息
     *
     * @param units
     * @return
     */
    @Override
    public List<Unit> findUnitNotIncluded(List<Unit> units, int... rowStartIdxAndCount) {
        String ids = ids(units);
        EntityManagerHelper.log("finding Units  not  incloded:( " + ids + ")",
            Level.INFO, null);
        try {
            final String queryString = "select model from Unit model where model.id not in (" + ids + ") order by model.orderby";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 保存全部
     *
     * @param units
     */
    @Override
    public void saveAll(List<Unit> units) {
        EntityManagerHelper.log("saving Units instance", Level.INFO, null);
        try {
            getEntityManager().getTransaction().begin();
            units.forEach(unit -> getEntityManager().persist(unit));
//            getEntityManager().persist(entity);
            getEntityManager().getTransaction().commit();
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            getEntityManager().getTransaction().rollback();
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * 组合ids
     *
     * @param units
     * @return
     */
    private String ids(List<Unit> units) {
        StringBuilder sb = new StringBuilder();
        units.forEach(unit -> {
            Integer id = unit.getId();
            if (id != null) {
                sb.append(Unit.SPLIT_CHAR + id);
            } else {
                throw new RuntimeException("unit id must be not null!");
            }

        });
        return sb.deleteCharAt(0).toString();
    }

    /**
     * 多条件查询
     * @param queryType 查询类型   1 查询研究所  2 查询基地
     * @param unit  单位
     * @param utype  单位类型
     * @param area
     * @param subjects  学科
     * @param ecoRegion   生态区
     * @return
     */
    @Override
    public List<Unit> findUnitByMultipleonditions(int queryType, Unit unit, String utype, Area area, Subjects subjects, String ecoRegion) {
        EntityManagerHelper.log("finding Unit instance by Multipleonditions",
            Level.INFO, null);
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("select model from Unit model ");
            //判断  unit不为空
            if (unit != null) {
                if (queryType == 1) {
                    str_Unit(sb, true);
                } else {
                    str_UnitJiDi(sb, true);
                }
                //判断utype不为空
                if (utype != null) {
                    str_uType(sb, false);
                    if (area != null) {
                        str_area(sb, false);
                        if (subjects != null) {
                            str_subject(sb, false);
                        }
                    } else {
                        //判断 subjects 不为空
                        if (subjects != null) {
                            str_subject(sb, false);
                        }
                    }
                } else {
                    //判断 area不为空
                    if (area != null) {
                        str_area(sb, false);
                    } else {
                        //判断 subjects 不为空
                        if (subjects != null) {
                            str_subject(sb, false);
                        }
                    }
                }

            } else {
                //判断utype不为空
                if (utype != null) {
                    str_uType(sb, true);
                    if (area != null) {
                        str_area(sb, false);
                        if (subjects != null) {
                            str_subject(sb, false);
                        }
                    } else {
                        //判断 subjects 不为空
                        if (subjects != null) {
                            str_subject(sb, false);
                        }
                    }
                } else {
                    //判断 area不为空
                    if (area != null) {
                        str_area(sb, true);
                        if (subjects != null) {
                            str_subject(sb, false);
                        }
                    } else {
                        //判断 subjects 不为空
                        if (subjects != null) {
                            str_subject(sb, true);
                        }
                    }
                }
            }

        } catch (RuntimeException re) {
            EntityManagerHelper.log("find Unit instance by Multipleonditions failed", Level.SEVERE, re);
            throw re;
        }
        final String queryString = sb.toString();
        Query query = getEntityManager().createQuery(queryString);
        query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
        if (unit != null) {
            query.setParameter("unitId", unit.getId());
        }
        if (utype != null) {
            query.setParameter("uType", "%" + utype + "%");
        }
        if (area != null) {
            query.setParameter("areaId", area.getId());
        }
        if (subjects != null) {
            query.setParameter("subjectId", subjects.getId());
        }


        List<Unit> units = query.getResultList();


        return units;
    }

    private void str_Unit(StringBuilder sb, boolean whereOrAnd) {
        if (whereOrAnd) {
            sb.append(where());
        } else {
            sb.append(and());
        }
        sb.append(" model.id = :unitId ");
    }

    private void str_UnitJiDi(StringBuilder sb, boolean whereOrAnd) {
        if (whereOrAnd) {
            sb.append(where());
        } else {
            sb.append(and());
        }
        sb.append(" model.parent=:unitId ");

    }

    private void str_area(StringBuilder sb, boolean whereOrAnd) {
        if (whereOrAnd) {
            sb.append(where());
        } else {
            sb.append(and());
        }
        sb.append("model.area.id =:areaId ");
    }

    private void str_uType(StringBuilder sb, boolean whereOrAnd) {
        if (whereOrAnd) {
            sb.append(where());
        } else {
            sb.append(and());
        }
        sb.append("model.utype like :uType ");
    }

    private void str_subject(StringBuilder sb, boolean whereOrAnd) {
        if (whereOrAnd) {
            sb.append(where());
        } else {
            sb.append(and());
        }
        sb.append("model.id in (select model1.id from Unit model1 join model1.subjects  subject where subject.id = :subjectId) ");
    }

    private String where() {
        return "where ";
    }


    private String and() {
        return "and ";
    }
}
