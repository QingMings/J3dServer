package com.iezview.caas.daoimpl;

import java.util.List;
import java.util.logging.Level;
import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.iezview.caas.dao.IRemotesensemapDAO;
import com.iezview.caas.entity.Remotesensemap;

/**
 * A data access object (DAO) providing persistence and search support for
 * Remotesensemap entities. Transaction control of the save(), update() and
 * delete() operations must be handled externally by senders of these methods or
 * must be manually added to each of these methods for data to be persisted to
 * the JPA datastore.
 *
 * @see Remotesensemap
 * @author MyEclipse Persistence Tools
 */
public class RemotesensemapDAO implements IRemotesensemapDAO {
	// property constants
	public static final String SNAME = "sname";
	public static final String MAXLNG = "maxlng";
	public static final String MAXLAT = "maxlat";
	public static final String MINLNG = "minlng";
	public static final String MINLAT = "minlat";
	public static final String RESOURCEPATH = "resourcepath";
	public static final String REMARK = "remark";

	private EntityManager getEntityManager() {
		return EntityManagerHelper.getEntityManager();
	}

	/**
	 * Perform an initial save of a previously unsaved Remotesensemap entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * RemotesensemapDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Remotesensemap entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Remotesensemap entity) {
		EntityManagerHelper.log("saving Remotesensemap instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			getEntityManager().persist(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Remotesensemap entity. This operation must be
	 * performed within the a database transaction context for the entity's data
	 * to be permanently deleted from the persistence store, i.e., database.
	 * This method uses the
	 * {@link EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * RemotesensemapDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Remotesensemap entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Remotesensemap entity) {
		EntityManagerHelper.log("deleting Remotesensemap instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			entity = getEntityManager().getReference(Remotesensemap.class, entity.getId());
			getEntityManager().remove(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Remotesensemap entity and return it or a copy
	 * of it to the sender. A copy of the Remotesensemap entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity. This operation must be performed within the
	 * a database transaction context for the entity's data to be permanently
	 * saved to the persistence store, i.e., database. This method uses the
	 * {@link EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = RemotesensemapDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Remotesensemap entity to update
	 * @return Remotesensemap the persisted Remotesensemap entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Remotesensemap update(Remotesensemap entity) {
		EntityManagerHelper.log("updating Remotesensemap instance", Level.INFO, null);
		try {
		    EntityManagerHelper.beginTransaction();
			Remotesensemap result = getEntityManager().merge(entity);
			EntityManagerHelper.commit();
			EntityManagerHelper.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
		    EntityManagerHelper.rollback();
			EntityManagerHelper.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public Remotesensemap findById(Integer id) {
		EntityManagerHelper.log("finding Remotesensemap instance with id: " + id, Level.INFO, null);
		try {
            getEntityManager().clear();//禁用hibernate 缓存  xml配置不起作用
			Remotesensemap instance = getEntityManager().find(Remotesensemap.class, id);
			return instance;
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Remotesensemap entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Remotesensemap property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            number of results to return.
	 * @return List<Remotesensemap> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<Remotesensemap> findByProperty(String propertyName, final Object value,
			final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding Remotesensemap instance with property: " + propertyName + ", value: " + value,
				Level.INFO, null);
		try {
			final String queryString = "select model from Remotesensemap model where model." + propertyName
					+ "= :propertyValue";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			query.setParameter("propertyValue", value);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<Remotesensemap> findBySname(Object sname, int... rowStartIdxAndCount) {
		return findByProperty(SNAME, sname, rowStartIdxAndCount);
	}

	public List<Remotesensemap> findByMaxlng(Object maxlng, int... rowStartIdxAndCount) {
		return findByProperty(MAXLNG, maxlng, rowStartIdxAndCount);
	}

	public List<Remotesensemap> findByMaxlat(Object maxlat, int... rowStartIdxAndCount) {
		return findByProperty(MAXLAT, maxlat, rowStartIdxAndCount);
	}

	public List<Remotesensemap> findByMinlng(Object minlng, int... rowStartIdxAndCount) {
		return findByProperty(MINLNG, minlng, rowStartIdxAndCount);
	}

	public List<Remotesensemap> findByMinlat(Object minlat, int... rowStartIdxAndCount) {
		return findByProperty(MINLAT, minlat, rowStartIdxAndCount);
	}

	public List<Remotesensemap> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount) {
		return findByProperty(RESOURCEPATH, resourcepath, rowStartIdxAndCount);
	}

	public List<Remotesensemap> findByRemark(Object remark, int... rowStartIdxAndCount) {
		return findByProperty(REMARK, remark, rowStartIdxAndCount);
	}

	/**
	 * Find all Remotesensemap entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Remotesensemap> all Remotesensemap entities
	 */
	@SuppressWarnings("unchecked")
	public List<Remotesensemap> findAll(final int... rowStartIdxAndCount) {
		EntityManagerHelper.log("finding all Remotesensemap instances", Level.INFO, null);
		try {
			final String queryString = "select model from Remotesensemap model";
			Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
				int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
				if (rowStartIdx > 0) {
					query.setFirstResult(rowStartIdx);
				}

				if (rowStartIdxAndCount.length > 1) {
					int rowCount = Math.max(0, rowStartIdxAndCount[1]);
					if (rowCount > 0) {
						query.setMaxResults(rowCount);
					}
				}
			}
			return query.getResultList();
		} catch (RuntimeException re) {
			EntityManagerHelper.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

    @Override
    public Long count() {
        EntityManagerHelper.log("finding Remotesensemap count",Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Remotesensemap model";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public List<Remotesensemap> findRemotesensemapByNameKeyWord(String keyWord, int... rowStartIdxAndCount) {
       return findByPropertylike(SNAME,keyWord,rowStartIdxAndCount);
    }

    @Override
    public Long findRemotesensemapCountByNameKeyWord(String keyWord) {
        return findCountByProperty(SNAME,keyWord);
    }

    @Override
    public List<Remotesensemap> findByPropertylike(String propertyName, Object value, int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding Remotesensemap instance with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select model from Remotesensemap model where model." + propertyName
                + " like :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue","%"+ value+"%");
            if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);
                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);
                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }
            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

    @Override
    public Long findCountByProperty(String propertyName, Object value) {
        EntityManagerHelper.log("finding Remotesensemap count with property: " + propertyName + ", value: " + value,
            Level.INFO, null);
        try {
            final String queryString = "select count(model.id) from Remotesensemap model where model." + propertyName
                + " like  :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
            query.setParameter("propertyValue", "%" + value + "%");

            return (Long) query.getSingleResult();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed", Level.SEVERE, re);
            throw re;
        }
    }

}
