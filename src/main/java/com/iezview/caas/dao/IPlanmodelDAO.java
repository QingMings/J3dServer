package com.iezview.caas.dao;

import java.sql.Time;
import java.util.List;

import com.iezview.caas.entity.Planmodel;
import com.iezview.caas.entity.Unit;

/**
 * Interface for PlanmodelDAO.
 *
 * @author MyEclipse Persistence Tools
 */

public interface IPlanmodelDAO {
	/**
	 * Perform an initial save of a previously unsaved Planmodel entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * IPlanmodelDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Planmodel entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Planmodel entity);

	/**
	 * Delete a persistent Planmodel entity. This operation must be performed
	 * within the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link javax.persistence.EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * IPlanmodelDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Planmodel entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Planmodel entity);

	/**
	 * Persist a previously saved Planmodel entity and return it or a copy of it
	 * to the sender. A copy of the Planmodel entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = IPlanmodelDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Planmodel entity to update
	 * @return Planmodel the persisted Planmodel entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Planmodel update(Planmodel entity);

	public Planmodel findById(Integer id);

	/**
	 * Find all Planmodel entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Planmodel property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Planmodel> found by query
	 */
	public List<Planmodel> findByProperty(String propertyName, Object value, int... rowStartIdxAndCount);

	public List<Planmodel> findByMname(Object mname, int... rowStartIdxAndCount);

	public List<Planmodel> findByCentrallng(Object centrallng, int... rowStartIdxAndCount);

	public List<Planmodel> findByCentrallat(Object centrallat, int... rowStartIdxAndCount);

	public List<Planmodel> findByCentralheight(Object centralheight, int... rowStartIdxAndCount);

	public List<Planmodel> findByDataformat(Object dataformat, int... rowStartIdxAndCount);

	public List<Planmodel> findByAltitudemode(Object altitudemode, int... rowStartIdxAndCount);

	public List<Planmodel> findByHeading(Object heading, int... rowStartIdxAndCount);

	public List<Planmodel> findByTilt(Object tilt, int... rowStartIdxAndCount);

	public List<Planmodel> findByRoll(Object roll, int... rowStartIdxAndCount);

	public List<Planmodel> findByXscale(Object xscale, int... rowStartIdxAndCount);

	public List<Planmodel> findByYscale(Object yscale, int... rowStartIdxAndCount);

	public List<Planmodel> findByZscale(Object zscale, int... rowStartIdxAndCount);

	public List<Planmodel> findByTiletype(Object tiletype, int... rowStartIdxAndCount);

	public List<Planmodel> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount);

	public List<Planmodel> findByDescription(Object description, int... rowStartIdxAndCount);

	public List<Planmodel> findByIsload(Object isload, int... rowStartIdxAndCount);

	/**
	 * Find all Planmodel entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Planmodel> all Planmodel entities
	 */
	public List<Planmodel> findAll(int... rowStartIdxAndCount);

    /**
     * 查询总行数
     * @return
     */
	public Long count();

    /**
     * 测试名称关键字查询 规划模型
     * @param keyWord
     * @param rowStartIdxAndCount
     * @return
     */
	public List<Planmodel> findPlanmodelByNameKeyWord(String keyWord,int... rowStartIdxAndCount);

    /**
     * 测试 名称关键字查询规划模型总行数
     * @param keyWord
     * @return
     */
	public Long  findPlanmodelCountByNameKeyWord(String keyWord);

    /**
     * 按照规划模型 属性模糊查询
     * @param propertyName
     * @param value
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Planmodel> findByPropertylike(String propertyName, Object value, int... rowStartIdxAndCount);

    /**
     * 按照规划模型属性模糊查询 行数
     * @param propertyName
     * @param value
     * @return
     */
    public Long findCountByProperty(String propertyName,Object value);

    /**
     * 按照 单位查询 规划模型
     * @param unit
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Planmodel>  findPlanModelByUnit(Unit unit, int... rowStartIdxAndCount);

    /**
     * 按照单位查询规划模型总数
     * @param unit
     * @return
     */
    public Long  findPlanModelCountByUnit(Unit unit);
}
