//package com.iezview.caas.dao;
//
//import java.util.List;
//
//import com.iezview.caas.entity.Unitobliquerel;
//
///**
// * Interface for UnitobliquerelDAO.
// *
// * @author MyEclipse Persistence Tools
// */
//
//public interface IUnitobliquerelDAO {
//	/**
//	 * Perform an initial save of a previously unsaved Unitobliquerel entity.
//	 * All subsequent persist actions of this entity should use the #update()
//	 * method. This operation must be performed within the a database
//	 * transaction context for the entity's data to be permanently saved to the
//	 * persistence store, i.e., database. This method uses the
//	 * {@link javax.persistence.EntityManager#persist(Object)
//	 * EntityManager#persist} operation.
//	 *
//	 * <pre>
//	 *
//	 * EntityManagerHelper.beginTransaction();
//	 * IUnitobliquerelDAO.save(entity);
//	 * EntityManagerHelper.commit();
//	 * </pre>
//	 *
//	 * @param entity
//	 *            Unitobliquerel entity to persist
//	 * @throws RuntimeException
//	 *             when the operation fails
//	 */
//	public void save(Unitobliquerel entity);
//
//	/**
//	 * Delete a persistent Unitobliquerel entity. This operation must be
//	 * performed within the a database transaction context for the entity's data
//	 * to be permanently deleted from the persistence store, i.e., database.
//	 * This method uses the
//	 * {@link javax.persistence.EntityManager#remove(Object)
//	 * EntityManager#delete} operation.
//	 *
//	 * <pre>
//	 * EntityManagerHelper.beginTransaction();
//	 * IUnitobliquerelDAO.delete(entity);
//	 * EntityManagerHelper.commit();
//	 * entity = null;
//	 * </pre>
//	 *
//	 * @param entity
//	 *            Unitobliquerel entity to delete
//	 * @throws RuntimeException
//	 *             when the operation fails
//	 */
//	public void delete(Unitobliquerel entity);
//
//	/**
//	 * Persist a previously saved Unitobliquerel entity and return it or a copy
//	 * of it to the sender. A copy of the Unitobliquerel entity parameter is
//	 * returned when the JPA persistence mechanism has not previously been
//	 * tracking the updated entity. This operation must be performed within the
//	 * a database transaction context for the entity's data to be permanently
//	 * saved to the persistence store, i.e., database. This method uses the
//	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
//	 * operation.
//	 *
//	 * <pre>
//	 * EntityManagerHelper.beginTransaction();
//	 * entity = IUnitobliquerelDAO.update(entity);
//	 * EntityManagerHelper.commit();
//	 * </pre>
//	 *
//	 * @param entity
//	 *            Unitobliquerel entity to update
//	 * @return Unitobliquerel the persisted Unitobliquerel entity instance, may
//	 *         not be the same
//	 * @throws RuntimeException
//	 *             if the operation fails
//	 */
//	public Unitobliquerel update(Unitobliquerel entity);
//
//	public Unitobliquerel findById(Integer id);
//
//	/**
//	 * Find all Unitobliquerel entities with a specific property value.
//	 *
//	 * @param propertyName
//	 *            the name of the Unitobliquerel property to query
//	 * @param value
//	 *            the property value to match
//	 * @param rowStartIdxAndCount
//	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
//	 *            row index in the query result-set to begin collecting the
//	 *            results. rowStartIdxAndCount[1] specifies the the maximum
//	 *            count of results to return.
//	 * @return List<Unitobliquerel> found by query
//	 */
//	public List<Unitobliquerel> findByProperty(String propertyName, Object value, int... rowStartIdxAndCount);
//
//	public List<Unitobliquerel> findByRemark(Object remark, int... rowStartIdxAndCount);
//
//	/**
//	 * Find all Unitobliquerel entities.
//	 *
//	 * @param rowStartIdxAndCount
//	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
//	 *            row index in the query result-set to begin collecting the
//	 *            results. rowStartIdxAndCount[1] specifies the the maximum
//	 *            count of results to return.
//	 * @return List<Unitobliquerel> all Unitobliquerel entities
//	 */
//	public List<Unitobliquerel> findAll(int... rowStartIdxAndCount);
//}
