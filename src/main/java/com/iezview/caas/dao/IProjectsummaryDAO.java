package com.iezview.caas.dao;

import java.util.List;

import com.iezview.caas.entity.Projectsummary;

/**
 * Interface for ProjectsummaryDAO.
 *
 * @author MyEclipse Persistence Tools
 */

public interface IProjectsummaryDAO {
	/**
	 * Perform an initial save of a previously unsaved Projectsummary entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * IProjectsummaryDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Projectsummary entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Projectsummary entity);

	/**
	 * Delete a persistent Projectsummary entity. This operation must be
	 * performed within the a database transaction context for the entity's data
	 * to be permanently deleted from the persistence store, i.e., database.
	 * This method uses the
	 * {@link javax.persistence.EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * IProjectsummaryDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Projectsummary entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Projectsummary entity);

	/**
	 * Persist a previously saved Projectsummary entity and return it or a copy
	 * of it to the sender. A copy of the Projectsummary entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity. This operation must be performed within the
	 * a database transaction context for the entity's data to be permanently
	 * saved to the persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = IProjectsummaryDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Projectsummary entity to update
	 * @return Projectsummary the persisted Projectsummary entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Projectsummary update(Projectsummary entity);

	public Projectsummary findById(Integer id);

	/**
	 * Find all Projectsummary entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Projectsummary property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Projectsummary> found by query
	 */
	public List<Projectsummary> findByProperty(String propertyName, Object value, int... rowStartIdxAndCount);

	public List<Projectsummary> findByPname(Object pname, int... rowStartIdxAndCount);

	public List<Projectsummary> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount);

	public List<Projectsummary> findByRemark(Object remark, int... rowStartIdxAndCount);

	/**
	 * Find all Projectsummary entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Projectsummary> all Projectsummary entities
	 */
	public List<Projectsummary> findAll(int... rowStartIdxAndCount);

    /**
     * 查询总行数
     * @return
     */
    public Long count();

    /**
     * 名称关键字查询 项目汇总表
     * @param keyWord
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Projectsummary> findProjectsummaryByNameKeyWord(String keyWord,int... rowStartIdxAndCount);

    /**
     * 名称关键字查询项目汇总表总行数
     * @param keyWord
     * @return
     */
    public Long  findProjectsummaryCountByNameKeyWord(String keyWord);

    /**
     * 按照项目汇总表 属性模糊查询
     * @param propertyName
     * @param value
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Projectsummary> findByPropertylike(String propertyName, Object value, int... rowStartIdxAndCount);

    /**
     * 按照项目汇总表属性模糊查询 行数
     * @param propertyName
     * @param value
     * @return
     */
    public Long findCountByProperty(String propertyName,Object value);
}
