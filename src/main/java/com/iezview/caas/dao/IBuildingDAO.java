package com.iezview.caas.dao;

import java.util.List;

import com.iezview.caas.entity.Area;
import com.iezview.caas.entity.Building;
import com.iezview.caas.entity.Subjects;
import com.iezview.caas.entity.Unit;

/**
 * Interface for BuildingDAO.
 *
 * @author MyEclipse Persistence Tools
 */

public interface IBuildingDAO {
	/**
	 * Perform an initial save of a previously unsaved Building entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * IBuildingDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Building entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Building entity);

	/**
	 * Delete a persistent Building entity. This operation must be performed
	 * within the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link javax.persistence.EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * IBuildingDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Building entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Building entity);

	/**
	 * Persist a previously saved Building entity and return it or a copy of it
	 * to the sender. A copy of the Building entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = IBuildingDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Building entity to update
	 * @return Building the persisted Building entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Building update(Building entity);

	public Building findById(Integer id);

	/**
	 * Find all Building entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Building property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Building> found by query
	 */
	public List<Building> findByProperty(String propertyName, Object value, int... rowStartIdxAndCount);

	public List<Building> findByNumber(Object number, int... rowStartIdxAndCount);

	public List<Building> findByBname(Object bname, int... rowStartIdxAndCount);

	public List<Building> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount);

	public List<Building> findByBtype(Object btype, int... rowStartIdxAndCount);

	public List<Building> findByYears(Object years, int... rowStartIdxAndCount);

	public List<Building> findByHeight(Object height, int... rowStartIdxAndCount);

	public List<Building> findByPurpose(Object purpose, int... rowStartIdxAndCount);

	public List<Building> findByTotalarea(Object totalarea, int... rowStartIdxAndCount);

	public List<Building> findByAbovearea(Object abovearea, int... rowStartIdxAndCount);

	public List<Building> findByUnderarea(Object underarea, int... rowStartIdxAndCount);

	public List<Building> findByConstructionunits(Object constructionunits, int... rowStartIdxAndCount);

	public List<Building> findByCentrallng(Object centrallng, int... rowStartIdxAndCount);

	public List<Building> findByCentrallat(Object centrallat, int... rowStartIdxAndCount);

	public List<Building> findBySynopsis(Object synopsis, int... rowStartIdxAndCount);

	/**
	 * Find all Building entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Building> all Building entities
	 */
	public List<Building> findAll(int... rowStartIdxAndCount);

    /**
     * @desc  根据名称关键字 模糊查询建筑信息
     * @author  qingmings
     * @param keyWord  名称关键字
     * @param rowStartIdxAndCount  index 0  从第几条开始查，  index 1 查多少条
     * @return
     */
	public List<Building>  findBuildingByNameKeyWord(String keyWord,int... rowStartIdxAndCount);

    /**
     * return totals  of area data
     * @return
     */
    public Long   findBuildingCountByPropertyLike(String propertyName, Object value);

    /**
     * 根据名称关键字查询建筑总行数
     * @param keyword
     * @return
     */
    public Long findBuildingCountByNameKeyWord(String keyword);
    public Long count();


    /**
     * 根据单位查询建筑
     * @param unit  单位
     * @param rowStartIdxAndCount index 0  从第几条开始查，  index 1 查多少条
     * @return
     */
    public List<Building> findBuildingByUnit(Unit unit,int... rowStartIdxAndCount);

    /**
     * 根据单位查询建筑总行数
     * @param unit
     * @return
     */
    public Long findBuildingCountByUnit(Unit unit);

    /**
     * 按院所名称关键字查询下属建筑
     * @param keyWord 院所名称关键字
     * @param rowStartIdxAndCount index 0  从第几条开始查，  index 1 查多少条
     * @return
     */
    public List<Building> findBuildingByUnitNameKeyWord(String keyWord,int... rowStartIdxAndCount );

    /**
     * 按院所名称关键字查询下属建筑总行数
     * @param keyWord 院所名称关键字
     * @return
     */
    public Long findBuildingCountByUnitNameKeyWord(String keyWord);

    /**
     * 插入多条
     * @param buildings
     */
    public  void   saveAll(List<Building> buildings);

    /**
     * 根据行政区域查询 建筑分布
     * @param area
     * @param rowStartIdxAndCount
     * @return
     */
    public  List<Building>  findBuildingByArea(Area area,int... rowStartIdxAndCount );

    /**
     * 根据行政区域查询 建筑分布 总数
     * @param area
     * @return
     */
    public  Long  findBuildingCountByArea(Area area);

    /**
     * 根据 学科查询 建筑分布
     * @param subjects
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Building> findBuildingBySubject(Subjects subjects,int... rowStartIdxAndCount );

    /**
     * 根据 学科查询 建筑分布 总数
     * @param area
     * @return
     */
    public Long  findBuildingCountBySubject(Subjects area);
}
