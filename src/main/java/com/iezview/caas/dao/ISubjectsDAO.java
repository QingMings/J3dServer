package com.iezview.caas.dao;

import java.util.List;

import com.iezview.caas.entity.Subjects;

/**
 * Interface for SubjectsDAO.
 *
 * @author MyEclipse Persistence Tools
 */

public interface ISubjectsDAO {
    /**
     * Perform an initial save of a previously unsaved Subjects entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object)
     * EntityManager#persist} operation.
     * <p>
     * <pre>
     *
     * EntityManagerHelper.beginTransaction();
     * ISubjectsDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity Subjects entity to persist
     * @throws RuntimeException when the operation fails
     */
    public void save(Subjects entity);

    /**
     * Delete a persistent Subjects entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the {@link javax.persistence.EntityManager#remove(Object)
     * EntityManager#delete} operation.
     * <p>
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISubjectsDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity Subjects entity to delete
     * @throws RuntimeException when the operation fails
     */
    public void delete(Subjects entity);

    /**
     * Persist a previously saved Subjects entity and return it or a copy of it
     * to the sender. A copy of the Subjects entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     * <p>
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ISubjectsDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity Subjects entity to update
     * @return Subjects the persisted Subjects entity instance, may not be the
     * same
     * @throws RuntimeException if the operation fails
     */
    public Subjects update(Subjects entity);

    public Subjects findById(Integer id);

    /**
     * Find all Subjects entities with a specific property value.
     *
     * @param propertyName        the name of the Subjects property to query
     * @param value               the property value to match
     * @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *                            row index in the query result-set to begin collecting the
     *                            results. rowStartIdxAndCount[1] specifies the the maximum
     *                            count of results to return.
     * @return List<Subjects> found by query
     */
    public List<Subjects> findByProperty(String propertyName, Object value, int... rowStartIdxAndCount);

    public List<Subjects> findBySname(Object sname, int... rowStartIdxAndCount);

    public List<Subjects> findByRemark(Object remark, int... rowStartIdxAndCount);

    /**
     * Find all Subjects entities.
     *
     * @param rowStartIdxAndCount Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *                            row index in the query result-set to begin collecting the
     *                            results. rowStartIdxAndCount[1] specifies the the maximum
     *                            count of results to return.
     * @return List<Subjects> all Subjects entities
     */
    public List<Subjects> findAll(int... rowStartIdxAndCount);

    /**
     * 根据名称关键字查询科目
     * @param keyWord 关键字
     * @param rowStartIdxAndCount   index  0  从第几条开始查询， index 1 分页大小
     * @return
     */
    public List<Subjects> findSubjectsByNameKeyWord(String keyWord, int... rowStartIdxAndCount);

    /**
     * 查询名称关键字的总条数
     * @param keyWord
     * @return
     */
    public Long  findSubjectsCountByNameKeyWord(String keyWord);

    /**
     * 查询科目总条数
     * @return
     */
    public  Long count();

    /**
     * 根据科目的属性查询 总条数
     * @param property
     * @param value
     * @return
     */
    public Long findCountByProperty(String property,Object value);
}
