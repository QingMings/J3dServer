package com.iezview.caas.dao;

import java.util.List;

import com.iezview.caas.entity.Planmap;

/**
 * Interface for PlanmapDAO.
 *
 * @author MyEclipse Persistence Tools
 */

public interface IPlanmapDAO {
	/**
	 * Perform an initial save of a previously unsaved Planmap entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * IPlanmapDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Planmap entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Planmap entity);

	/**
	 * Delete a persistent Planmap entity. This operation must be performed
	 * within the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link javax.persistence.EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * IPlanmapDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Planmap entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Planmap entity);

	/**
	 * Persist a previously saved Planmap entity and return it or a copy of it
	 * to the sender. A copy of the Planmap entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = IPlanmapDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Planmap entity to update
	 * @return Planmap the persisted Planmap entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Planmap update(Planmap entity);

	public Planmap findById(Integer id);

	/**
	 * Find all Planmap entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Planmap property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Planmap> found by query
	 */
	public List<Planmap> findByProperty(String propertyName, Object value, int... rowStartIdxAndCount);

	public List<Planmap> findByPname(Object pname, int... rowStartIdxAndCount);

	public List<Planmap> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount);

	public List<Planmap> findByRemark(Object remark, int... rowStartIdxAndCount);

	/**
	 * Find all Planmap entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Planmap> all Planmap entities
	 */
	public List<Planmap> findAll(int... rowStartIdxAndCount);

    /**
     * 查询总行数
     * @return
     */
    public Long count();

    /**
     * 名称关键字查询 规划图
     * @param keyWord
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Planmap> findPlanmapByNameKeyWord(String keyWord,int... rowStartIdxAndCount);

    /**
     * 名称关键字查询规划图总行数
     * @param keyWord
     * @return
     */
    public Long  findPlanmapCountByNameKeyWord(String keyWord);

    /**
     * 按照规划图 属性模糊查询
     * @param propertyName
     * @param value
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Planmap> findByPropertylike(String propertyName, Object value, int... rowStartIdxAndCount);

    /**
     * 按照规划图属性模糊查询 行数
     * @param propertyName
     * @param value
     * @return
     */
    public Long findCountByProperty(String propertyName,Object value);
}
