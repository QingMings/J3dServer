package com.iezview.caas.dao;

import java.sql.Time;
import java.util.List;
import java.util.Set;

import com.iezview.caas.entity.Obliquemodel;

/**
 * Interface for ObliquemodelDAO.
 *
 * @author MyEclipse Persistence Tools
 */

public interface IObliquemodelDAO {
	/**
	 * Perform an initial save of a previously unsaved Obliquemodel entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * IObliquemodelDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Obliquemodel entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Obliquemodel entity);

	/**
	 * Delete a persistent Obliquemodel entity. This operation must be performed
	 * within the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link javax.persistence.EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * IObliquemodelDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Obliquemodel entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Obliquemodel entity);

	/**
	 * Persist a previously saved Obliquemodel entity and return it or a copy of
	 * it to the sender. A copy of the Obliquemodel entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = IObliquemodelDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Obliquemodel entity to update
	 * @return Obliquemodel the persisted Obliquemodel entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Obliquemodel update(Obliquemodel entity);

	public Obliquemodel findById(Integer id);

	/**
	 * Find all Obliquemodel entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Obliquemodel property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Obliquemodel> found by query
	 */
	public List<Obliquemodel> findByProperty(String propertyName, Object value, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByName(Object name, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByCentrallng(Object centrallng, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByCentrallat(Object centrallat, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByCentralheight(Object centralheight, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByDataformat(Object dataformat, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByAltitudemode(Object altitudemode, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByHeading(Object heading, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByTilt(Object tilt, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByRoll(Object roll, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByXscale(Object xscale, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByYscale(Object yscale, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByZscale(Object zscale, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByTiletype(Object tiletype, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByTilemin(Object tilemin, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByTilemax(Object tilemax, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByTilequantity(Object tilequantity, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByDescription(Object description, int... rowStartIdxAndCount);

	public List<Obliquemodel> findByIsload(Object isload, int... rowStartIdxAndCount);

	/**
	 * Find all Obliquemodel entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Obliquemodel> all Obliquemodel entities
	 */
	public List<Obliquemodel> findAll(int... rowStartIdxAndCount);

    /**
     * 模糊查询倾斜摄影实体 属性
     * @param propertyName
     * @param value
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Obliquemodel> findByPropertylike(String propertyName, Object value, int... rowStartIdxAndCount);

    /**
     * 模糊查询倾斜摄影实体 属性 的行数
     * @param propertyName
     * @param value
     * @return
     */
    public Long findCountByPropertylike(String propertyName,Object value);

    /**
     * 查询总行数
     * @return
     */
    public Long count();

    /**
     * 按名称关键字 查询 倾斜摄影模型
     * @param keyWord
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Obliquemodel> findObliquemodelByNameKeyWord(String keyWord,int...rowStartIdxAndCount);

    /**
     * 按名称关键字查询倾斜摄影模型行数
     * @param keyWord
     * @return
     */
    public Long  findObliquemodelCountByNameKeyWord(String keyWord);

}
