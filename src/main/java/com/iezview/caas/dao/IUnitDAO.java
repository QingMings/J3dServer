package com.iezview.caas.dao;

import java.util.List;
import java.util.Set;

import com.iezview.caas.entity.Area;
import com.iezview.caas.entity.Subjects;
import com.iezview.caas.entity.Unit;

/**
 * Interface for UnitDAO.
 *
 * @author MyEclipse Persistence Tools
 */

public interface IUnitDAO {
	/**
	 * Perform an initial save of a previously unsaved Unit entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * IUnitDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Unit entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Unit entity);

    /**
     * 保存基地，
     * @param entity 基地
     */
	public void saveBase(Unit entity);

	/**
	 * Delete a persistent Unit entity. This operation must be performed within
	 * the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link javax.persistence.EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * IUnitDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Unit entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Unit entity);

	/**
	 * Persist a previously saved Unit entity and return it or a copy of it to
	 * the sender. A copy of the Unit entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = IUnitDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Unit entity to update
	 * @return Unit the persisted Unit entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Unit update(Unit entity);

	public Unit findById(Integer id);

	/**
	 * Find all Unit entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Unit property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Unit> found by query
	 */
	public List<Unit> findByProperty(String propertyName, Object value, int... rowStartIdxAndCount);

	public List<Unit> findByParent(Object parent, int... rowStartIdxAndCount);

	public List<Unit> findByUname(Object uname, int... rowStartIdxAndCount);

	public List<Unit> findByShortname(Object shortname, int... rowStartIdxAndCount);

	public List<Unit> findByKeyname(Object keyname, int... rowStartIdxAndCount);

	public List<Unit> findByUtype(Object utype, int... rowStartIdxAndCount);

	public List<Unit> findBySubjects(Object subjects, int... rowStartIdxAndCount);

	public List<Unit> findByCentrallng(Object centrallng, int... rowStartIdxAndCount);

	public List<Unit> findByCentrallat(Object centrallat, int... rowStartIdxAndCount);

	public List<Unit> findByCoveragearea(Object coveragearea, int... rowStartIdxAndCount);

	public List<Unit> findByUaddress(Object uaddress, int... rowStartIdxAndCount);

	public List<Unit> findBySynopsis(Object synopsis, int... rowStartIdxAndCount);

	public List<Unit> findByUlevel(Object ulevel, int... rowStartIdxAndCount);

	public Long findCountByUlevel(Object ulevel);

	public List<Unit> findByNodekind(Object nodekind, int... rowStartIdxAndCount);

	/**
	 * Find all Unit entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Unit> all Unit entities
	 */
	public List<Unit> findAll(int... rowStartIdxAndCount);

    /**
     * 查询所有研究所
     * @param rowStartIdxAndCount
     * @return
     */
	public List<Unit> findAllYanJiuSuo(int ...rowStartIdxAndCount);

    /**
     *
     * 构建 subjects string
     * @author  qingmings
     * @param subjects
     * @return
     */
//    public String setSubjectsString(List<String> subjects);

    /**
     *
     * 按关键字【UName】 模糊查询 单位
     * @author qingmings
     * @param keyWord  查询关键字
     * @param rowStartIdxAndCount   index 0   从第几条开始查， index  1  查多少条
     * @return
     */
    public List<Unit> findUnitByNameKeyWord(String keyWord,int... rowStartIdxAndCount);
    /**
     *
     * 按关键字【UShortName】 模糊查询 单位
     * @author qingmings
     * @param keyWord  查询关键字
     * @param rowStartIdxAndCount   index 0   从第几条开始查， index  1  查多少条
     * @return
     */
    public List<Unit> findUnitByShortNameKeyWord(String keyWord,int... rowStartIdxAndCount);

    /**
     *根据行政区域查询单位
     * @param area
     * @return
     */
    /**
     *根据行政区域查询单位
     * @param area 行政区域
     * @param rowStartIdxAndCount  index 0   从第几条开始查， index  1  查多少条
     * @return
     */
    public List<Unit> findUnitByArea(Area area,int...rowStartIdxAndCount);

    public List<Unit> findYanJjiuSuoByArea(Area area,int...rowStartIdxAndCount );

    /**
     * 根据研究所和 研究所的行政区域查询下属基地
     * @param unit
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Unit> findJiDiByYanJiuSuoAndAreaID(Unit unit,int... rowStartIdxAndCount);

    /**
     * 根据研究所查询下属所有基地
     * @param unit
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Unit> findJidiByYanJiuSuo(Unit unit,int...rowStartIdxAndCount);

    /**
     * 根据地址关键字查询单位
     * @param keyWord  地址关键字
     * @param rowStartIdxAndCount  index 0   从第几条开始查， index  1  查多少条
     * @return
     */
    public List<Unit> findUnitByAddressKeyWord(String keyWord,int... rowStartIdxAndCount);

    /**
     * return totals  of Unit data
     * @return
     */
    public Long   findUnitCountByPropertylike(String propertyName, Object value);

    public Long   findUnitCountByProperty(String propertyName, Object value);

    /**
     * 查询符合名称关键字的单位总条数
     * @param keyWord 名称关键字
     * @return
     */
    public Long  findUnitCountByNameKeyWord(String keyWord);

    /**
     * 查询符合地名关键字的单位总条数
     * @param keyWord
     * @return
     */
    public Long  findUnitCountByAddressKeyWord(String keyWord);

    /**
     * @description 查询某个行政区域的单位总条数
     * @param area
     * @return
     */
    public Long  findUnitCountByArea(Area area);

    /**
     * 根据院所名称关键字查询下属基地
     * @param keyWord  院所名称关键字
     * @param rowStartIdxAndCount index 0   从第几条开始查， index  1  查多少条
     * @return
     */
    public List<Unit> findUnitByParentNameKeyWord(String keyWord,int... rowStartIdxAndCount);

    /**
     * 根据院所名称关键字查询下属基地总行数
     * @param keyWord 院所名称关键字
     * @return
     */
    public Long findUnitCountByParentNameKeyWord(String keyWord);

    /**
     * 单位总数
     * @return
     */
    public Long count();

    /**
     * 研究所-->  基地-->   站
     * 检查 排序字段
     * 如果是 研究所，  查询所有研究所排序字段的值，对比参数的排序值，如果结果集为0 说明排序值可用，否则不可用
     * 如果是 基地 , 查询其所属研究所下的所有基地的排序值，对比参数的排序值，如果结果集为0,说明排序值可用，否则不可用
     * 如果是 站   查询器所属的基地下的所有站的排序值，对比参数的排序值，如果结果集为0，说明排序值可用，否则不可用
     * @param unit 单位
     * @return
     */
    public List<Unit> checkOrder(Unit unit);

    /**
     * 查询单位  排除给定的
     * @param units
     * @return
     */
    public List<Unit>  findUnitNotIncluded(List<Unit> units, int... rowStartIdxAndCount);

    /**
     * 保存全部
     * @param units
     */
    public void saveAll(List<Unit> units);

    /**
     *多条件查询
     * @param queryType 查询类型   1 查询研究所  2 查询基地
     * @param unit  单位
     * @param utype  单位类型
     * @param Area 行政区域
     * @param subjects  学科
     * @param ecoRegion   生态区
     */
    public List<Unit> findUnitByMultipleonditions(int queryType, Unit unit, String utype, Area Area , Subjects subjects,String ecoRegion);

 }
