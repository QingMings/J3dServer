package com.iezview.caas.dao;

import java.util.List;

import com.iezview.caas.entity.Remotesensemap;

/**
 * Interface for RemotesensemapDAO.
 *
 * @author MyEclipse Persistence Tools
 */

public interface IRemotesensemapDAO {
	/**
	 * Perform an initial save of a previously unsaved Remotesensemap entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 *
	 * <pre>
	 *
	 * EntityManagerHelper.beginTransaction();
	 * IRemotesensemapDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Remotesensemap entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(Remotesensemap entity);

	/**
	 * Delete a persistent Remotesensemap entity. This operation must be
	 * performed within the a database transaction context for the entity's data
	 * to be permanently deleted from the persistence store, i.e., database.
	 * This method uses the
	 * {@link javax.persistence.EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * IRemotesensemapDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 *
	 * @param entity
	 *            Remotesensemap entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(Remotesensemap entity);

	/**
	 * Persist a previously saved Remotesensemap entity and return it or a copy
	 * of it to the sender. A copy of the Remotesensemap entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity. This operation must be performed within the
	 * a database transaction context for the entity's data to be permanently
	 * saved to the persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 *
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = IRemotesensemapDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 *
	 * @param entity
	 *            Remotesensemap entity to update
	 * @return Remotesensemap the persisted Remotesensemap entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public Remotesensemap update(Remotesensemap entity);

	public Remotesensemap findById(Integer id);

	/**
	 * Find all Remotesensemap entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Remotesensemap property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Remotesensemap> found by query
	 */
	public List<Remotesensemap> findByProperty(String propertyName, Object value, int... rowStartIdxAndCount);

	public List<Remotesensemap> findBySname(Object sname, int... rowStartIdxAndCount);

	public List<Remotesensemap> findByMaxlng(Object maxlng, int... rowStartIdxAndCount);

	public List<Remotesensemap> findByMaxlat(Object maxlat, int... rowStartIdxAndCount);

	public List<Remotesensemap> findByMinlng(Object minlng, int... rowStartIdxAndCount);

	public List<Remotesensemap> findByMinlat(Object minlat, int... rowStartIdxAndCount);

	public List<Remotesensemap> findByResourcepath(Object resourcepath, int... rowStartIdxAndCount);

	public List<Remotesensemap> findByRemark(Object remark, int... rowStartIdxAndCount);

	/**
	 * Find all Remotesensemap entities.
	 *
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<Remotesensemap> all Remotesensemap entities
	 */
	public List<Remotesensemap> findAll(int... rowStartIdxAndCount);

    /**
     * 查询总行数
     * @return
     */
    public Long count();

    /**
     * 测试名称关键字查询 规划模型
     * @param keyWord
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Remotesensemap> findRemotesensemapByNameKeyWord(String keyWord,int... rowStartIdxAndCount);

    /**
     * 测试 名称关键字查询规划模型总行数
     * @param keyWord
     * @return
     */
    public Long  findRemotesensemapCountByNameKeyWord(String keyWord);

    /**
     * 按照规划模型 属性模糊查询
     * @param propertyName
     * @param value
     * @param rowStartIdxAndCount
     * @return
     */
    public List<Remotesensemap> findByPropertylike(String propertyName, Object value, int... rowStartIdxAndCount);

    /**
     * 按照规划模型属性模糊查询 行数
     * @param propertyName
     * @param value
     * @return
     */
    public Long findCountByProperty(String propertyName,Object value);
}
