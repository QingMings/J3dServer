package com.iezview.caas.fx;

import com.iezview.caas.http.retrofit.ProgressListener;
import com.iezview.caas.http.retrofit.body.ProgressInfo;
import javafx.concurrent.Task;
import retrofit2.Retrofit;

/**
 * 上传下载进度Task抽象类
 * T  service
 */
public abstract class ProgressTask<T> extends Task<T> implements ProgressListener {
    protected   String mBaseUrl; //基础Url
    protected Retrofit.Builder builder ; // 网络请求
    protected Progress<T> progress;  //进度回调
    public ProgressTask(String baseUrl){
            mBaseUrl =baseUrl;
        builder=new Retrofit.Builder().baseUrl(mBaseUrl);
    }

    /**
     * 创建 retrofit Service
     * @param t  Service
     * @return
     */
    protected T createService(Class<T> t){
        return builder.build().create(t);
    }
    @Override
    protected T call() throws Exception {
        return progress.call();
    }

    /**
     * 取消
     */
    @Override
    protected void cancelled() {
        progress.cancelled();
    }

    /**
     * 返回 uuid 名称
     * @param uuid
     */
    @Override
    public void onSuccess(String uuid) {

    }

    /**
     * 进度信息
     * @param progressInfo
     */
    @Override
    public void onProgress(ProgressInfo progressInfo) {
        updateProgress(progressInfo.getPercent(),100);
        if (progressInfo.isFinish()){
            done();
        }
    }

    @Override
    public void onError(long id, Exception e) {
       throw  new RuntimeException("上传失败",e);
    }
}
