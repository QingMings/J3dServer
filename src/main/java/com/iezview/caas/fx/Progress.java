package com.iezview.caas.fx;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

import java.util.concurrent.Callable;
import java.util.function.Consumer;

/**
 * 进度信息抽象类
 *
 * @param <T> service
 */
public abstract class Progress<T> implements Callable<T> {

    protected T mservice; // retrofit 的 Service
    protected MultipartBody.Part mfile; //上传的文件
    protected Call<ResponseBody> call;

    /**
     *
     * @param file 上传的文件
     */
    public Progress(MultipartBody.Part file) {
        this.mfile = file;
    }

    public Progress(){

    }

    /**
     * 执行结果回调
     */
    private Consumer<String> consumer;

    /**
     * 注入 service  和  回调
     * @param t
     * @param c
     */
    public void setService(T t,Consumer<String > c) {
        this.mservice = t;
        this.consumer=c;
    }


    @Override
    public T call() throws Exception {
        return null;
    }

    /**
     * 取消
     */
    public void cancelled() {
        if (call != null) {
            call.cancel();
        }
    }

    /**
     * 触发回调结果
     * @param result
     */
    public void accept(String result){
        if (consumer!=null){
            consumer.accept(result);
        }
    }



}
