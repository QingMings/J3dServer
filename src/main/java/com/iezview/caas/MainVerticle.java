package com.iezview.caas;

import com.iezview.caas.http.server.HttpServerVerticle;
import com.iezview.caas.utils.ResourcesUtils;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 *
 * 程序入口
 * 默认提供配置文件
 * 支持读取外部配置文件 格式  【-conf JsonFilePath】
 * <pre> config file example
 *	{
 *		"server": {
 *			"address": "localhost",
 *			"port": 8080,
 *			"rootStoragePath": "/Users/shishifanbuxie/Office/storage"
 *		},
 *		"storageStructure": {
 *			"unitKey": "院所本部",
 *			"baseUnitKey": "下属基地",
 *			"subStructure": {
 *				"remoteSensingImage": "遥感图",
 *				"planningImage": "规划图",
 *				"liveModel": "实景模型",
 *				"planningModel": "规划模型",
 *				"buildingImage": "建筑图片",
 *				"planningTable": "规划汇总表"
 *			}
 *		}
 *	}
 * </pre>
 *
 *
 */
public class MainVerticle extends AbstractVerticle {

    private Logger logger;

    public MainVerticle() {
        this.logger = LoggerFactory.getLogger(MainVerticle.class);
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        Future<JsonObject> getConfFuture = Future.future();
        ConfigStoreOptions file = new ConfigStoreOptions().setType("file").setFormat("json").setConfig(new JsonObject().put("path", "com/iezview/caas/config.json"));
        ConfigRetrieverOptions options = new ConfigRetrieverOptions().addStore(file);
        ConfigRetriever retriever = ConfigRetriever.create(vertx, options);
        retriever.getConfig(getConfFuture);
        getConfFuture.compose(fileConfig ->
            Future.<String>future(f -> {
                logger.debug("配置文件加载完成");
                if (config().size() == 0) {
                    logger.info("load default config file");
                    fileConfig.fieldNames().forEach(key -> {
                        config().put(key, fileConfig.getValue(key));
                    });
                }else{
                    if (processArgs()!=null&&processArgs().size()==2&&processArgs().get(0).equals("-conf")){
                        logger.info("load outSide config file :"+processArgs().get(1));
                    }else {
                        logger.info("load  config from Options ");
                    }
                }
                logger.info(" init config succes");
                logger.info(config().encodePrettily());
                config().fieldNames().forEach(key->{
                    fileConfig.put(key,config().getValue(key));
                });
                vertx.deployVerticle(HttpServerVerticle.class.getName(), new DeploymentOptions().setConfig(config()), f);
            })
        ).compose(str ->
            Future.<String>future(f -> {
                ResourcesUtils.getInstance().initConfig(vertx, getConfFuture.result(), f);
            })
        ).setHandler(ar -> {
            if (ar.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail(ar.cause());
            }
        });
    }

    @Override
    public void stop() throws Exception {
        vertx.close();
    }
}
