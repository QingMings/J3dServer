package com.iezview.caas.utils;

import com.iezview.caas.dao.*;
import com.iezview.caas.daoimpl.*;
import com.iezview.caas.entity.*;

import java.util.*;

public class    TreeQuery {

    private IUnitDAO unitDAO;
    private IAreaDAO areaDAO;
    private ISubjectsDAO subjectsDAO;
    private IPlanmodelDAO planmodelDAO;
    private IObliquemodelDAO obliquemodelDAO;



    public TreeQuery() {
        unitDAO = new UnitDAO();
        areaDAO = new AreaDAO();
        subjectsDAO = new SubjectsDAO();
        planmodelDAO =new PlanmodelDAO();
        obliquemodelDAO = new ObliquemodelDAO();
    }

    /**
     * 按照 行政区域 构建
     *
     * @return
     */
    public Map<Area, List<Unit>> groupByArea() {
        Map<Area, List<Unit>> groupByArea = new LinkedHashMap<>();
        //查询所有的行政区域
        List<Area> areas = areaDAO.findAll();
        areas.forEach(area -> {
            //根据行政区域查询研究所
            List<Unit> yanjiusuos = unitDAO.findYanJjiuSuoByArea(area);
            if (yanjiusuos != null && yanjiusuos.size() > 0) {
                yanjiusuos.forEach(yjs -> {
                    //根据地域获得 单位
//                    List<Unit> units = groupByArea.get(area);
//                    if (units == null) {
//                        List<Unit> unitList = new ArrayList<>();
//                        unitList.add(yjs);
//                        groupByArea.put(area, unitList);
//                    } else {
//                        units.add(yjs);
//                    }

                    putOrAdd(groupByArea, area, yjs);
                    //查询所有下属基地
                    List<Unit> allJidis = unitDAO.findJidiByYanJiuSuo(yjs);

                    allJidis.forEach(jidi -> {
                        //如果行政区域相等（重写了equas和hashcode方法，所以刻意直接判断equas）
                        if (jidi.getArea().equals(yjs.getArea())) {
                            groupByArea.get(area).add(jidi);
                        } else {
                            //不相等加入新的行政区域内
                            List<Unit> unitotherArea = groupByArea.get(jidi.getArea());
                            if (unitotherArea == null) {
                                List<Unit> unitList = new ArrayList<>();
                                unitList.add(jidi);
                                groupByArea.put(jidi.getArea(), unitList);
                            } else {
                                unitotherArea.add(jidi);
                            }
                        }

                    });
                });
            }
        });
        return groupByArea;
    }


    public static <T> void putOrAdd(Map<T, List<Unit>> group, T t, Unit yjs) {
        List<Unit> units = group.get(t);
        if (units == null) {
            List<Unit> unitList = new ArrayList<>();
            unitList.add(yjs);
            group.put(t, unitList);
        } else {
            units.add(yjs);
        }
    }

    /**
     * 按 级别
     *
     * @return
     */
    public Map<Unit, List<Unit>> groupByLevel() {
        Map<Unit, List<Unit>> groupByLevel = new LinkedHashMap<>();
        //查询所有研究所
        List<Unit> yanjiusuos = unitDAO.findAllYanJiuSuo();
        if (yanjiusuos != null && yanjiusuos.size() > 0) {
            yanjiusuos.forEach(yjs -> {
                //根据研究所查询下属基地信息
                List<Unit> jidis = unitDAO.findJidiByYanJiuSuo(yjs);

                List<Unit> units = groupByLevel.get(yjs);
                if (units == null) {
                    groupByLevel.put(yjs, jidis);
                } else {
                    units.addAll(jidis);
                }
            });
        }

        return groupByLevel;
    }


    public Map<Subjects, List<Unit>> groupBySubject() {
        Map<Subjects, List<Unit>> groupBySubject = new LinkedHashMap<>();
        List<Subjects> subjects = subjectsDAO.findAll();
        subjects.forEach(subject -> {
            // subject.getUnits 加了过滤条件 返回的都是研究所
            subject.getUnits().forEach(yjs -> {
                putOrAdd(groupBySubject, subject, yjs);
                List<Unit> alljidis = unitDAO.findJidiByYanJiuSuo(yjs);
                alljidis.stream().filter(jidi -> jidi.getSubjects().contains(subject)).forEach(jidi -> {
                    putOrAdd(groupBySubject, subject, jidi);
                });
            });
        });

        return groupBySubject;
    }


    public Map<Unit,List<Planmodel>> findAllPlanModel(){
        Map<Unit,List<Planmodel>>  allPlanModel = new LinkedHashMap<>();

        List<Unit> units = unitDAO.findAll();
        units.forEach(unit ->{
            List<Planmodel>   planmodels=   planmodelDAO.findPlanModelByUnit(unit);
            if (planmodels!=null&&planmodels.size()>0){
                allPlanModel.put(unit,planmodels);
            }
        });
        return allPlanModel;
    }

    public Map<Unit,List<Obliquemodel>>  findAllObliqueModel(){
        Map<Unit,List<Obliquemodel>>  allObliquerModel = new LinkedHashMap<>();
        List<Unit>  units = unitDAO.findAll();
        units.forEach(unit -> {
            Set<Obliquemodel>  obliquemodels = unit.getObliquemodels();
            if (obliquemodels!=null&&obliquemodels.size()>0){
                allObliquerModel.put(unit,new ArrayList<>(obliquemodels));
            }
        });
        return  allObliquerModel;
    }

}
