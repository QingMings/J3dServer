package com.iezview.caas.utils;

import com.iezview.caas.daoimpl.EntityManagerHelper;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * 资源工具类
 * 在使用前务必初始化 配置
 *
 * @author qingmings
 * config example
 * {
 * "server": {
 * "address": "",
 * "port": 8080
 * },
 * "storageStructure1": {
 * "unitKey": "院所本部",
 * "baseUnitKey": "下属基地",
 * "subStructure": {
 * "remoteSensingImage": "遥感图",
 * "planningImage": "规划图",
 * "liveModel":"实景模型",
 * "planningModel":"规划模型",
 * "buildingImage":"建筑图片",
 * "planningTable":"规划汇总表"
 * }
 * }
 * }
 */
public class ResourcesUtils {
    private static ResourcesUtils ourInstance = new ResourcesUtils();
    private Logger logger;
    //存储结构
    private JsonObject config;
    private Vertx vertx;
    public static final String server="server";
    public static final String storageStructure = "storageStructure";//存储结构key
    public static final String rootPath = "rootStoragePath";// 存储节点根路径
    public static final String unitKey = "unitKey"; //院所key
    public static final String baseUnitKey = "baseUnitKey"; //基地key
    public static final String subStructure = "subStructure"; //子结构
    public static final String remoteSensingImage = "remoteSensingImage";//遥感图
    public static final String planningImage = "planningImage";//规划图
    public static final String liveModel = "liveModel";//实景模型
    public static final String planningModel = "planningModel";//规划模型
    public static final String buildingImage = "buildingImage";// 建筑图片
    public static final String planningTable = "planningTable";// 规划汇总表

    public static ResourcesUtils getInstance() {
        return ourInstance;
    }

    public Vertx getVertx() {
        return this.vertx;
    }

    private ResourcesUtils() {
        logger = LoggerFactory.getLogger(ResourcesUtils.class);
    }

//    /**
//     * 初始化配置文件
//     *
//     * @param path    配置路径
//     * @param handler
//     */
//    public void initConfigPath(String path, Handler<AsyncResult<JsonObject>> handler) {
//        Future future = Future.<JsonObject>future();
//        vertx.fileSystem().readFile(path, ar -> {
//            try {
//                if (ar.succeeded()) {
//                    JsonObject config = ar.result().toJsonObject().getJsonObject(storageStructure);
//                    if (config != null) {
//                        this.config = config;
//                        future.complete(config);
//                    } else {
//                        future.fail("Can't find JsonObject Config with key:" + storageStructure);
//                    }
//                } else {
//                    future.fail(ar.cause());
//                }
//            } catch (RuntimeException re) {
//                future.fail(re);
//            } finally {
//                if (handler != null) {
//
//                    vertx.runOnContext(event -> handler.handle(future));
//                }
//            }
//        });
//    }

    /**
     * 初始化资源工具类
     *
     * @param vertx
     * @param config
     * @param future
     */
    public void initConfig(Vertx vertx, JsonObject config, Future<String> future) {
        this.vertx = vertx;
        this.config = config;
        logger.info("init resourcesUtils complete!");
        EntityManagerHelper.getEntityManager();
        logger.info("init  entityManager complate!");
        future.complete("init resourcesUtils successful");
    }

    private void checkConfig() {
        if (config == null) {
            throw new RuntimeException("config 未初始化成功!");
        }
    }

    /**
     * 创建forder
     *
     * @return
     */
    public ResourcesUtils generateFolder() {
        checkConfig();
        System.out.println(config.encodePrettily());
        return this;
    }

    /**
     * 创建子存储结构
     *
     * @param path 父级目录
     * @return
     */
    public ResourcesUtils generateSubFolder(String path) {
        checkConfig();
        Path parentPath = Paths.get(path);
        if (Files.notExists(parentPath))
            try {
                Files.createDirectories(parentPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        if (Files.exists(parentPath)) {
            config.getJsonObject(storageStructure).getJsonObject(subStructure).forEach(entry -> {
                Path subDirPath = parentPath.resolve((String) entry.getValue());
                if (Files.notExists(subDirPath)) {
                    try {
                        Files.createDirectory(subDirPath);
                        System.out.println(subDirPath.toAbsolutePath());
                        System.out.println("create folder:" + entry.getValue());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        return this;
    }

    /**
     * 创建子存储结构
     *
     * @param path 父级目录
     * @return
     */
    public ResourcesUtils generateSubForder(Path path) {
        return generateSubFolder(path.toUri().getPath());
    }

    /**
     * 获取根存储路径
     * @return
     */
    public String getRootPath() {
        return config.getJsonObject(server).getString(rootPath);
    }

    /**
     * 获取 院所本部字段
     * @return
     */
    public String getUnitKey() {
        return config.getJsonObject(storageStructure).getString(unitKey);
    }

    /**
     * 获取 下属基地字段
     * @return
     */
    public String getBaseUnitKey(){
        return config.getJsonObject(storageStructure).getString(baseUnitKey);
    }

    /**
     * 根据 type 获取 存储目录 字段
     * @param typeId
     * @return
     */
    public String getSubStructurekey(String typeId){
        String key ="";
        JsonObject subStructureObject=config.getJsonObject(storageStructure).getJsonObject(subStructure);
        switch (typeId){
            case "1":
                key = subStructureObject.getString(remoteSensingImage);
                break;
            case "2":
                key= subStructureObject.getString(planningImage);
                break;
            case "3":
                key= subStructureObject.getString(liveModel);
                break;
            case "4":
                key =subStructureObject.getString(planningModel);
                break;
            case "5":
                key = subStructureObject.getString(buildingImage);
                break;
            case "6":
                key =subStructureObject.getString(planningTable);
                break;
            default:
                key=null;
        }
        return key;
    }
}
