package com.iezview.caas.utils.excel.model;

import com.github.crab2died.annotation.ExcelField;
import org.apache.commons.lang3.StringUtils;

/**
 * 基建局单位信息和建筑信息整理excel
 * Excel 2 Object
 */
public class UnitExcelModel {
    @ExcelField(title = "编号", order = 1)
    private String id;
    @ExcelField(title = "所名", order = 2)
    private String name;
    @ExcelField(title = "单位全称", order = 3)
    private String fullName;
    @ExcelField(title = "遥感影像", order = 4)
    private String remoteseneMap;
    @ExcelField(title = "规划图", order = 5)
    private String planMap;
    @ExcelField(title = "单位分类", order = 6)
    private String unitType;
    @ExcelField(title = "基地编号", order = 7)
    private String unitNum;
    @ExcelField(title = "站名", order = 8)
    private String stationName;
    @ExcelField(title = "站点编号", order = 9)
    private String stationNum;
    @ExcelField(title = "中心点经度", order = 10)
    private String centerLong;
    @ExcelField(title = "中心点纬度", order = 11)
    private String centerLat;
    @ExcelField(title = "所属行政区域", order = 12)
    private String area;
    @ExcelField(title = "建筑编号", order = 13)
    private String buildingNum;
    @ExcelField(title = "建筑名称", order = 14)
    private String buildingName;
    @ExcelField(title = "建筑图片", order = 15)
    private String buildingImage;
    @ExcelField(title = "建筑年代", order = 16)
    private String buildingYear;
    @ExcelField(title = "建筑中心点经度", order = 17)
    private String buildingCenterLong;
    @ExcelField(title = "建筑中心点纬度", order = 18)
    private String buildingCenterLat;
    @ExcelField(title = "总建筑面积(㎡)", order = 19)
    private String buildingTotalMeasureArea;
    @ExcelField(title = "地上面积(㎡)", order = 20)
    private String buildingUpperMeasureArea;
    @ExcelField(title = "地下面积(㎡)", order = 21)
    private String buildingLowerMeasureArea;
    @ExcelField(title = "层数", order = 22)
    private String buildingTotalLayer;
    @ExcelField(title = "参建单位", order = 23)
    private String constructioUnits;
    @ExcelField(title = "用途", order = 24)
    private String purpose;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getUnitNum() {
        return unitNum;
    }

    public void setUnitNum(String unitNum) {
        this.unitNum = unitNum;
    }

    public String getCenterLong() {
        return centerLong;
    }

    public void setCenterLong(String centerLong) {
        this.centerLong = centerLong;
    }

    public String getCenterLat() {
        return centerLat;
    }

    public void setCenterLat(String centerLat) {
        this.centerLat = centerLat;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBuildingNum() {
        return buildingNum;
    }

    public void setBuildingNum(String buildingNum) {
        this.buildingNum = buildingNum;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getBuildingYear() {
        return buildingYear;
    }

    public void setBuildingYear(String buildingYear) {
        this.buildingYear = buildingYear;
    }

    public String getBuildingTotalMeasureArea() {
        return buildingTotalMeasureArea;
    }

    public void setBuildingTotalMeasureArea(String buildingTotalMeasureArea) {
        this.buildingTotalMeasureArea = buildingTotalMeasureArea;
    }

    public String getBuildingUpperMeasureArea() {
        return buildingUpperMeasureArea;
    }

    public void setBuildingUpperMeasureArea(String buildingUpperMeasureArea) {
        this.buildingUpperMeasureArea = buildingUpperMeasureArea;
    }

    public String getBuildingLowerMeasureArea() {
        return buildingLowerMeasureArea;
    }

    public void setBuildingLowerMeasureArea(String buildingLowerMeasureArea) {
        this.buildingLowerMeasureArea = buildingLowerMeasureArea;
    }

    public String getBuildingTotalLayer() {
        return buildingTotalLayer;
    }

    public void setBuildingTotalLayer(String buildingTotalLayer) {
        this.buildingTotalLayer = buildingTotalLayer;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    /**
     * 是否空行
     *
     * @return
     */
    public boolean isBlankOrNull() {
        return StringUtils.isBlank(this.id) &&
            StringUtils.isBlank(this.name) &&
            StringUtils.isBlank(this.fullName) &&
            StringUtils.isBlank(this.remoteseneMap) &&
            StringUtils.isBlank(this.unitType) &&
            StringUtils.isBlank(this.unitNum) &&
            StringUtils.isBlank(this.stationName) &&
            StringUtils.isBlank(this.stationNum) &&
            StringUtils.isBlank(this.centerLong) &&
            StringUtils.isBlank(this.centerLat) &&
            StringUtils.isBlank(this.area) &&
            StringUtils.isBlank(this.buildingNum) &&
            StringUtils.isBlank(this.buildingName) &&
            StringUtils.isBlank(this.buildingYear) &&
            StringUtils.isBlank(this.buildingTotalMeasureArea) &&
            StringUtils.isBlank(this.buildingUpperMeasureArea) &&
            StringUtils.isBlank(this.buildingLowerMeasureArea) &&
            StringUtils.isBlank(this.buildingTotalLayer) &&
            StringUtils.isBlank(this.purpose);
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRemoteseneMap() {
        return remoteseneMap;
    }

    public void setRemoteseneMap(String remoteseneMap) {
        this.remoteseneMap = remoteseneMap;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationNum() {
        return stationNum;
    }

    public void setStationNum(String stationNum) {
        this.stationNum = stationNum;
    }

    public String getPlanMap() {
        return planMap;
    }

    public void setPlanMap(String planMap) {
        this.planMap = planMap;
    }

    public String getBuildingImage() {
        return buildingImage;
    }

    public void setBuildingImage(String buildingImage) {
        this.buildingImage = buildingImage;
    }

    public String getBuildingCenterLong() {
        return buildingCenterLong;
    }

    public void setBuildingCenterLong(String buildingCenterLong) {
        this.buildingCenterLong = buildingCenterLong;
    }

    public String getBuildingCenterLat() {
        return buildingCenterLat;
    }

    public void setBuildingCenterLat(String buildingCenterLat) {
        this.buildingCenterLat = buildingCenterLat;
    }

    public String getConstructioUnits() {
        return constructioUnits;
    }

    public void setConstructioUnits(String constructioUnits) {
        this.constructioUnits = constructioUnits;
    }

    /**
     * 是否是研究所
     *
     * @return
     */
    public boolean isUnit() {
        return (!isBlankOrNull()) && this.id != null && "s".equals(this.unitType.trim());
    }

    /**
     * 是否是基地
     *
     * @return
     */
    public boolean isUnitBase() {
        boolean res = false;
        try {
            res = (!isBlankOrNull()) && this.name == null && this.fullName != null && ("j".equals(this.unitType.trim()) || "jz".equals(this.unitType.trim()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * 是否是带站的基地
     *
     * @return
     */
    public boolean isUnitBaseWithStation() {
        return (!isBlankOrNull()) && this.name == null && this.fullName != null && "jz".equals(this.unitType.trim());
    }


    /**
     * 是否是站
     *
     * @return
     */
    public boolean isStation() {
        return (!isBlankOrNull()) && this.stationName != null && this.stationNum != null;
    }

    /**
     * 是否是建筑
     *
     * @return
     */
    public boolean isBuilding() {
        return ((!isBlankOrNull())) &&
            this.buildingNum != null &&
            this.buildingName != null;
    }

    @Override
    public String toString() {
        return "UnitExcelModel{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", fullName='" + fullName + '\'' +
            ", remoteseneMap='" + remoteseneMap + '\'' +
            ", planMap='" + planMap + '\'' +
            ", unitType='" + unitType + '\'' +
            ", unitNum='" + unitNum + '\'' +
            ", stationName='" + stationName + '\'' +
            ", stationNum='" + stationNum + '\'' +
            ", centerLong='" + centerLong + '\'' +
            ", centerLat='" + centerLat + '\'' +
            ", area='" + area + '\'' +
            ", buildingNum='" + buildingNum + '\'' +
            ", buildingName='" + buildingName + '\'' +
            ", buildingImage='" + buildingImage + '\'' +
            ", buildingYear='" + buildingYear + '\'' +
            ", buildingCenterLong='" + buildingCenterLong + '\'' +
            ", buildingCenterLat='" + buildingCenterLat + '\'' +
            ", buildingTotalMeasureArea='" + buildingTotalMeasureArea + '\'' +
            ", buildingUpperMeasureArea='" + buildingUpperMeasureArea + '\'' +
            ", buildingLowerMeasureArea='" + buildingLowerMeasureArea + '\'' +
            ", buildingTotalLayer='" + buildingTotalLayer + '\'' +
            ", constructioUnits='" + constructioUnits + '\'' +
            ", purpose='" + purpose + '\'' +
            '}';
    }
}
